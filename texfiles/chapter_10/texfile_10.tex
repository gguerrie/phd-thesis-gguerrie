\chapter{Results}
\label{sec:chapter_10}

This chapter presents the results obtained from the application of the fit strategies described in Chapter~\ref{sec:chapter_5} to data. 
The fitted values for \xtt, \xz, \ratio, and $\epsilon_b$ are presented for the primary measurement, employing 29 \ifb of integrated luminosity.
A general comparison with the results obtained in the previous measurements is provided as well.
Further details on both ATLAS-CONF-2022-070 and ATLAS-CONF-2023-006 are provided in Appendix~\ref{app:PreviousResults}, according to the chronological order of the publications.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \afterpage{\clearpage}
% \newpage
% \section{Primary measurement - 29 \ifb}
% \label{sec:MainResult}

Figure~\ref{fig:ttbar_PrePost} shows the distributions used in the fit to extract the results before and after performing the fit. Distributions employed in previous measurements are included as well.
The expected values for the \ttbar and $Z$-boson production cross-section, for the fiducial phase space of the $Z$-boson, and the ratio of the cross-sections are:

\begin{align*}
  R_{t\bar{t}/Z}^{\text{SM}} &= 1.238^{+0.063}_{-0.071}~(\text{scale+PDF+}\alpha_\text{s}), \\
  \sigma_{\ttbar}^{\text{SM}} &= 924^{+32}_{-40} \mathrm{(scale+PDF) pb}, \\
  \sigma_{Z}^{\text{SM}} &= 746^{+21}_{-22}~(\text{scale+PDF+}\alpha_\text{s})~\text{pb}.
\end{align*}

Where the term $\text{scale+PDF}$ indicates the QCD scale variations and the PDF uncertainties. 
For the \xz\ and \ratio\ measurements, also the uncertainty related to $\alpha_\text{s}$ is considered.
The $\epsilon_b$ value in the simulation is $0.5431 \pm 0.0003 \mathrm{(MC stat.)}$.

The fitted values are:

\begin{align*}
  R_{t\bar{t}/Z} &= 1.145 \pm 0.003 \mathrm{(stat.)}\pm 0.021 \mathrm{(syst.)}\pm 0.002 \mathrm{(lumi.)}, \\
  \sigma_{\ttbar} &= 850 \pm 3 \mathrm{(stat.)}\pm 18 \mathrm{(syst.)}\pm 20 \mathrm{(lumi.) pb}, \\
  \xz &= 743.6 \pm 0.2~\mathrm{(stat.)} \pm 11~\mathrm{(syst.)}\pm 16~\mathrm{(lumi.)~pb}, \\
  \epsilon_b &= 0.544 \pm 0.001 \mathrm{(stat.)}\pm 0.004 \mathrm{(syst.)}\pm 0.001 \mathrm{(lumi.) pb},
\end{align*}

where $\mathrm{(stat.)}$, $\mathrm{(syst.)}$, $\mathrm{(lumi.)}$ represent respectively the statistical, systematic, and luminosity-related uncertainties.
In this measurement, as described in Section~\ref{sec:fit-setups}, only same flavour channels are used in the $Z$-only fit.
This procedure is designed to account for all the correlations among systematic uncertainties while computing the \ttbar and $Z$-boson cross sections measurements.
Moreover, in the $Z$-boson cross section fit an additional normalisation uncertainty of 5.1\% is applied on the \ttbar sample, according to the theoretical uncertainty on the
predicted cross section value.  
Table~\ref{tab:results_comp} presents the comparison of the results obtained in the three different analyses reported in this thesis. 

% \begin{landscape}
  {
  \renewcommand{\arraystretch}{1.5}
  \begin{table}[!hbt]
    \centering
    \resizebox{\textwidth}{!}{
      \begin{tabular}{ll|c|c|c}
        \toprule
        \multicolumn{2}{c}{\textbf{}} & \textbf{ATLAS-CONF-2022-070} & \textbf{ATLAS-CONF-2023-006} & \textbf{Primary Measurement} \\
        
        \multirow{2}{*}{\(\sigma_{t\bar{t}}\) [pb]} & Theory & $ 924^{+32}_{-40} $ 
        & $ 924^{+32}_{-40} $ & $ 924^{+32}_{-40} $ \\
        & Measurement & $830 \pm 12\pm 27\pm 86$ & $859 \pm 4\pm 22 \pm 19 $ & $ 850 \pm 3 \pm 18 \pm 20 $ \\
        \midrule
        
        \multirow{2}{*}{\(\sigma_{Z \to\ell\ell}^{\text{fid.}}\) [pb]} & Theory &  $2182^{+42}_{-45} $ & $ 741 \pm 15$ & $ 746^{+21}_{-22}$ \\
         & Measurement & $2075 \pm 2\pm 98\pm 199$ & $ 751.2 \pm 0.3\pm 15 \pm 17$ & $ 743.6 \pm 0.2 \pm 11 \pm 16$ \\
         \midrule
        
        \multirow{2}{*}{\(R_{t\bar{t}/Z}\)} & Theory & $0.423 \pm 0.015 $ & $ 1.2453 \pm 0.076$ & $ 1.238^{+0.063}_{-0.071}$  \\
         & Measurement & $0.400 \pm 0.006\pm 0.017\pm 0.005$ & $1.144 \pm 0.006\pm 0.022\pm 0.003$ & $ 1.145 \pm 0.003 \pm 0.021 \pm 0.002 $  \\
         \midrule
        
        \multirow{2}{*}{\(\epsilon_b\)} & MC simulation & $0.5529\pm 0.0002$ & $0.5451\pm 0.0002$ & $0.5431 \pm 0.0003$ \\
         & Measurement & $ 0.553 \pm 0.007  \pm 0.005  \pm 0.001 $ & $ 0.548\pm 0.002\pm 0.004 \pm 0.001$ & $ 0.544 \pm 0.001 \pm 0.004 \pm 0.001 $  \\
        \bottomrule
    \end{tabular}
    }
    \caption{Comparison between the measurements performed using different subsets of the 2022 data, and their corresponding theory predictions.
    As described in Section~\ref{sec:DataMC_definitions_Z}, for ATLAS-CONF-2022-070 the predicted cross-section for $Z$ is computed with the requirement $m_{\ell\ell}>40\gev$.
    Theory predictions are associated with the (scale + PDF) uncertainty, except for the predictions on \xz\ and $R_{t\bar{t}/Z}$, that in the primary measurement include also the uncertainty on $\alpha_\text{s}$.
    The value of $\epsilon_b$ obtained from the simulation is followed by the statistical uncertainty on the MC sample.
    Results from measurements are followed by statistical $\pm$ systematic $\pm$ luminosity uncertainties.
    }
  \label{tab:results_comp}
  \end{table}
  }
  % \end{landscape}

The result for \xtt\ is presented for a top quark mass of 172.5~\gev. 
The dependence of the result from $\mtop$ is $(1/\xtt)\mathrm{d}\xtt/\mathrm{d}m_t=-0.36\%/\gev$, 
as estimated from a variation of the top-quark mass in the \ttbar and $tW$ samples to 171.5~\gev and 173.5~\gev.

Table~\ref{tab:zratio_GroupedImpact} shows the impact of the systematic uncertainties grouped by their origin. 
The uncertainties from the previous measurements have been added as well to facilitate the comparison.

In the \ttbar\ cross section measurement, the uncertainty in the luminosity estimation yields the largest contribution, followed by electron and muon reconstruction.
Uncertainties in the parton shower and hadronisation modelling also have a significant impact on the precision.
For the \xz\ result, uncertainty on luminosity, muon reconstruction, \pileup and electron reconstruction provide the largest contribution to the measurement.
For the cross section ratio, several sources of the systematic uncertainties partially cancel out; 
nevertheless, the dominant contribution comes from \ttbar\ modelling, as it does not cancel out in the ratio. Other significant sources of uncertainty include trigger and lepton reconstruction efficiencies.
The reason for which the trigger efficiency uncertainty does not fully cancel out, is that \ttbar and $Z$-boson events have different $\eta$ and \pt\ distributions; 
moreover, different combinations of triggers are employed for the samples.

Figure~\ref{fig:results1} shows the measured $\ttbar$ cross-section as a function of the
centre-of-mass energy compared to the theory prediction using the PDF4LHC21 PDF set.
This primary measurement is compatible with the prediction at 1.5 standard deviations, assuming uncorrelated uncertainties between the prediction and measurement.
In Fig.~\ref{fig:results2} the ratio of the $\ttbar$ and $Z$-boson production cross-sections is compared to
the prediction for several sets of PDF. The ratio is slightly lower than
most predictions, but compatible at 1.3 standard deviations with the SM prediction based on the PDF4LHC21 set, assuming a top-quark mass of 172.5~\gev.

As in the previous measurements, cross-checks have been performed to validate the obtained results.
Firstly, some pull and constraint on nuisance parameters could be related to the $Z \rightarrow ee$ and $Z \rightarrow \mu\mu$ cross sections not being equal. 
The ratio of $Z\to \mu\mu$ over $Z \to ee$ has been measured to be $R_{Z\to \mu\mu/Z\to ee} = 1.003 \pm 0.034$; the two cross section measurements agree to better than 0.5 standard deviations,
therefore no significant pulls are observed, except for a NP related to the isolation of muons, that is constrained to about 75\% of its pre-fit uncertainty.
The complete summary of ranking, pulls and constraints of the NPs, as well as correlation matrices, is shown in Appendix~\ref{app:correlations_29}.
Moreover, as the \pt\ of the dilepton pair is known to not be accurately modelled by the MC simulation~\cite{PMGR-2021-01}, 
the distribution of the \pt\ of the simulated dilepton pair has been reweighted to match the data in $ee$ and $\mu\mu$ events.
This operation is performed assuming that the only contribution comes from $Z$-boson events.
This test yields a 0.13\% impact on the prediction, thus it has been considered negligible.

\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_10/zratio/Summary.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_10/zratio/Summary_postFit.pdf}}\\
  \subfloat[]{\includegraphics[width=0.49\textwidth]{figures/chapter_10/ATLAS-CONF-2023-006/Summary.pdf}}
  \subfloat[]{\includegraphics[width=0.49\textwidth]{figures/chapter_10/ATLAS-CONF-2023-006/Summary_postFit.pdf}}\\
  \subfloat[]{\includegraphics[width=0.49\textwidth]{figures/chapter_10/ATLAS-CONF-2022-070/Summary.pdf}}
  \subfloat[]{\includegraphics[width=0.49\textwidth]{figures/chapter_10/ATLAS-CONF-2022-070/Summary_postFit.pdf}}
  \caption{Comparison of data and prediction for the event yields in the three lepton channels before the fit (left) and after the fit (right). 
  Distributions used in the primary measurement (a, b), as well as in ATLAS-CONF-2022-070 (c, d) and ATLAS-CONF-2023-006 (e, f) results are reported.
  The $e\mu$ channel is split into events with one or two $b$-tagged jets.
  The bottom panel shows the ratio of data over the prediction. The hashed bands represent the total uncertainty. 
  Correlations of the NPs as obtained from the fit are used to build the uncertainty bad in the post-fit distribution.}  
  \label{fig:ttbar_PrePost}
\end{figure}

\begin{landscape}
\begin{table}[!hbt]
  \centering
    \begin{tabular}{ll|ccc|ccc|ccc}
      \toprule
      \multicolumn{2}{c}{\multirow{2}{*}{\textbf{Category}}} & \multicolumn{3}{c}{\textbf{ATLAS-CONF-2022-070}} & \multicolumn{3}{c}{\textbf{ATLAS-CONF-2023-006}} & \multicolumn{3}{c}{\textbf{Primary Measurement}} \\
      \cline{3-11}
      \multicolumn{2}{c}{} & \multicolumn{9}{c}{Uncertainty [\%]} \\
      \cline{3-11}
      \multicolumn{2}{c}{\rule{0pt}{1.05\normalbaselineskip}} & \(\sigma_{t\bar{t}}\) & \(\sigma_{Z \to\ell\ell}^{m_{\ell\ell}>40}\) & \(R_{t\bar{t}/Z}\) & \(\sigma_{t\bar{t}}\) & \(\sigma_{Z \to\ell\ell}^{\text{fid.}}\) & \(R_{t\bar{t}/Z}\) & \(\sigma_{t\bar{t}}\) & \(\sigma_{Z \to\ell\ell}^{\text{fid.}}\) & \(R_{t\bar{t}/Z}\) \\
      [0.5ex] \cline{3-11}
      \(t\bar{t}\) & parton shower/hadronisation & 0.6 & 0.2 & 0.7 & 1.1 & \(<0.2\) & 1.0 & 0.9 & \(<0.2\) & 0.9 \\
      & scale variations & 0.5 & \(<0.2\) & 0.5 & 0.2 & \(<0.2\) & 0.2 & 0.4 & \(<0.2\) & 0.4 \\
      & Top quark \pt reweighting & - & - & - & 0.6 &\(<0.2\) & 0.5 & 0.6 & \(<0.2\) & 0.6 \\
      \(Z\) & scale variations & 0.2 & 2.9 & 2.9 & 0.2 & 0.5 & 0.3 & \(<0.2\) & 0.4 & 0.3 \\
      Bkg. & Single top modelling & 0.6 & \(<0.2\) & 0.6 & 0.4 & \(<0.2\) & 0.4 & 0.6 & \(<0.2\) & 0.6 \\
      & Diboson modelling & \(<0.2\) & \(<0.2\) & 0.5 & \(<0.2\) &\(<0.2\) & \(<0.2\) & \(<0.2\) & \(<0.2\) & 0.2 \\
      & Mis-Id leptons & 0.6 & \(<0.2\) & 0.6 & 0.5 & \(<0.2\) & 0.5 & 0.6 & \(<0.2\) & 0.6 \\
      & \(t\bar{t}V\) modelling & - & - & - & - & - & - & \(<0.2\) & \(<0.2\) & \(<0.2\) \\
      Lept. & Electron reconstruction & 1.6 & 2.3 & 1.1 & 1.0 & 1.1 & 0.5 & 1.2 & 1.0 & 0.4 \\
      & Muon reconstruction & 1.3 & 2.4 & 0.3 & 1.5 & 1.2 & 0.8 & 1.4 & 1.4 & 0.3 \\
      & Lepton trigger & 0.2 & 1.3 & 1.1 & 0.4 & 0.7 & 0.8 & 0.4 & 0.4 & 0.4 \\
      Jets/tagging & Jet reconstruction & 0.2 & - & 0.2 & 0.4 & - & 0.3 & 0.4 & - & 0.4 \\
      & Flavour tagging & 1.9 & - & 1.9 & 0.2 & - & 0.2 & 0.4 & - & 0.3 \\
      \midrule
      & Pileup & 0.5 & 0.6 & \(<0.2\) & 1.1 & 1.1 & \(<0.2\) & 0.7 & 0.8 & \(<0.2\) \\
      & PDFs & 0.5 & 1.4 & 1.3 & 0.4 & 0.2 & 0.4 & 0.5 & \(<0.2\) & 0.5 \\
      & Luminosity & 10.3 & 9.6 & 1.3 & 2.3 & 2.2 & 0.3 & 2.3 & 2.2 & 0.3 \\
      \bottomrule
      & Systematic Uncertainty & 10.8 & 10.7 & 4.4 & 3.5 & 3.0 & 2.0 & 3.2 & 2.8 & 1.8 \\
      & Statistical Uncertainty & 1.5 & 0.1 & 1.5 & 0.5 & 0.03 & 0.5 & 0.3 & 0.02 & 0.3 \\
      \bottomrule
      & Total Uncertainty & 11 & 10.7 & 4.7 & 3.5 & 3.0 & 2.0 & 3.2 & 2.8 & 1.9 \\
  \end{tabular}
  \caption{ Observed impact of the different sources of uncertainty on
    the $\ttbar$ and $Z$-boson cross sections and their ratio $R$,
    grouped by category. The $\ttbar$ cross section and $R$ are obtained
    in a simultaneous fit to the four regions including both opposite- and same-flavour,
    while the $Z$-boson cross section is obtained from
    a fit to same-flavour events only. The impact of each category is
    obtained by repeating the fit after having fixed the set of nuisance
    parameters corresponding to that category, and subtracting that
    uncertainty in quadrature from the uncertainty found in the full
    fit. The statistical uncertainty is obtained by repeating the fit after having fixed all
    nuisance parameters to their fitted values. Only the acceptance
    effects are considered for the PDF and the modelling uncertainties.}
\label{tab:zratio_GroupedImpact}
\end{table}
\end{landscape}

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/chapter_10/zratio/xsec_ecm}
  \caption{Comparison of the measures $\ttbar$ cross-section compared to theory predictions using the PDF4LHC21 PDF set.
  The measurements shown include $e\mu$ final state ($e\mu$ + $b$-tagged jets); $ee$, $\mu\mu$ and $e\mu$ final states ($ll$ + $b$-tagged jets); 
  single lepton final states ($l$ + jets); as well as combinations of final states (combined).
  The bottom panel shows the ratio of the measured values and three predictions that either contain only the uncertainties originating from the QCD scale variations (black), 
  only the variations in the PDF uncertainties (red) or the total uncertainty in the prediction (blue).}
  \label{fig:results1}
\end{figure}

\begin{figure}[htb]
  \centering
  \includegraphics[width=1.\textwidth]{figures/chapter_10/zratio/Xsec_ratio_vs_Theory}
  \caption{Ratio of the $\ttbar$ to the $Z$-boson cross-section
    compared to the prediction for several sets of parton distribution functions. 
    If not explicitly mentioned, a top-quark mass of 172.5 \gev\ is assumed.
    The hatched box represents the PDF+$\alpha_\text{S}$ component of the uncertainty on the prediction.
    For the PDF4LH21 PDF set, predictions for different assumptions about the top-quark mass are also displayed.}
  \label{fig:results2}
\end{figure}

\FloatBarrier


