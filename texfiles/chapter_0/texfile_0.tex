\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}
\markboth{Introduction}{Introduction}

The Standard Model (SM) of particle physics serves as the prevailing framework for our understanding of the fundamental constituents of the Universe. 
Developed over the past decades through rigorous theoretical formulation and extensive empirical validation, the SM encapsulates the interactions between elementary particles via three of the four fundamental forces:
electromagnetism, the weak force, and the strong nuclear force. 
High-energy particle colliders like the Large Hadron Collider (LHC)~\cite{Evans:2008zzb} at CERN (Conseil Européen pour la Recherche Nucléaire) offer unique opportunities to probe the robustness of the SM and to search for phenomena that could signify physics beyond the Standard Model (BSM). 
The LHC is currently the world's most powerful particle accelerator, operating at a centre-of-mass energy of 13.6 \tev.
In July 2012, the last missing piece of the SM, the Higgs Boson, was observed~\cite{HIGG-2012-27,CMS-HIG-12-028} independently by the two multi-purpose experiments at the LHC: ATLAS~\cite{PERF-2007-01} and CMS~\cite{CMS-CMS-00-001}.

Despite its overwhelming success in predicting and describing a plethora of phenomena, the SM is not without limitations. 
For instance, it does not incorporate the fourth fundamental force, gravity, as described by General Relativity, 
nor does it offer an explanation for the disparity between the quarks' masses.

Among the quarks, the top quark stands as a notable representative of some intricacies within the SM, also hinting at potential pathways to explore the unresolved issues; it is the heaviest known elementary particle, and its large coupling to the Higgs boson suggests a particularly important role in the SM.
The top quark was discovered in 1995 by the CDF~\cite{PhysRevLett.74.2626} and D\O \:~\cite{PhysRevLett.74.2632} Collaborations at the Fermilab Tevatron $p\bar{p}$ collider. 
Its short lifetime causes the top quark to transfer all of its properties to the decay products, making it a unique particle compared to others in the SM.


\subsection*{Motivation and structure of the thesis}
In the Summer of 2022, the LHC resumed its physics operations, entering the third run of physics data acquisition.
The LHC Run~3 is characterised by a small energy increase with respect to the previous run, from \sqs=13 to \sqs=13.6 \tev, accompanied by improvements in both the accelerator and the experiments.
The ATLAS detector has received a number of upgrades, both in its hardware and software components.
All of these upgrades are designed to withstand the more challenging conditions expected after the High-Luminosity upgrade of the LHC. 

The analysis reported in this thesis uses the first dataset available from Run~3, offering crucial insights into validating the functionality of the upgraded detector and reconstruction software.
To ensure a quick and reliable feedback at the beginning of a new physics run, fast physics measurements of known processes are highly beneficial.
The top quark pair production is an accessible, well-known process within the SM. 
The measurement of the top quark pair production cross section represents one of the best scenarios to benchmark the performance of the ATLAS detector.
Events featuring a pair of opposite-charge leptons (electrons or muons), several jets and $b$-tagged jets are considered. 
The choice of the dilepton channel allows to reduce the sensitivity to uncertainties related to $b$-tagging and jets. 
However, carrying out a measurement in the early stages of a physics run implies having a significant impact from the possibly limited knowledge of the integrated luminosity. 
To mitigate the considerable uncertainty on luminosity, the ratio of the top quark pair to the $Z$-boson production cross sections is presented. 
This approach enables the mutual cancellation of not only luminosity-related uncertainties but also various other uncertainties, thereby enhancing the precision of the measurement.
Furthermore, given that the dynamics of top quark pairs and $Z$-boson production are largely influenced by different parton distribution functions, 
the ratio of these cross-sections at a specific centre-of-mass energy exhibits considerable sensitivity to the gluon-to-quark parton distribution function ratio~\cite{STDM-2016-02}.

To obtain a consistent and robust study on the ATLAS detector performance in Run~3, the same measurement is performed three times, employing three different datasets, gathered in different periods of 2022.
The author of this thesis took part in all the measurements, significantly contributing to almost every aspect of the analysis, as well as in the writing of the internal supporting notes, and of the paper that is based on them.
Given the nature of the analysis, the author believes that three words aptly capture the essence of the measurements presented in this work: accuracy, speed, and momentum.
It constitutes one of the first measurements published by the ATLAS collaboration in Run~3, and it is the result of a collaborative endeavour involving nearly all the experiment's working groups.

This thesis is structured as follows: 
Chapter~\ref{sec:chapter_1} provides an overview on the SM, focusing on its gauge structure and on the overall physics reach of the theory.
Chapter~\ref{sec:chapter_2} specifically addresses the top quark physics, highlighting its production mechanisms, its properties, and summarising the previous measurements performed by both the ATLAS and CMS collaborations.
In Chapter~\ref{sec:chapter_3}, the design and performance of the LHC accelerator complex, together with the ATLAS detector, are presented.
Special consideration is given to the upgrades performed on the ATLAS detector since the last run of data acquisition.
Chapter~\ref{sec:chapter_4} presents the experimental procedures employed in the reconstruction, identification and isolation of the physics objects used in the measurement, highlighting the updates.
Chapter~\ref{sec:chapter_5} discusses in depth the strategy employed in the measurement, and the statistical procedures adopted to extract the results.
This chapter also provides a chronological summary of publications pertinent to the analysis, tracing their development over time.
Chapter~\ref{sec:chapter_6} includes the description of the simulated Monte Carlo samples utilised in the analysis to model signal and background events.
Chapter~\ref{sec:chapter_7} explores comprehensively the dataset used in the measurement, as well as the event selection employed.
Chapter~\ref{sec:ratioCalculation} delves into the description of the theoretical predictions for the ratio of the \ttbar and $Z$-boson production cross sections.
Chapter~\ref{sec:chapter_9} elaborates on the sources of systematic uncertainties affecting the measurement. 
Chapter~\ref{sec:chapter_10} presents the experimental results obtained for the individual cross sections, as well as for the ratio measurement. 
Finally, the conclusions of this thesis, alongside with an outlook for the future of Run~3 in ATLAS, is provided in Chapter~\ref{sec:Conclusions}.

\FloatBarrier