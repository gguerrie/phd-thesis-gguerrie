\chapter{Event selection}
\label{sec:chapter_7}
The measurement presented in this thesis employs data collected by the ATLAS detector in $pp$ collisions at $\sqs = 13.6~\tev$, using all available 2022 data with a working muon trigger. 
This corresponds to an integrated luminosity of approximately \lumi, following the luminosity measurement methods described in Section~\ref{sect: Forward}. 
Section~\ref{sec: dataset} describes the dataset used and the related data taking conditions. 
Events are gathered in the dilepton channel, obeying to specific criteria, as presented in Section~\ref{sec:pre-selection}. 
A more refined selection is then outlined in Section~\ref{sec:dilepton-selection}, with events categorised according to their flavour.

\section{Dataset}
\label{sec: dataset}
The dataset consists of a number of \emph{ATLAS runs} -- data acquisition periods recorded within specific fills of the LHC. 
Each run contains numerous luminosity blocks (LBs), each of which corresponds to about one minute of data collection.
As mentioned previously, not all events recorded by the ATLAS detector are used in the analyses; only events passing certain quality criteria are selected. 
Data quality criteria require all detector subsystems to be fully operational. 
The ``good'' LBs are stored in a \emph{good run list} (GRL), which is available for each data-taking year separately.
The GRL utilised in this thesis is valid for analyses employing muon triggers.
% Only events passing data quality requirements are selected by checking that they are present in the Good Run List (GRL): \verb|20230207/data22_13p6TeV.periodAllYear_DetStatus-v109-pro28-04_MERGED_PHYS_StandardGRL_All_Good_25ns_ignore_TRIGLAR.xml|.
The \pileup conditions of 2022-2023 data are shown in Figure~\ref{fig: mu_2022_2023}.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.7\textwidth]{figures/chapter_7/mu_2022_2023.pdf}
  \caption{Distribution of the mean number of interactions per crossing for the 2022-23 $pp$ collision data at 13.6 TeV centre-of-mass energy. 
  All physics data recorded by ATLAS during stable beams in Run~3 are shown (until 5 June 2023). Figure taken from~\cite{ATLASLumiTwiki}.}
  \label{fig: mu_2022_2023}
\end{figure}


\section{Pre-selection}
\label{sec:pre-selection}

This analysis employs single-lepton triggers for electrons or muons, as listed in Table~\ref{tab:EventSelection/Triggers}.
The single-object triggers vary in their requirements concerning object identification, isolation, and \pt thresholds.
Triggers with lower \pt requirements impose stricter identification and isolation criteria; on the other hand, triggers with higher \pt thresholds require looser (or no) identification and isolation criteria.
A logical OR is applied between the triggers, allowing events to be selected via either the electron or the muon trigger.
To operate within the plateau regime of the triggers, events must feature leptons with $\pt > 27~\gev$.
Additionally, the leptons that fired the trigger are required to be trigger-matched\footnote{Being trigger-matched means that the objects detected in real-time (online objects) and those analysed in detailed offline studies (offline objects) must have a small angular distance between each other.}.
Only events with a primary vertex~\cite{ATL-PHYS-PUB-2015-026}, reconstructed from at least two tracks with $\pt > 500~\mev$, originating from the beam collision region in the $x$--$y$ plane, are selected.

\begin{table}[!hbpt]
  \centering
    \begin{tabular}{ll}
    \toprule
    Electron triggers & Muon triggers \\
    \midrule
    HLT\_e26\_lhtight\_ivarloose\_L1EM22VHI & HLT\_mu24\_ivarmedium\_L1MU14FCH \\
    HLT\_e60\_lhmedium\_L1EM22VHI &  HLT\_mu50\_L1MU14FCH \\
    HLT\_e140\_lhloose\_L1EM22VHI & \empty \\
    \bottomrule
    \end{tabular}
    \caption{A list of triggers used in the analysis. Triggers are split between electron and muon flavours. A logical OR is applied between the triggers.}
    \label{tab:EventSelection/Triggers}
\end{table}

\FloatBarrier

\section{Dilepton selection}
\label{sec:dilepton-selection}
As stated in the previous chapters, three sub-channels are defined based on the flavours of the leptons: $ee$, $e\mu$ and $\mu\mu$.
Selected events must contain exactly two leptons (electrons or muons) of opposite electric charge.
Events from the $Z$ production contribute to the $e\mu$ selection only via $Z\to\tau\tau$ with leptonic decays of the tau leptons.
In the same-flavour channels, events are required to have the dilepton invariant mass, $m_{\ell\ell}$, in the $Z$-boson window, $66 < m_{\ell\ell} < 116~\gev$, to increase the purity of the events originating from a $Z$-boson.
No dilepton mass criteria are imposed on the $e\mu$ channel, but the events are required to have one or two $b$-tagged jets, as described in Section~\ref{sec:chapter_5}.
The $e\mu$ channel is solely utilised in the extraction of the \ttbar cross-section, owing to its naturally small background contribution.
The same-flavour channels serve both for comparison between data and prediction and for determining the ratio of the \ttbar and the $Z$-boson cross-sections.
Table~\ref{tab:EventSelection/Channels} summarises the selection criteria employed in the analysis. 
Table~\ref{tab:DilepYields} shows the event yields predicted and observed in the different channels; the $e\mu$ inclusive selection is only used in the simulation to calculate $C_b$.

\subsection{$e\mu$ channel}\label{sub:sele-emu}
Alongside the inclusive selection, events featuring at least one reconstructed jet with $\pt > 30~\gev$, which is also $b$-tagged using the DL1dv01 tagger at the 77\% efficiency working point, are utilised in the extraction of the \ttbar\ cross-section and $b$-jet efficiency.
Comparison of data and simulation is presented in Figures~\ref{fig:DataMCDilepEMu1}, \ref{fig:DataMCDilepEMu1}, and~\ref{fig:DataMCDilepEMu2}.
Given that the selection is inclusive in jets, distributions involving the leading jet necessitate that events have at least one reconstructed jet.
Figures~\ref{fig:DataMCDilepEMu1B1}, \ref{fig:DataMCDilepEMu1B1.1}, and~\ref{fig:DataMCDilepEMu1B2} show data to MC comparison for events with at least one $b$-tagged jet.

% This selection is pure in \ttbar events.
% Several \pt-related distributions show slopes in the data to prediction ratio.
% This has been seen at every centre-of-mass energy and it is, at least partially, related to the missing higher-order correction in the MC sample.

\subsection{Same-flavour channels}
The same-flavour channels have a large contribution from the $Z$(+jets) events.
An inclusive jet selection targets the $Z$-boson contribution, which is subsequently employed in the measurement of the cross-section ratio\footnote{Despite the selection being inclusive, jets are defined to ensure a consistent overlap-removal procedure with the $e\mu$ channel.}.
For the $ee$ channel, comparison of data and simulation is presented in Figures~\ref{fig:DataMCDilepEE2}-\ref{fig:DataMCDilepEE1}.
Additionally, Figure~\ref{fig:DataMCDilepEEjet} shows the distributions of $\pt$ and $\eta$ for the events with at least one jet with $\pt>30~\gev$.
Figure~\ref{fig:DataMCDilepEEbalance} shows the jet and $Z$-boson \pt\ balance distribution, together with the jet multiplicity.
For the $\pt$ balance distribution, only events with exactly one jet with $\pt > 30~\gev$ and $\Delta \phi(\text{jet}, \ell\ell) > 2.7$ are selected. 
Similarly, for the $\mu\mu$ channel, Figures~\ref{fig:DataMCDilepMUMU2}-\ref{fig:DataMCDilepMUMU1} show the basic kinematic distributions.
Figure~\ref{fig:DataMCDilepMUMUjet} shows the leading jet $\pt$ and $\eta$ distributions and Figure~\ref{fig:DataMCDilepMUMUbalance} shows the $\pt$ balance distribution together with the jet multiplicity; in both figures the same selection criteria are applied as in the $ee$ channel.

Several distributions show significant deviations between prediction and data, especially for high \pt.
However, the vast majority of events populate the low \pt portions of the distributions, where agreement between data and prediction is notably better.
Moreover, the jet- and \met-related distributions are not relevant for this measurement, as the selection is inclusive in jets and no \met requirements are imposed.
An additional check, documented in Appendix~\ref{app:dileptonPt}, has been performed by reweighting the dilepton \pt\ distribution to assess the impact on the yields.

\begin{table}[hbpt]
  \centering
  \small
  \resizebox{0.6\textwidth}{!}{
    \begin{tabular}{ccc}
    \toprule
    \textbf{Object} & \multicolumn{2}{c}{\textbf{Requirements}} \\
    \toprule
    \empty & $e\mu$ & $ee/\mu\mu$ \\
    \midrule
    Trigger & \multicolumn{2}{l}{Single-electron OR single-muon} \\
    Primary vertex & \multicolumn{2}{c}{$\ge1$ (with $\ge2$ tracks with $\pt\ge500$ \mev)} \\
    \midrule
    Leptons & == 2 & == 2  \\
    \midrule
    Jets & $\geq 1$ & $\geq 0$  \\
    $b$-tags (DL1dv01 77\%) & $\geq 1$ & $\geq 0$ \\
    \midrule
    $m_{\ell\ell}$ cut [\gev] & - & $66 < m_{\ell\ell} < 116$ \\
    \bottomrule
    \end{tabular}
  }
    \caption{Summary of the event selection used in the different channels of the analysis. Dash indicates that the given requirement is not applied.}
    \label{tab:EventSelection/Channels}
\end{table}


\begin{table}[hbpt]
    \centering
  \resizebox{\textwidth}{!}{
  \begin{tabular}{lr@{$\,\pm\,$}lr@{$\,\pm\,$}lr@{$\,\pm\,$}lr@{$\,\pm\,$}lr@{$\,\pm\,$}l}
    \toprule
    \textbf{Process} & \multicolumn{10}{c}{\textbf{Channel}} \\
    \midrule 
    \empty & \multicolumn{2}{c}{$e\mu$ inclusive} & \multicolumn{2}{c}{$e\mu$ 1 $b$} & \multicolumn{2}{c}{$e\mu$ 2 $b$} & \multicolumn{2}{c}{$ee$} & \multicolumn{2}{c}{$\mu\mu$} \\
    \midrule 
    $\ttbar$ &       $177\,000$ & $5\,000$ & $87\,400$ & $2\,600$ & $52\,700$ & $1\,600$ & $22\,300$ & $1\,200$ & $33\,500$ & $1\,900$ \\
    $Z\rightarrow ll$ &   $13\,100$ & $600$ & $270$ & $70$ & $14.2$ & $3.0$ & $7\,7490\,000$ & $230\,000$ & $14\,100\,000$ & $400\,000$ \\
    Single-top $tW$-channel & $19\,000$ & $9\,000$ & $10\,200$ & $700$ & $1\,720$ & $290$ & $2\,250$ & $120$ & $3\,410$ & $200$ \\
    Diboson &            $17\,000$ & $8\,000$ & $230$ & $120$ & $5.3$ & $3.0$ & $9\,000$ & $5\,000$ & $18\,000$ & $9\,000$ \\
    $t\bar{t}V$ &     $220$ & $110$ & $100$ & $50$ & $69$ & $35$ & $240$ & $120$ & $250$ & $130$ \\
    Fake/non-prompt leptons & $2\,800$ & $1\,400$ & $1\,100$ & $500$ & $500$ & $500$ & $6\,000$ & $6\,000$ & $6\,000$ & $6\,000$ \\
    \midrule
    Total prediction & $229\,000$ & $14\,000$ & $99\,200$ & $3\,000$ & $55\,000$ & $1\,700$ & $7\,780\,000$ & $230\,000$ & $14\,100\,000$ & $400\,000$ \\
    \midrule
    Data & \multicolumn{2}{c}{$222\,711$} & \multicolumn{2}{c}{$92\,385$} & \multicolumn{2}{c}{$50\,956$} & \multicolumn{2}{c}{$7\,812\,978$} & \multicolumn{2}{c}{$14\,242\,875$} \\
    \bottomrule
  \end{tabular}
  }
  \caption{Predicted and observed event yields in the different dilepton channels
    after event selection. Systematic uncertainties, representing all considered sources, are shown for the predicted yields.}
  \label{tab:DilepYields}
  \end{table}

\newpage

% \section{Comparison of data and prediction}\label{sec:dataMCplots}

% \begin{figure}[htbp]
%   \centering
%   \subfloat[]{\includegraphics[width=0.49\textwidth]{figures/chapter_7/bjet_n_77_emu}}
%   \subfloat[]{\includegraphics[width=0.49\textwidth]{figures/chapter_7/lepton1_pt_log_emu}} \\
%   \subfloat[]{\includegraphics[width=0.49\textwidth]{figures/chapter_7/lepton1_pt_log_ee}}
%   \subfloat[]{\includegraphics[width=0.49\textwidth]{figures/chapter_7/lepton1_pt_log_mumu}} \\
%   \subfloat[]{\includegraphics[width=0.49\textwidth]{figures/chapter_7/dilepton_m_peak_ee}}
%   \subfloat[]{\includegraphics[width=0.49\textwidth]{figures/chapter_7/dilepton_m_peak_mumu}}
 
%   \caption{Comparison of observed data and predictions for (a) the distribution
%     of the number of $b$-tagged jets in the inclusive $e\mu$ channel,
%     (b) the \pt\ of the leading lepton in the $e\mu$ channel with at least one
%     $b$-tagged jet,
%     (c) the leading lepton~\pt in the $ee$ channel, (d) the leading lepton~\pt in the $\mu\mu$ channel,
%     invariant mass of the dilepton system in (e) the $ee$ channel and (f)
%     the $\mu\mu$ channel. The expected yields are calculated by normalising
%     the MC prediction to the product of integrated luminosity and predicted
%     cross section for each process. The ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
%     The ``Misid. leptons'' label represents fake and
%     non-prompt leptons.  The hashed band represents the total
%     uncertainty. The bottom panel shows the ratio of data to the
%     prediction. The rightmost bins contain the overflow events.}
%     \label{fig:DataMC1}
% \end{figure}


% \subsection{$e\mu$ channel}

\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/jet1_pt_emu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/jet1_eta_emu.pdf}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/jet2_pt_emu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/jet2_eta_emu.pdf}}
  \caption{Comparison of data and prediction in the $e\mu$ channel for the leading jet \pt (a),
  the leading jet $\eta$ (b),
  the sub-leading jet \pt\ (c), and
  the sub-leading jet $\eta$ (d).
  % The ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The ``Misid. leptons'' label represents fake and non-prompt leptons.
  The last bin contains overflow events. 
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepEMu1}
\end{figure}

\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/lepton1_pt_emu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/lepton1_eta_emu.pdf}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/lepton2_pt_emu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/lepton2_eta_emu.pdf}}
  \caption{Comparison of data and prediction in the $e\mu$ channel for the leading lepton \pt\ (a),
  the leading lepton $\eta$ (b),
  the sub-leading lepton \pt\ (c), and
  the sub-leading lepton $\eta$ (d).
  % The ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The ``Misid. leptons'' label represents fake and non-prompt leptons.
  The last bin contains overflow events. 
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepEMu1.1}
\end{figure}
  
\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/HT_emu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/jet_n_emu.pdf}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/bjet_n_77_emu.pdf}} 
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/met_emu.pdf}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/dilepton_deltaPhi_emu.png}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU/dilepton_m_emu.pdf}}
  \caption{Comparison of data and prediction in the $e\mu$ channel for the scalar sum of transverse momenta $\HT^{\text{jets}}$ (a),
  the jet multiplicity (b),
  the $b$-jet multiplicity (c),
  the missing energy \met (d),
  the $\Delta\phi$ between dilepton pairs (e), and
  the invariant mass of the dilepton pair (f).
  % The ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The ``Misid. leptons'' label represents fake and non-prompt leptons.
  The last bin contains overflow events. 
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepEMu2}
\end{figure}
  
\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/jet1_pt_emu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/jet1_eta_emu.pdf}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/jet2_pt_emu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/jet2_eta_emu.pdf}}
  \caption{Comparison of data and prediction in the $e\mu$ channel for the leading jet \pt (a),
  the leading jet $\eta$ (b),
  the sub-leading jet \pt\ (c), and
  the sub-leading jet $\eta$ (d).
  Events with at least one $b$-tagged jet are selected. 
  % The ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The ``Misid. leptons'' label represents fake and non-prompt leptons.
  The last bin contains overflow events. 
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepEMu1B1}
\end{figure}

\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/lepton1_pt_emu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/lepton1_eta_emu.pdf}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/lepton2_pt_emu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/lepton2_eta_emu.pdf}}
  \caption{Comparison of data and prediction in the $e\mu$ channel for the leading lepton \pt\ (a),
  the leading lepton $\eta$ (b),
  the sub-leading lepton \pt\ (c), and
  the sub-leading lepton $\eta$ (d).
  Events with at least one $b$-tagged jet are selected. 
  % The ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The ``Misid. leptons'' label represents fake and non-prompt leptons.
  The last bin contains overflow events. 
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepEMu1B1.1}
\end{figure}
  
\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/HT_emu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/jet_n_emu.pdf}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/bjet_n_77_emu.pdf}} 
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/met_emu.pdf}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/dilepton_deltaPhi_emu.png}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EMU1B/dilepton_m_emu.pdf}}
  \caption{Comparison of data and prediction in the $e\mu$ channel for the scalar sum of transverse momenta $\HT^{\text{jets}}$ (a),
  the jet multiplicity (b),
  the $b$-jet multiplicity (c),
  the missing energy \met (d),
  the $\Delta\phi$ between dilepton pairs (e), and
  the invariant mass of the dilepton pair (f).  
  Events with at least one $b$-tagged jet are selected. 
  % The ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The ``Misid. leptons'' label represents fake and non-prompt leptons.
  The last bin contains overflow events. 
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepEMu1B2}
\end{figure}
  

\FloatBarrier
% \subsection{$ee$ and $\mu\mu$ channels in $Z$ mass window}

\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EE/dilepton_deltaPhi_ee.png}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EE/dilepton_deltaR_ee.png}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EE/dilepton_m_peak_ee.pdf}}
  \caption{Comparison of data and prediction in the $ee$ channel for various observables. The last bin contains overflow events. 
  The ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepEE2}
\end{figure}

\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EE/lepton1_pt_log_ee.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EE/lepton2_pt_log_ee.pdf}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EE/lepton1_eta_ee.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EE/lepton2_eta_ee.pdf}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EE/HT_log_ee.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EE/met_log_ee.pdf}}
  \caption{Comparison of data and prediction in the $ee$ channel for various observables. The last bin contains overflow events. The
  ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepEE1}
\end{figure}

\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EE/jet1_pt_log_ee.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EE/jet1_eta_ee.pdf}}
  \caption{Comparison of data and prediction in the $ee$ channel for the leading jet distributions. Only events with at least one jet with $\pt > 30~\gev$ are selected. The last bin contains overflow events. The
  ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepEEjet}
\end{figure}

\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/EE/jet_Z_balance_pt_30_50_ee.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/jet_n_log_ee.pdf}}
  \caption{Comparison of data and prediction in the $ee$ channel for the jet-$Z$ $\pt$ balance (a), and for the jet multiplicity (b). 
  Event with exactly one jet with $\pt>30~\gev$ and $\Delta \phi(\text{jet}, \ell\ell) > 2.7$ are selected. The last bin contains overflow events. The
  ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepEEbalance}
\end{figure}

\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/MUMU/dilepton_deltaPhi_mumu.png}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/MUMU/dilepton_deltaR_mumu.png}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/MUMU/dilepton_m_peak_mumu.pdf}}
  \caption{Comparison of data and prediction in the $\mu\mu$ channel for various observables. The last bin contains overflow events. The
  ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepMUMU2}
\end{figure}

\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/MUMU/lepton1_pt_log_mumu.pdf}} 
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/MUMU/lepton2_pt_log_mumu.pdf}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/MUMU/lepton1_eta_mumu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/MUMU/lepton2_eta_mumu.pdf}} \\
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/MUMU/HT_log_mumu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/MUMU/met_log_mumu.pdf}}
  \caption{Comparison of data and prediction in the $\mu\mu$ channel for various observables. The last bin contains overflow events. The
  ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepMUMU1}
\end{figure}

\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/MUMU/jet1_pt_log_mumu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/MUMU/jet1_eta_mumu.pdf}}
  \caption{Comparison of data and prediction in the $\mu\mu$ channel for the leading jet distributions. Only events with at least one jet with $\pt > 30~\gev$ are selected. The last bin contains overflow events. The
  ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepMUMUjet}
\end{figure}

\begin{figure}[!htb]
  \centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/MUMU/jet_Z_balance_pt_30_50_mumu.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/chapter_7/jet_n_log_mumu.pdf}}
  \caption{Comparison of data and prediction in the $\mu\mu$ channel for the jet-$Z$ $\pt$ balance (a), and for the jet multiplicity (b). Event with exactly one jet with $\pt>30~\gev$ and $\Delta \phi(\text{jet}, \ell\ell) > 2.7$ are selected. The last bin contains overflow events. The
  ``Other SM'' represents all non-$Z$-boson SM processes merged into a single contribution.
  The hashed band represents the total systematic uncertainty. 
  The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:DataMCDilepMUMUbalance}
\end{figure}

\FloatBarrier

\section{Comparison with previous measurements}
\subsection*{Event selection in ATLAS-CONF-2022-070}
Due to the early stage of the analysis, a sizeable difference is observed between the efficiency for electron triggers in the forward region. 
Thus, events with electrons $|\eta| < 2.37$ are excluded in this analysis.

\FloatBarrier