\chapter{The Standard Model}
\label{sec:chapter_1}

The SM of elementary particles represents the current state of knowledge regarding the fundamental constituents of the Universe and their ways of interacting.
This theory is founded on two essential principles: gauge theories that are rooted in the $SU(3)_c\otimes SU(2)_L \otimes $U(1)$_Y$\footnote{In this notation, $c$ denotes the colour charge, while $L$ and $Y$ respectively 
represent the action of the $SU(2)$ Lie algebra on left-handed chiral fields and the weak hypercharge $Y=2(Q-T_3)$\cite{Gell-Mann1956}; here, $Q$ is the electric 
charge and $T_3=\frac{1}{2}\sigma_3$ is the third component of the isospin, which is a quantum number of the weak interaction.} gauge group, 
which provide a comprehensive depiction of both strong and electroweak interactions, along with the mechanism of Spontaneous Symmetry Breaking (SSB). 
These gauge theories provide a well-defined mechanism for adding interaction terms to the free-particle Lagrangian through the exploitation of local transformations depending on four-position coordinates.
The aim of the SM is to provide a unified theoretical description of the three fundamental interactions (strong, weak, and electromagnetic, the last two being united in a single Electroweak (EW)
interaction), which are predominant at the particle physics scales\footnote{The gravitational force cannot yet be included in the framework of the SM at present.}.

The model successfully describes the interaction between the fundamental spin-$\frac{1}{2}$ constituents of matter, called fermions, through the exchange of spin-1 gauge bosons. 
Fermions, obeying Fermi-Dirac statistics, are classified into leptons and quarks, whereas bosons are governed by Bose-Einstein statistics. 
Among the five elementary bosons, four are spin-1 \emph{vector bosons}, and one is a spin-0 \emph{scalar boson}; the properties of bosons are further described in the next sections.

The fermion sector is organised into three generations, as shown in Figure~\ref{fig:SM_table}. Out of the $3\times4$ elementary fermionic building blocks of matter (together with their anti-matter counterparts), six are charged or neutral leptons. 
The charged ones ($e^-, \mu^-, \tau^-$) are able to undergo both electromagnetic and weak interactions, while neutral leptons ($\nu_e, \nu_{\mu}, \nu_{\tau}$) called neutrinos, are solely subject to the weak force.
Each lepton is assigned a quantum number called lepton flavour \cite{GRIBOV1969493}. 
Its exact conservation in every process involving leptons accidentally follows from gauge invariance and the assumption that neutrinos are massless. 
However, due to the observation of neutrino oscillations \cite{Ahmad:2001an, Ahmad:2002jz, Fukuda:1998mi}, the conservation law of this quantum number is in fact only a very good approximation
\footnote{Total lepton and baryon numbers are, separately, exactly and accidentally conserved, so far, from experimental constraints on the proton decay lifetime.}. \par

The remaining six elementary fermions are the so-called quarks \cite{GELLMANN1964214}.
Quarks exist in six flavours, up ($u$), down, ($d$), charm ($c$), strange ($s$), top ($t$) and bottom ($b$); they carry a fractional charge, expressed in units of the positron charge. 
In addition to the flavour quantum number, quarks are also characterised by a colour state (blue, red or green); this quantum number causes the quarks to be confined in colour-neutral bound states called hadrons. 
This concept will be further described in Sec.~\ref{sect: QCD}.

\begin{figure}
	\centering
	\includegraphics[trim={1cm 1cm 1cm 1cm},clip,width=0.8\textwidth]{figures/chapter_1/SM}
	\caption{Overview of the elementary particles and fields in the Standard Model. Figure taken from~\cite{SM_fig}. Adapted the top quark mass according to~\cite{TOPQ-2017-03}
	and the Higgs boson mass according to~\cite{Workman:2022ynf}.}
	\label{fig:SM_table}
\end{figure}

 The SM also describes the interactions between particles via the three previously mentioned forces; 
 all these interactions are mediated by the exchange of gauge bosons. 
 The forces are described with a Quantum Field Theory (QFT) that is based on a Lie algebra stating the symmetry of the interactions, 
 and are explained in the following sections.

 The Lagrangian of the SM can be constructed by requiring it to be gauge-invariant, local, and renormalisable. It can be divided in several parts:
 \begin{equation}\label{math: SM lagrangian}
	\mathcal{L}_{\mathrm{SM}} = \mathcal{L}_{\mathrm{gauge}} + \mathcal{L}_{\mathrm{fermions}} + \mathcal{L}_{\mathrm{Higgs}} + \mathcal{L}_{\mathrm{Yukawa}}
\end{equation}
The first term describes gauge fields and their self-interactions, while the second term represents fermions and their interplay with corresponding gauge fields; both of these subjects are further discussed in Section~\ref{sect: gauge theories}. 
The third term concerns the Higgs field and its self-interactions; together with the fourth term, it is responsible for the SSB, a process responsible for the creation of mass of the elementary particles, as reported in Sec.~\ref{sect:higgs}.
Finally, the fourth term outlines the interactions of fermions with the Higgs field. 
Crucial for the generation of mass for fermions, this term's implications are studied in Section~\ref{sect: Fermions masses}.

\section{Gauge theories}\label{sect: gauge theories}
A pivotal advancement in constructing the SM involved the formulation of a method to derive interaction theories from theories without interaction within the framework of QFT.

\subsection{Quantum electrodynamics}
The concept of gauge invariance arises from the necessity for the Lagrangian to be independent of the phase of complex-valued fields.\cite{10.2307/j.ctt7snx1}. The formulation of this symmetry principle goes back to the electromagnetism, where the physical $\mathbf{E}$ and $\mathbf{B}$ fields are obtained from the scalar and vector potentials, respectively $\phi$ and $\mathbf{A}$ and do not change under the transformations
$$ \phi\rightarrow\phi'=\phi -\dfrac{d\chi}{dt} \qquad \text{and} \qquad \mathbf{A}\rightarrow\mathbf{A}=\mathbf{A}+\nabla\chi $$
or, in 4-vector notation:
\begin{equation}
A_{\mu}\rightarrow A_{\mu}'=A_{\mu}+\partial_{\mu} \chi.
\end{equation}
In relativistic quantum mechanics, the gauge invariance of electromagnetism is related to a local phase transformation that must leave the physics unmodified:
\begin{equation}
\psi(x)\rightarrow \psi'(x)=\hat{U}(x)\psi(x)=e^{iq\chi(x)}\psi(x)
\label{math: $U(1)$ transformation}
\end{equation}
where the phase $\chi(x)$ can potentially assume different values at all points in space-time; if $\chi(x)$ is not constant in the whole space-time, this is the case of a \emph{local} phase invariance. 
To the contrary, if $\chi(x)=\chi$ the phase invariance is \emph{global}. 

The Maxwell equations in their covariant form for a free electromagnetic (EM) field are written as:
\begin{equation}
	\partial_{\mu}F_{\mu\nu}=0\,, \; \text{with} \;\; F_{\mu\nu}=\partial_{\mu}A_{\nu}-\partial_{\nu}A_{\mu}
	\label{math: EM Lagrangian}
\end{equation}
where the EM field strength tensor $F_{\mu\nu}$ is gauge invariant (if a global phase transformation is considered). Consequently, the Maxwell field equations are both gauge-invariant and Lorentz-covariant. 
The EM Lagrangian Density (referred to as Lagrangian from now on) is then given by:
\begin{equation}
\mathcal{L}_{\mathrm{EM}}=-\dfrac{1}{4}F_{\mu\nu}F^{\mu\nu}
\end{equation}


Similarly, the expression of the Dirac Lagrangian of a fermion requiring the invariance under a global $U(1)$ symmetry is\footnote{The set of all transformations $\hat{U}=e^{iq\chi(x)}$ such as \eqref{math: $U(1)$ transformation}, constitute the unitary Abelian group $U(1)$.}:
\begin{equation}
\mathcal{L}_{\mathrm{D}} =\overline{\psi}(x)(i\gamma^{\mu}\partial_{\mu}-m)\psi(x)
\label{math: Dirac eq fermion global $U(1)$}
\end{equation}
In \eqref{math: Dirac eq fermion global $U(1)$}, the Dirac spinor $\psi(x)$ is defined as $\psi(x)=(\psi_1(x),\dots,\psi_4(x))^T$, while $\overline{\psi}(x)$ is the complex-valued $\psi(x)$ such that $\overline{\psi}(x)=\psi^{\dagger}(x)\gamma^0$, $m$ is the fermion mass and $\gamma^{\mu}$ the Dirac matrices. \par
 To the contrary, for a local phase transformation $\mathcal{L}_{\mathrm{D}}$ has to be modified by coupling the Dirac field to the Maxwell field in order to preserve invariance:
\begin{equation}
\mathcal{L}_{\mathrm{D,EM}} =\overline{\psi}(x)[i\gamma^{\mu}D_{\mu}-m]\psi(x)
\label{math: Dirac eq fermion local $U(1)$}
\end{equation}
where the partial derivative has been substituted by the \emph{gauge covariant derivative} following the definition:
\begin{equation}\label{math: gauge covariant derivative}
\partial_{\mu}\rightarrow D_{\mu}=\partial_{\mu}-ieA_{\mu}
\end{equation}
 In \eqref{math: gauge covariant derivative}, $A_{\mu}$ is interpreted as the field corresponding to a \emph{massless gauge boson}. 
 The existence of a gauge field which couples to Dirac particles in exactly the same way as the photon implies a fundamental statement: quantum electrodynamics (QED), including Maxwell equations can be obtained demanding the invariance of physics under a local $U(1)$ transformation.  \bigskip \par
 The complete gauge invariant QED Lagrangian is therefore given by:
\begin{equation}
\mathcal{L}_{\mathrm{QED}}=-\dfrac{1}{4}F_{\mu\nu}F^{\mu\nu}+\overline{\psi}(x)[i\gamma^{\mu}D_{\mu}-m]\psi(x)
\label{math: $U(1)$ QED Lagrangian}
\end{equation}
where the \emph{interaction term} is defined by $$ \mathcal{L}_{\mathrm{int}} = e \overline{\psi}\gamma^{\mu}A_{\mu}\psi $$

\subsection{Electroweak interaction}
The local gauge invariance principle can be applied to include weak interactions in the gauge invariant Lagrangian formalism. 
The EW theory \cite{PhysRevLett.19.1264,SALAM1964168,GLASHOW1961579} unifies the EM interaction and the weak interaction through the $SU(2)_{L}\otimes U(1)_Y$ group,
which includes both charged and neutral currents. The charged currents change the flavour of the left-handed fermion fields, whereas neutral currents conserve the flavour.
In the notation $SU(2)_{L}$, the subscript $L$ denotes that the weak isospin current interacts solely with left-handed fermions;
this is a direct consequence of the hypothesis that parity is not conserved\footnote{This statement implies that the Lagrangian might be a pseudoscalar.} in weak interactions \cite{YangLee:1956}.
The correctness of this hypothesis has been proved experimentally \cite{Wu:1957}, leading to the conclusion that parity must be \emph{maximally violated}.
Maximal parity violation is accomplished by introducing a \emph{vector minus axial} ($V-A$) structure of the weak theory; this implies right-handed and left-handed spinors defined as:
\begin{equation}\label{math: l-r handed spinors}
	\begin{aligned}
		\psi_R = P_R\psi=\frac{1}{2}(1+\gamma^5)\psi \\
		\psi_L = P_L\psi=\frac{1}{2}(1-\gamma^5)\psi
	\end{aligned}
\end{equation}
In \eqref{math: l-r handed spinors}, $P_{R,L}$ are the \emph{chirality operators}, and $\gamma^5$ is the product of the Dirac matrices.
Hence, fermions are characterised as of left-handed doublets of quarks $q^i_L$ and leptons $\ell^i_L$, 
along with right-handed singlets of quarks $u^i_R(d^i_R)$ and leptons $\ell^i_R(\nu^i_R)$:

\begin{equation}\label{math: l-r handed fermions}
	\begin{alignedat}{2}
		&q^i_L = \begin{pmatrix} u \\ d \end{pmatrix}_L\begin{pmatrix} c \\ s \end{pmatrix}_L\begin{pmatrix} t \\ b \end{pmatrix}_L; \qquad &&\ell^i_L = \begin{pmatrix} e \\ \nu_e \end{pmatrix}_L\begin{pmatrix} \mu \\ \nu_{\mu} \end{pmatrix}_L\begin{pmatrix} \tau \\ \nu_{\tau} \end{pmatrix}_L; \\
		&u^i_R = u_R,c_R,t_R; \qquad &&d^i_R = d_R,s_R,b_R; \\
		&\ell^i_R = e_R,\mu_R,\tau_R; \qquad &&\nu^i_R = \nu^e_R,\nu^{\mu}_R,\nu^{\tau}_R;
	\end{alignedat}
\end{equation}

In doublets, both neutrinos and up-type quarks $(u, c, t)$ carry a weak isospin value of $T_3 = +\frac{1}{2}$. 
Conversely, charged leptons and down-type quarks $(d, s, b)$ possess a weak isospin of $T_3 = -\frac{1}{2}$. 
As a result, members of the same doublet hold identical hypercharges: leptons hold $Y = -1$, while quarks carry $Y = \frac{1}{3}$. 
This hypercharge is inferred from the multiplication of the two symmetry groups. 

The $SU(2)_L \otimes U(1)_Y$ gauge group is incompatible with mass terms for either the gauge bosons or fermions without causing a violation of gauge invariance. 
To incorporate the observed masses, spontaneous electroweak symmetry breaking (EWSB) is employed at energies around the mass scale of the $W$ and $Z$ bosons. 
This process, often referred to as the ``Higgs mechanism" \cite{PhysRevLett.13.508, HIGGS1964132, PhysRevLett.13.321}, includes the introduction of an $SU(2)_L$ doublet of complex scalar fields $\phi=(\phi^+,\phi^0)^T$. 
As demonstrated in Section~\ref{sect:higgs}, when the neutral component achieves a non-zero vacuum expectation value, the symmetry of $SU(2)_L \otimes U(1)_Y$ breaks down to $U(1)_{\mathrm{QED}}$. 
This breakdown imparts mass to the three electroweak gauge bosons, $W^{\pm}$ and $Z^0$, while the photon remains massless. This process preserves the electromagnetic symmetry $U(1)_{\mathrm{QED}}$. 
The residual degree of freedom from the scalar doublet produces an additional scalar particle, known as the Higgs boson.

\subsection{Quantum Chromodynamics}
\label{sect: QCD}
The SM section that accounts for strong interactions between quarks is called Quantum Chromodynamics (QCD). 
Similarly to QED, QCD interactions are mediated by eight massless gluons corresponding to the eight generators of the $SU(3)_c$ local gauge symmetry. 
The single charge of the QED is replaced by ``colour'' charges \emph{r, g} and \emph{b} which correspond to three orthogonal states in $SU(3)_c$ space.

The development of QCD began with the \emph{Eightfold Way} classification of baryons and mesons \cite{PhysRev.125.1067}, 
followed by the introduction of the hadron quark content \cite{GELLMANN1964214,Zweig:1964jf}. 
The study of this inner structure, achieved through the Deep Inelastic Scattering (DIS) \cite{Abramowicz2015} technique, has led to the formulation 
of the \emph{Parton Model} \cite{Feynman:1969wa}.

The quarks' dynamics is generated by a colour symmetry \cite{FRITZSCH1973365} in the SU(3)$_c$ symmetry group; 
this framework is a non-Abelian gauge theory, so that the interaction mediators can carry colour charge themselves and couple to each other. 

At large momentum scale $Q^2\gg 1$ (GeV/$c)^2$ the coupling $\alpha_s$ diminishes, a phenomenon termed \emph{Asymptotic freedom}~\cite{DAVIDPOLITZER1974129}, 
implying that quarks and gluons are weakly coupled. To the contrary, $\alpha_s$ is large for small values of $Q^2$: as a result, quarks are 
bounded in the so-called \emph{colour confinement}. This fact has the important experimental consequence that quarks
produced in high energy particle interactions manifest themselves as collimated streams
of hadrons called \emph{jets}\footnote{The energy and direction of a jet are correlated to the energy
and direction of its parent quark. The process by which the quark evolves into a jet is
called \emph{hadronisation}, and consists of a parton shower, which can be perturbatively calculated,
and a fragmentation process, which is a non-perturbative process modelled using
Monte Carlo (MC) techniques. The treatment of jets is further inspected in Section~\ref{sec: jets-reco}.}. 

The Lagrangian that describes the strong interaction is given by:
\begin{equation}\label{math: QCD lagrangian}
\mathcal{L}_{\mathrm{QCD}}=-\frac{1}{4}G_{\mu\nu}^a(G^{\mu\nu})_a+\sum_{k=1}^{n_f}\overline{\psi}_k\,i\gamma^{\mu}(D_{\mu}-m_k)\psi_k,
\end{equation}
 where the index $k$ stands for the flavour of the quark. The covariant derivative $D_{\mu}$ is defined as:
\begin{equation}\label{SU(3) covariant derivative}
D_{\mu}=\partial_{\mu}-ig_sT_aG_{\mu}^a
\end{equation}
 while $G_{\mu\nu}^a$ represents the gluon field (with $a=1\dots 8$):
\begin{equation}\label{math: gluon field}
G_{\mu\nu}^a=\partial_{\mu}G_{\nu}^a-\partial_{\nu}G_{\mu}^a+g_sf^{abc}G_{\mu}^bG_{\nu}^c.
\end{equation}
 The terms $f^{abc}$ are the structure constants of $SU(3)_c$ and $T_a$ are the eight $SU(3)_c$ generators ($T^a~=~\frac{1}{2}~\lambda^a$, where $\lambda^a$ are the Gell-Mann matrices) undergoing the commutation relation $[T^a,T^b]=if^a_{bc}T^c$. The Lagrangian in \eqref{math: QCD lagrangian} has a built-in exact colour gauge symmetry, meaning that the gluon fields are massless. $\mathcal{L}_{\mathrm{QCD}}$ can be summarised as the contribution of three different parts:
\begin{equation}
\mathcal{L}_{\mathrm{QCD}}=\mathcal{L}_{\mathrm{G}}+\mathcal{L}_{\mathrm{q}}+\mathcal{L}_{\mathrm{int}},
\end{equation}
where $\mathcal{L}_{\mathrm{G}}=-\frac{1}{4}G_{\mu\nu}^a(G^{\mu\nu})_a$ is the massless gluon field term, $\mathcal{L}_{\mathrm{q}}=\sum_{k=1}^{n_f}\overline{\psi}_k\,i\gamma^{\mu}(\partial_{\mu}-m_k)\psi_k$ represents the massive quark fields and the interaction between quark currents and gluon fields is encoded in $\mathcal{L}_{\mathrm{int}}=J_a^{\mu}G_{\mu}^a$ with $J_a^{\mu}=\alpha_s\sum_{k=1}^{n_f}\overline{\psi}_k\,\gamma^{\mu}T^a\psi_k$.

\section{Spontaneous Symmetry Breaking and \\ Higgs~Mechanism }\label{sect:higgs}
A Lagrangian is composed of two parts. The first part involves the derivatives of the fields, which is referred to as the kinetic term. The second part, called the potential, is expressed in terms of the fields themselves. \par
Within the EW framework, the Higgs mechanism is incorporated in the $SU(2)_L \otimes $U(1)$_Y$ local gauge symmetry. 
Let's consider two complex scalar fields arranged in a weak isospin doublet:
\begin{equation}
\phi=\begin{pmatrix}
\phi^+\\
\phi^0
\end{pmatrix}=
\frac{1}{\sqrt{2}}\begin{pmatrix}
\phi_1+i\phi_2\\
\phi_3+i\phi_4
\end{pmatrix}
\end{equation}
 which is subject to a potential of the form:

\begin{equation}\label{math: higgs potential}
V(\phi)=\frac{1}{2}\mu^2\phi^{\dagger}\phi+\frac{1}{4}\lambda(\phi^{\dagger}\phi)^2;
\end{equation}
%
 the corresponding Lagrangian is:
\begin{equation}\label{math: Higgs lagrangian}
	\mathcal{L}_{\mathrm{Higgs}}=(\partial_{\mu}\phi)^{\dagger}(\partial^{\mu}\phi)-V(\phi).
\end{equation}
The quadratic term in the field can be interpreted as a mass term, while the $\phi^4$ term is identified as self-interactions of the scalar field.

A distinction arises from the sign of $\mu^2$. Specifically, $\lambda$ must be positive to ensure the potential is bounded from below. However, $\mu^2$ can be either positive or negative, which leads to different shapes of the potential.
 When $\mu^2<0$, $V(\phi)$ exhibits a set of degenerate minima satisfying:
$$ \phi^{\dagger}\phi=\frac{\varv^2}{2}=-\frac{\mu^2}{2\lambda}. $$
 The no longer unique choice of the potential minimum (called \emph{vacuum state}) produces a \emph{spontaneous symmetry breaking} of the Lagrangian.\par
 Since it is necessary that after SSB the neutral photon remains massless, the minimum of the potential must correspond to a non-zero vacuum expectation value (VEV), $\varv$, only for $\phi^0$:
\begin{equation}\label{math: vev}
\bra{0}\phi\ket{0}=\frac{1}{\sqrt{2}} \begin{pmatrix}
0\\
\varv
\end{pmatrix}.
\end{equation}
The fields can then be expanded around the minimum as follows:
\begin{equation}
\phi(x)=\frac{1}{\sqrt{2}}\begin{pmatrix}
\phi_1(x)+i\phi_2(x)\\
\varv+ \eta(x)+i\phi_4(x)
\end{pmatrix}.
\end{equation}
As described before, after the SSB a massive scalar field and three massless gauge Goldstone\footnote{According to the Goldstone theorem, for every independent transformation of the symmetry group that does not 
preserve the fundamental state, a massless particle originates in the spectrum of field excitations; this particle takes the name of Goldstone boson.} 
bosons appear.
These Goldstone bosons are absorbed by the gauge fields corresponding to the $W^{\pm}$ and $Z^0$ bosons, providing them with the longitudinal polarisation states necessary for acquiring mass.\par
 Through the ``gauging away'' of the Goldstone fields the doublet can be written in the unitary gauge:
\begin{equation}\label{math: phi in unitary gauge}
\phi(x)=\frac{1}{\sqrt{2}}\begin{pmatrix}
0\\
\varv+ h(x)
\end{pmatrix}.
\end{equation}

Herein lies the essence of the \emph{Higgs mechanism}: the Goldstone fields are removed from the Lagrangian while the mass terms of gauge bosons and the Higgs particle itself arise. 
More than forty years after its prediction, a particle consistent with the Higgs boson was discovered \cite{HIGG-2012-27,CMS-HIG-12-028}, thus completing the SM framework.


\subsection{Bosons masses}\label{sect: Bosons masses}
The bosons mass terms previously mentioned can be identified by correcting the Lagrangian of \eqref{math: Higgs lagrangian} such that it is 
gauge invariant under the $SU(2)_L \otimes $U(1)$_Y$ symmetry group. This is achieved by replacing the partial derivative with the appropriate covariant term:
$$ \partial_{\mu}\rightarrow D_{\mu}=\partial_{\mu}+ig_W\mathbf{T}\cdot\mathbf{W}_{\mu} +ig'\frac{Y}{2}B_{\mu};$$
 
here, the gauge field $B_{\mu}$ couples with $Y$, $\mathbf{T}$ are the three generators of the  $SU(2)_L$ symmetry\footnote{In this notation, $\mathbf{T} = \frac{1}{2}\mathbf{\sigma}$, where $\mathbf{\sigma}$ are the Pauli matrices.}, and  $\mathbf{W}_{\mu}$ are the three weak gauge fields.

The expression of $(D_{\mu}\phi)^{\dagger}(D^{\mu}\phi)$ where $\phi$ is a Higgs doublet
 \footnote{This particular doublet is such that the lower component is neutral and has $I_W^{(3)}=-\frac{1}{2}$; thus, it has hypercharge $Y=1$. 
 Furthermore, the dependence on $x$ of $\phi$ is neglected in the notation.} 
 expressed in the unitary gauge as in \eqref{math: phi in unitary gauge} is:

\begin{equation}\label{math: higgs mass step 1}
\begin{split}
(D_{\mu}\phi)^{\dagger}(D^{\mu}\phi)=&\left|\left(\partial_{\mu}  -ig_W\frac{\tau^{k}}{2}W_{\mu}^{k} -i\frac{g'}{2}B_{\mu}  \right)\phi\right|^2=\\
=&\frac{1}{2}(\partial_{\mu}h)(\partial^{\mu}h)+\frac{1}{8}g_W^2(W^{(1)}_{\mu}+iW^{(2)}_{\mu})(W^{(1)\mu}-iW^{(2)\mu})(\varv+h)^2\\
&\frac{1}{8}(g_W W^{(3)}_{\mu} -g'B_{\mu})(g_WW^{(3)\mu}-g'B^{\mu})(\varv+h)^2.
\end{split}
\end{equation}

Since the bosons masses are given by the terms in \eqref{math: higgs mass step 1} which are quadratic in the boson fields, in the Lagrangian the mass terms for the $W^{(1)}$ and $W^{(2)}$ fields will be:
$$ \frac{1}{2}m_W^2W^{(k)}_{\mu}W^{(k)\mu} \;\; \text{with} \;\; k=1,2 $$
This means that:
\begin{equation}
m_W=\frac{1}{2}g_Wv
\end{equation}

Taking now the terms which are quadratic in the $W^{(3)}$ and $B$ fields, one gets:
\begin{equation}\label{math: higgs mass step 2}
\frac{\varv^2}{8}(g_W W^{(3)}_{\mu} -g'B_{\mu})(g_WW^{(3)\mu}-g'B^{\mu})=\frac{\varv^2}{8}\begin{pmatrix}
W^{(3)}_{\mu} & B_{\mu}
\end{pmatrix}
\begin{pmatrix}
g_W^2 & -g_Wg'\\
-g_Wg'& g'^2
\end{pmatrix}
\begin{pmatrix}
W^{(3)}_{\mu} \\ 
B_{\mu}
\end{pmatrix},
\end{equation}
where the matrix containing the coefficients $g_W$ and $g'$ is called \emph{mass matrix} $\mathbf{M}$.\par
 Here the physical boson fields are obtained by computing the basis in which the mass matrix is diagonal, so after solving the characteristic equation $\det(\mathbf{M}-\lambda I)=0$, the expression of \eqref{math: higgs mass step 2} becomes:

\begin{equation}\label{math: higgs mass step 3}
\frac{\varv^2}{8}\begin{pmatrix}
 A_{\mu} & Z_{\mu}
\end{pmatrix}
\begin{pmatrix}
0 & 0\\
0 & g_W^2+g'^2
\end{pmatrix}
\begin{pmatrix}
A_{\mu} \\ 
Z_{\mu}
\end{pmatrix},
\end{equation}
where $A_{\mu}$ and $Z_{\mu}$ are physical fields corresponding to the eigenvectors of $\mathbf{M}$ which are summarised below, together with their eigenvalues:

\begin{eqnarray}\label{key}
Z_{\mu}=\frac{g'W^{(3)}_{\mu}+g_WB_{\mu}}{\sqrt{g_W^2+g'^2}} &\text{with}&\;\; m_Z=\frac{\varv}{2}\sqrt{g_W^2+g'^2}\\[10pt]
A_{\mu}=\frac{g'W^{(3)}_{\mu}-g_WB_{\mu}}{\sqrt{g_W^2+g'^2}} &\text{with}&\;\; m_A=0.
\end{eqnarray}


The parameters which regulate the EWSB of the SM are the two coupling constants $g_W$ and $g'$, together with the parameters $\lambda$ and $\mu$ of \eqref{math: higgs potential}, whose values are computed from the experimentally measured masses of the gauge and Higgs bosons \cite{Workman:2022ynf} to be $\lambda\simeq 0.129$ and $|\mu^2|\simeq(88.8 \text{ GeV})^2$. 
 
The mass of the Higgs boson, $m_H$, and the VEV are connected via parameters of the potential, $\mu$ and $\lambda$:
\begin{eqnarray}\label{math: higgs mass}
	m_H=\sqrt{\frac{\lambda}{2}}\varv \\
	\varv = \sqrt{\frac{\mu^2}{\lambda}}.
	\end{eqnarray}

\section{The Yukawa mechanism and the fermions masses}\label{sect: Fermions masses}
 Regarding fermions, the SM operates on the families of left-handed doublets and right-handed singlets of quarks and leptons; however the fact that the SU(2)$_L$ group acts only on the left-handed part of fermions constitutes an issue concerning their mass.
In fact, the mass-related term $ -m(\overline{\psi}_L\psi_R+\overline{\psi}_R\psi_L) $ that appears in the QED Lagrangian in~\eqref{math: $U(1)$ QED Lagrangian} is not gauge invariant, since it couples both the left-handed and right-handed components. 
A new interaction called \emph{Yukawa interaction} is then introduced, to mediate the coupling between the Higgs field and massless fermions via a coupling constant $y$.\newline
The corresponding piece of the Lagrangian is:

\begin{equation}\label{math: yukawa lagrangian}
	\mathcal{L}_{\mathrm{Y}}=-\Gamma^{ij}_u\overline{q}^i_L \epsilon\phi^* u^j_R -\Gamma^{ij}_d\overline{q}^i_L \phi d^j_R -\Gamma^{ij}_e\overline{\ell}^i_L \phi e^j_R + h.c\footnote{$\epsilon = i\sigma_2$ is the total antisymmetric tensor in two dimensions, related to the second Pauli matrix $\sigma_2$,
	required to ensure each term is separately electrically neutral.},
\end{equation}

where $\Gamma_u,\Gamma_d,\Gamma_e$ are $3\times 3$ complex Yukawa matrices (in family space) of the up- and down-type quarks, respectively, and $i,j$ are the generation labels. 
Following the same operation of ``gauging away'' previously shown in \eqref{math: phi in unitary gauge}, the physical states are obtained by diagonalising the Yukawa matrices in order to obtain the diagonal mass matrices for $f = u,d$:

\begin{equation}\label{math: fermion masses}
	m_f = y_i\frac{\varv}{\sqrt{2}},
	\end{equation}

where $y_i$ is the Yukawa coupling for the i-th fermion mass eigenstate. \newline
Since these matrices introduced in \eqref{math: yukawa lagrangian} do not need to be diagonal, mixing between different fermion generations is allowed.
In the quark sector, the mixing between the \emph{weak eigenstates} of the down-type quarks $d'$, $s'$ and $b'$, and the corresponding mass 
eigenstates $d$, $s$ and $b$, is described by the Cabibbo-Kobayashi-Maskawa (CKM) matrix \cite{cabibbo:1963,kobayashimaskawa:1973}:
\begin{equation}\label{math: CKM}
	\begin{pmatrix}
		d'\\s'\\b'
	\end{pmatrix} = 
	\begin{pmatrix}
		V_{ud} & V_{us} & V_{ub} \\ 
		V_{cd} & V_{cs} & V_{cb} \\ 
		V_{td} & V_{ts} & V_{tb} \\ 
	\end{pmatrix}
	\begin{pmatrix}
		d\\s\\b
	\end{pmatrix}.
\end{equation}

As per convention, only the down-type quarks undergo mixing, while the up-type mass matrix remains diagonal. 
The coupling strength of the $W^{\pm}$ boson to physical up- and down-type quarks are determined by the matrix elements. 

This unitary matrix contains diagonal entries close to unity, while the off-diagonal entries are roughly 0.2 between the first and second generation, about 0.04 between the second and third, and even smaller when transitioning from the first to the third generation \cite{Workman:2022ynf}.

The matrix element $V_{tb}$ is indirectly constrained by the unitarity of the CKM matrix, assuming three quark generations 
(and by directly measuring the single top production cross-section, see Section~\ref{sec: ttbar-theory}). 
The value is very close to 1: $|V_{tb}| > 0.999$ at a 90\% confidence level (C.L.). 
Therefore, the top quark in the SM predominantly couples to bottom quarks, which influences both the top quark production, 
suppressing the electroweak single top production mechanisms relative to pair production (see Section~\ref{sec: stop-theory}), and its decay, 
facilitating the isolation and reconstruction of top events by focusing on the presence of $b$-quark jets in the final state (see Section~\ref{sec: bjets-reco}).

Assuming that neutrinos are massless, no such mixing occurs in the lepton sector. 
However, experimental evidence shows that neutrinos possess mass, leading to the introduction of an analogous leptonic mixing matrix, the Pontecorvo-Maki-Nakagawa-Sakata (PMNS) matrix \cite{Pontecorvo:1967fh}. 
For the purposes of this thesis, any lepton sector mixing has no impact, thus a massless neutrino SM formulation is adopted.

In summary, the SM is a unitary, renormalisable theory, capable of perturbatively calculating processes at high energies. It includes 18 parameters that must be derived from measurements:

\begin{itemize}
\item 9 Yukawa couplings for the fermion masses,
\item 4 parameters for the CKM mixing matrix,
\item 3 coupling constants $\alpha_s$, $g_W$, $g'$ for $SU(3)_c$, $SU(2)_L$, and $U(1)_Y$, respectively,
\item 2 parameters from EWSB: $\varv$ and $m_H$.
\end{itemize}
At currently accessible energy scales, the SM successfully models the interactions of fundamental fermions and gauge bosons. 
Its predictions have been corroborated with high precision at recent colliders (SPS, LEP, Tevatron, and LHC). 

\section{Beyond the Standard Model}

Despite the undeniable success of the SM in predicting a plethora of phenomena, as also shown in Figure~\ref{fig:bsm} for what concerns LHC measurements, it does not account for several observed aspects of the Universe.
Evidence of non-baryonic matter that does not partake in electromagnetic interactions, often referred to as \emph{dark matter}, is one such phenomenon outside the design limits of the SM~\cite{Clowe_2006}. 
Moreover, the occurrence of baryogenesis in the early Universe necessitates the violation of baryon numbers, a process not accommodated within the SM's scope \cite{Sakharov_1991}.
Furthermore, evidence \cite{Riess_1998} for dark energy, an unknown form of energy that accelerates the Universe's expansion, is also beyond the SM's explanatory capacity.

Given these limitations, numerous theoretical models that extend beyond the Standard Model (BSM) have been proposed to elucidate these unexplained phenomena. 
Prominent among these is Supersymmetry (SUSY) \cite{PhysRevD.3.2415,Golfand:1971iw,WESS197452,WESS197439}, a theoretical construct that to every fermion in the SM associates a bosonic counterpart, and conversely, to every boson associates a fermionic counterpart. 
These partner particles differ in spin by half a unit. 
Additionally, supersymmetry implies the existence of a second Higgs doublet. 
It is theorised that the new particles with colour charge, such as the gluinos ($\tilde{g}$) and top squarks ($\tilde{t}$) — the supersymmetric counterparts of the gluon and top quark, respectively — should have masses around 1 TeV. 
This feature addresses one of the most critical unknowns of the SM, whenever this is thought as a low energy limit of some additional UV completion: the so-called \emph{hierarchy problem}. 
This problem pertains to the question of why the Higgs boson is substantially lighter than the scale at which radiative corrections are significant~\cite{MARTIN_1998}:
\begin{equation}
	\Delta m_H = \frac{|\lambda_f^2|}{8\pi^2}\Lambda_{\text{UV}}^2+\dots
\end{equation}
where $\lambda_f$ is the coupling between the Higgs field and a fermion, and $\Lambda_{\text{UV}}$ represents the typical mass scale of the UV completion, which is e.g. the Plank mass for gravity.
A boson partner to the top quark would stabilise the Higgs boson mass against these large corrections, provided its mass is close to the electroweak symmetry breaking energy scale.

Another popular way of investigating BSM physics is through models featuring flavour-changing neutral currents (FCNCs), which the SM prohibits at the tree level due to the Glashow-Iliopoulos-Maiani (GIM) suppression \cite{PhysRevD.2.1285}.
This suppression is evident in the rarity of processes like top quark decays to a quark and a neutral boson, $t \rightarrow Xq$, ($X=g, Z, \gamma$ or $H$; $q=u$ or $c$). 
However, several BSM theories predict an enhanced FCNC rate, potentially observable in experiments.
For instance, in models with multiple Higgs doublets or those incorporating additional quark families, FCNC effects can be significantly magnified. 

Each of these BSM theories provides predictions for a vast number of properties that precise measurements within the SM framework can either exclude or constrain.

\begin{figure}
	\centering
	\includegraphics[width=1.0\textwidth]{figures/chapter_1/bsm}
	\caption{Summary of several Standard Model cross-section measurements performed by ATLAS at the LHC. The measurements are corrected for branching fractions, compared to the corresponding theoretical expectations. 
	Figure taken from~\cite{ATLAS-SM-plots}.}
	\label{fig:bsm}
\end{figure}