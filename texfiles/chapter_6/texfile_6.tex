\chapter{Signal and background modelling}
\label{sec:chapter_6}

In high energy physics, measurements often make use of simulation for the involved physics processes, with simulated events being compared to the experimentally observed data.
The predictions of various physics models, including the SM, are typically entrusted to Monte Carlo generators.

The first stage of the simulation is the hard process, in which the constituents of the colliding particles interact at high momentum scale to produce outgoing fundamental objects as the quarks, leptons, and bosons from the SM, or potential new particles from BSM theories.
The partons generated by the hard process emit gluons, forming a parton shower; these parton shower constituents then transform into stable, colour-neutral hadrons in a process called hadronisation. 
Many of the hadrons produced are unstable and decay. 
In hadron-hadron collisions, the remaining partons of the incoming hadrons further interact, producing underlying events\footnote{An underlying event is a particle production process not associated with the hardest parton-parton process.}.
These simulation steps are schematically shown in Figure \ref{fig:event_simulation}.

After the event generation, the detector response is simulated by the toolkit \GEANT~\cite{Agostinelli:2002hh} with the full simulation of the ATLAS detector.
The simulated samples are processed using the same software framework as the data~\cite{SOFT-2010-01}.
All simulated processes that include a top quark have its mass set to $m_{\text{t}} = 172.5~\gev$.

The effect of multiple $pp$ interactions in the same bunch crossing (\emph{in-time \pileup}) and neighbouring ones (\emph{out-of-time \pileup}), is modelled by overlaying the original hard-scattering event with simulated inelastic $pp$ events generated by \PYTHIA~8.307~\cite{Sjostrand:2007gs}, 
using the NNPDF2.3 LO set of PDFs~\cite{Ball:2012cx} and parameter values set according to the A3 tune~\cite{ATL-PHYS-PUB-2016-017} for a particle with high \pt.
For the particles with low \pt, the EPOS~2.0.1.4~\cite{Porteboeuf:2010um} generator is used with the EPOS LHC tune.

\begin{figure}[!htbp]
  \centering                                                                                                                                                                        
  \includegraphics[width=0.7\textwidth]{figures/chapter_6/event_simulation.png}
  \caption[]{
  Schematic diagram of the event simulation process.
  The hard process simulation is indicated by red lines, while blue lines represent the parton shower. 
  Green lines and dots depict the creation of hadrons and their subsequent decay. 
  Purple lines correspond to the simulation of the underlying events. Figure taken from~\cite{Simulation_MC}. 
  \label{fig:event_simulation}
  }
  \end{figure}

The MC simulated samples are generated with a best estimate of the \pileup distribution of data.
However, the distribution of the ``actual $\mu$'' variable does not agree between data and simulation, thus a dedicated weight is used to correct the discrepancy.
The reweighting uses a default MC \pileup profile, which is used to generate all the MC samples. It applies a weight based on the provided list of data acquisition runs included in the analysis\footnote{The concept of ``good run list'' is further inspected in Section~\ref{sec: dataset}.}, the corresponding file containing the integrated luminosity estimate, and the actual $\mu$ file. 
This operation leads to the matching between the simulated and observed actual $\mu$ distributions.
Figure~\ref{fig:AverageMu} shows the distribution of the actual $\mu$ between data and MC, highlighting, as expected, a reasonable agreement.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.49\textwidth]{figures/chapter_6/mu_actual_emu}
  \includegraphics[width=0.49\textwidth]{figures/chapter_6/mu_actual_ee}
  \includegraphics[width=0.49\textwidth]{figures/chapter_6/mu_actual_mumu}
  \caption{Comparison of data and prediction for the actual $\mu$ distribution between data and prediction in the three channels after the pileup reweighting is applied. The bottom panel shows the ratio of data over the prediction.}  
  \label{fig:AverageMu}
\end{figure}

\FloatBarrier

\section{\texorpdfstring{$t\bar{t}$}{ttbar} sample}
The production of \ttbar events is modelled using the \POWHEGBOX[v2]~\cite{Frixione:2007nw,Nason:2004rx,Frixione:2007vw,Alioli:2010xd}
generator at NLO in QCD with the \NNPDF[3.0nlo]~\cite{Ball:2014uwa} PDF set and the \hdamp parameter\footnote{The \hdamp parameter is a resummation damping factor and one of the parameters that controls the matching of \POWHEG matrix elements to the parton shower and thus effectively regulates the high-\pT radiation against which the \ttbar system recoils.} set to 1.5\,\mtop~\cite{ATL-PHYS-PUB-2016-020}. 
The events are interfaced to \PYTHIA[8.307]~\cite{Sjostrand:2014zea} to model the parton shower, hadronisation, and underlying event, with parameters set according to the A14 tune~\cite{ATL-PHYS-PUB-2014-021} and using the \NNPDF[2.3lo] set of PDFs~\cite{Ball:2012cx}.
The decays of bottom and charm hadrons are performed by \EVTGEN[2.1.1]~\cite{Lange:2001uf}.
The \ttbar sample is normalised to the cross-section prediction at NNLO in QCD including the resummation of NNLL soft-gluon terms calculated using \TOPpp[2.0]~\cite{Beneke:2011mq,Cacciari:2011hy,Baernreuther:2012ws,Czakon:2012zr,Czakon:2012pz,Czakon:2013goa,Czakon:2011xx}.
For proton--proton collisions at a centre-of-mass energy of \(\rts~=~\SI{13.6}{\TeV}\), this cross-section corresponds to \(\sigma(\ttbar)_{\text{NNLO+NNLL}}\) = $924^{+32}_{-40}$~pb using a top-quark mass of \(\mtop = \SI{172.5}{\GeV}\) and the PDF4LHC21 PDF set~\cite{Ball:2022hsh}. 
The uncertainty includes variations in the renormalisation and factorisation scales, $\alpha_S$ and PDFs.
For this calculation, the renormalisation and factorisation scales are set to match the top-quark mass for the nominal result.
The strong coupling constant is set to 0.118 for the nominal prediction.

\section{$Z$-boson sample}
Events from $Z/\gamma^*\to\ellell$ production are simulated with the \SHERPA[2.2.12]~\cite{Bothmann:2019yzt} generator using NLO in QCD matrix elements~(ME) for up to two partons, and leading-order (LO) matrix elements for up to four partons calculated with the Comix~\cite{Gleisberg:2008fv} and \OPENLOOPS~\cite{Buccioni:2019sur,Cascioli:2011va,Denner:2016kdg} libraries.
They are matched with the \SHERPA parton shower~\cite{Schumann:2007mg} using the \MEPSatNLO prescription~\cite{Hoeche:2011fd,Hoeche:2012yf,Catani:2001cc,Hoeche:2009rj} using the tune developed by the \SHERPA authors.
The \NNPDF[3.0nnlo] set of PDFs is used.
Decays of $Z/\gamma^*\to \ee$, $Z/\gamma^*\to \mumu$ and $Z/\gamma^*\to\tautau$ are simulated.
The sample is generated with a requirement of $m_{\ell\ell} > 10~\gev$.
The production is split into samples with $10 < m_{\ell\ell} < 40~\gev$ and samples with $m_{\ell\ell} > 40~\gev$.
The events are normalised to NNLO in QCD and NLO in EW corrections calculated using the \texttt{MATRIX}~\cite{Grazzini:2017mhc} program using the PDF4LHC21 PDF set.
In both the MC generator requirement and in the \texttt{MATRIX} calculation, leptons before QED radiation, called Born leptons, are used.
This calculation uses $m_{\ell\ell} > 40~\gev$ and the same $k$-factor as for the $m_{\ell\ell} > 40~\gev$ is also applied to the samples with $10 < m_{\ell\ell} < 40~\gev$ for the sake of continuity.
For the fiducial phase-space defined with lepton $\pt > 27~\gev$, $|\eta| < 2.5$ and $66 < m_{\ell\ell} < 116~\gev$, the predicted cross-section at NNLO in QCD and NLO in EW is $\sigma(Z)^{\mathrm{fid.}} = 746\pm 0.7\text{(integration)}^{+3}_{-5}(\text{scale})\pm21\text{(PDF)}$~pb for the $Z$-boson decaying into a single lepton flavour for leptons before QED radiation.
More details regarding the theoretical calculation are provided in Appendix~\ref{app:ZbosonTheory}.

\section{Background samples}

\subsection{Single top samples}

The associated production of single top quarks with \(W\) bosons (\(tW\)) is modelled using the \POWHEGBOX[v2] generator at NLO in QCD, employing the five-flavour scheme and the \NNPDF[3.0nlo] set of PDFs.
The diagram-removal scheme~\cite{Frixione:2008yi} is used to eliminate interference and overlap with \ttbar production.
The events are interfaced to \PYTHIA[8.307] using the A14 tune and the \NNPDF[2.3lo] set of PDFs.
The inclusive cross-section is corrected to the theory prediction calculated at approximate N${}^{3}$LO in QCD using the PDF4LHC21 PDF set~\cite{Kidonakis:2021vob}.
The cross-section corresponds to \(\sigma(tW)_{\text{aN}^3\text{LO}} = 87.9^{+2.0}_{-1.9}\text{(scale)}\pm2.4\text{(PDF)}~\si{\pb}\).

Single-top-quark \(t\)-channel production is modelled with the \POWHEGBOX[v2]~\cite{Frixione:2007nw,Alioli:2009je,Nason:2004rx,Frixione:2007vw,Alioli:2010xd} 
generator at NLO in QCD using the four-flavour scheme and the corresponding \NNPDF[3.0nlo] set of PDFs. 
The events are interfaced with \PYTHIA[8.307] using the A14 tune and the
\NNPDF[2.3lo] set of PDFs.
The inclusive cross-section is corrected to the theory prediction 
$\sigma_{t\text{-channel}}=232.2^{+4,3}_{-2.9}$\,pb. The cross-section is
calculated with the MCFM program~\cite{Campbell:2020fhf} at
NNLO QCD. The quoted uncertainties include the
uncertainties due to the choice of the renormalisation and factorisation
scales $\mu_{r}$, $\mu_{f}$, the uncertainty in the parton distributions
functions (PDFs) and in the value of the strong coupling constant
$\alpha_s$.

Single-top-quark \(s\)-channel production is modelled using the \POWHEGBOX[v2]
generator at NLO in QCD in the five-flavour scheme with the \NNPDF[3.0nlo] PDF set.
The events are interfaced with \PYTHIA[8.307] using the A14
tune and the \NNPDF[2.3lo] PDF set.
The inclusive cross-section is corrected to the theory prediction computed at NNLO in QCD, $\sigma_{s\text{-channel}}=7.246^{+0.059}_{-0.043}$\,pb.
In this analysis, the contributions from $t$-channel and $s$-channel productions only arise through fake and non-prompt leptons.

\subsection{Diboson samples}
Samples of diboson final states (\(VV\)) are simulated using the \SHERPA[2.2.12] generator, which includes off-shell effects and Higgs boson contributions where appropriate.
Both fully-leptonic and semileptonic final states, where one boson decays leptonically and the other hadronically, are generated using
matrix elements at NLO accuracy in QCD for up to one additional parton and at LO accuracy for up to three additional parton emissions.
Since no dedicated NNLO calculation for the cross-section is available at the time of writing, the same $k$-factors used in Run~2 are employed also for the \sqs = 13.6 \tev normalisation.


\subsection{\ttbar+$V$ samples}
Samples of associated production of the top-quark pairs with a $Z$-boson, a $W$-boson, or a Higgs boson are simulated using the \SHERPA[2.2.12] generator.
Events with at least two leptons in the final state are considered.
The small contributions are merged into a single process labelled $t\bar{t}+V$.
No dedicated $k$-factors are available at the time of writing, so a 50\% uncertainty is applied to the cross-section.

\subsection{Mis-identified and non-prompt lepton background}
As described in Chapter~\ref{sec:chapter_4}, charged pions can be mis-reconstructed as electrons in the detector; similarly, non-prompt electrons and muons can pass the isolation selection and be misidentified as prompt leptons.
Both mis-reconstructed electrons and non-prompt leptons are referred to as "fake leptons".

The majority of events with a fake lepton in the dilepton channel originate from events featuring a real, prompt single lepton.
This allows to use the MC predictions to estimate this background.

For each event, the MC truth information is checked to determine if the event that passes the selection contains a fake lepton and based on this information the events are split into real and fake contributions.
This is done when processing MC samples with two or more real leptons in the final state and also for processes with only one prompt lepton in the final state such as $W$+jets, simulated with \SHERPA[2.2.12], or single lepton \ttbar process, simulated with \POWHEGBOX[v2] interfaced with \PYTHIA[8.307].
The distributions classified as fake contributions represent the fake lepton background.
% The $Z$ samples are assumed to contain no fake leptons, as there is a fraction of events with an unknown lepton origin that could have been classified as fake leptons.
To cover the possible mis-modelling of the MC prediction, a conservative 50-100\% uncertainty is added to the prediction of this background.
The conservative uncertainty does not significantly impact the measured cross-section uncertainty, owing to the low contribution of the fake lepton background in the dilepton channel\footnote{The breakdown of the contributions can be seen in Table~\ref{tab:EventSelection/Channels}}.


\section{Comparison with previous measurements}
\label{sec:DataMC_definitions_Z}
\subsection*{Signal and background modelling in ATLAS-CONF-2022-070}
\paragraph{$Z$-boson sample:}
During the early stage of Run~3, a complete estimate of the $Z$-boson fiducial cross-section was not available. 
Therefore, the predicted cross-section is computed with the requirement $m_{\ell\ell}>40\gev$:
$$ \sigma(Z)^{m_{\ell\ell}>40} = 2182^{+20}_{-25}(\text{scale})\pm37\text{(PDF)}\: \text{pb}$$

\paragraph{Single-top samples:}
The cross-section value for the $tW$ process belongs to a previous N${}^{3}$LO estimate, corresponding to \(\sigma(tW)_{\text{NLO+NNLL}} = 87.6^{+2.0}_{-1.9}\text{(scales)}^{+2.1}_{-1.5}\text{(PDF)}~\si{\pb}\).

\paragraph{\ttbar+$V$ samples:}
No \ttbar+$V$ samples are used in this measurement.

\subsection*{Signal and background modelling in ATLAS-CONF-2023-006}
\paragraph{$Z$-boson sample:}
In this measurement, the production cross-section is extrapolated from the complete fiducial phase space, resulting in: 
$$\sigma(Z)^{\text{fid.}}~=~741\pm~4\text{(integration)}^{+3}_{-4}(\text{scale})\pm14\text{(PDF)}\: \text{pb}$$

The central value used in the main measurement is shifted by about 0.6\% from the one quoted above, following the update of one of the PDFs used in the calculation.

\paragraph{Single-top samples:}
The same cross-section value used in ATLAS-CONF-2022-070 is employed.

\paragraph{\ttbar+$V$ samples:}
No \ttbar+$V$ samples are used in this measurement.

\FloatBarrier

