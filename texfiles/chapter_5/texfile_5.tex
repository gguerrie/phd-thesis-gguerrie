\chapter{Analysis strategy}
\label{sec:chapter_5}
The aim of the analysis presented in this thesis is two-fold.
The first goal is to provide an extensive validation of the new detector components, reconstruction methodologies, and software used in the third run of the LHC.
This validation is carried out by providing data-to-MC comparisons for various kinematic distributions in a relatively pure sample of \ttbar events, in the dilepton final state.

The second and the main goal of the analysis is to measure the inclusive \ttbar cross-section, \xtt, in the above-mentioned final state.
A cross-section ratio of \ttbar to $Z$-boson production, denoted as \ratio, is provided with the goal of reducing experimental uncertainties, primarily those related to luminosity. 
Moreover, given that the \ttbar\ and $Z$-boson production dynamics are driven to a large extent by different PDFs, 
the ratio of these cross-sections at a given centre-of-mass energy has a significant sensitivity to the gluon-to-quark 
PDF ratio~\cite{Rojo:2015acz,Mangano:2012mh}.
Finally, the measurement of the $Z$-boson cross-section, \xz, defined in a fiducial phase space characterised by kinematic selection requirements that follow closely those applied to data events, is provided.
% characterised by lepton $\pt > 27~\gev$, lepton $|\eta| < 2.5$ and the invariant mass $66 < m_{\ell\ell} < 116~\gev$ for $Z\to\ell\ell$ with $\ell = e,\mu$, is provided.

Three channels are defined using electrons and muons of opposite electric charge: $ee$, $\mu\mu$ and $e\mu$. For the
same-flavour channels ($ee$, $\mu\mu$), the reconstructed invariant mass of the lepton pair, $m_{\ell\ell}$, is required to
be close to the $Z$-boson mass: these channels constitute a very pure sample, with $\sim$99.5\% of events originating from $Z$-boson production.
In the opposite-flavour channel ($e\mu$), the well-established technique of "$b$-tag counting" is exploited (as presented in Ref.~\cite{TOPQ-2018-17}), consisting in a simultaneous extraction of the \ttbar cross-section and the $b$-jet identification efficiency.
In this analysis, this $b$-tag counting method is modified to be suitable for a profile-likelihood approach that facilitates combinations with the single-lepton channel and the $Z$-boson measurements. \\
Section~\ref{sec: results-chronology} reports a concise summary of the various results published during Run~3. 
A detailed description of the procedure used to extract the cross-sections is provided in Section~\ref{sec:stat_analysis}.

% The \ttbar\ selection requires one electron and one muon of the opposite electric charge, and at least one $b$-tagged jet.
% The estimation of the backgrounds relies on MC simulation.
% A full description of the selection requirements is presented in Chapter~\ref{sec:chapter_5}.

\section{Results chronology}\label{sec: results-chronology}
The results presented in this thesis are divided into three parts: a primary measurement, and two preliminary analyses, each corresponding to distinct periods of the early Run~3 at the LHC.
A brief overview of the measurements, alongside with a preliminary study conducted at the very beginning of the data taking, is presented below.
Furthermore, Table~\ref{tab: strategy-comparison} provides a summarised comparison of the strategies; a more in-depth discussion of the varying procedures implemented across the three publications will be provided as necessary in the ensuing chapters.

\subsection{Preliminary studies -  790~\ipb of 2022 data}
The first studies on the detector and software performance were presented at the ``15th International Workshop on Top Quark Physics'' (TOP2022) conference, in September 2022~\cite{guerrieri2022run}.
Data were collected from August 6th to August 8th, 2022; the dataset corresponds to three LHC fills, specifically 8102, 8103, and 8106.
The comparison of data and prediction in the electron-muon final state provides a valuable input to validate the functionality of the
detector and the reconstruction software which went through a number of upgrades during LS2.

\subsection{ATLAS-CONF-2022-070 - 1.2~\ifb of 2022 data}
The first measurement of the inclusive \ttbar production cross-section, a fiducial $Z$-boson production cross-section and their ratio 
is performed using 1.2~$\ifb$ of data collected by the ATLAS detector at the centre-of-mass energy of $13.6$~\tev~\cite{ATLAS-CONF-2022-070}.
The data correspond to four different LHC fills, recorded between August 6th and August 9th, 2022. The LHC fill 8112 was added to the previous set.
This first set of results was presented as a poster at the ``152nd LHCC Meeting'' (LHCC2022), in November 2022.

\subsection{ATLAS-CONF-2023-006 - 11.3~\ifb of 2022 data}
This result~\cite{ATLAS-CONF-2023-006} represents a midterm milestone for the analysis effort, 
and a refined estimation of the \ttbar production cross-section, of the fiducial $Z$-boson production cross-section and of the ratio between the two using 11.3~\ifb.
A number of features are improved in the analysis, including a significant reduction of the luminosity uncertainty, and several updates on physics objects reconstruction and calibrations.
These results were presented at the ``57th Rencontres de Moriond 2023" (Moriond2023) conference, in March 2023.

\subsection{Primary measurement - 29~\ifb of 2022 data}
This measurement is the main result of this thesis. As in the previous analyses, it includes the estimate of the \ttbar production cross-section and of the \ttbar/$Z$ cross-sections ratio; 
additionally, the measurement of the fiducial $Z$-boson cross-section is provided by using a different fir strategy, as further described in Section~\ref{sec:fit-setups}.~\cite{atlascollaboration2023measurement}.
The analysis exploits the full 2022 dataset, enabling a significant validation of Run~3 data; 
moreover, this study presents an opportunity to refine the recommendations and features associated with the physics objects.
These results were presented at the ``European Physical Society Conference on High Energy Physics 2023" (EPS-HEP2023) conference, in August 2023.

\newgeometry{a4paper, total={170mm,257mm}, left=20mm, top=20mm}
\begin{landscape}
%   \resizebox{\textwidth}{\textheight}{%
    \begin{table}[t]
        \centering
        \begin{footnotesize}
        \begin{tabular}{p{0.2\textwidth}C{0.38\textwidth}C{0.38\textwidth}C{0.36\textwidth}}
            \toprule
            \textbf{Feature} & \textbf{ATLAS-CONF-2022-070} & \textbf{ATLAS-CONF-2023-006} & \textbf{Primary measurement} \\
            \midrule
            \multicolumn{4}{c}{\textbf{Analysis strategy}}\\
            Fit setup & Two separate fits; the first to measure \xtt\ ($e\mu$ channel), the second to extract \xzold, \ratio\ and $\epsilon_b$ ($e\mu$, $ee$ and $\mu\mu$ channels) & Two separate fits in the $e\mu$, $ee$ and $\mu\mu$ channels; the first to compute \xtt\ and \xz, the second to extract \ratio\ and $\epsilon_b$ & Added third fit to measure \xz\ from $ee$ and $\mu\mu$ channels only. \\
            \midrule
            \multicolumn{4}{c}{\textbf{Fiducial treatment of the $Z$-boson}}\\
            Phase space definition & $66 < m_{\ell\ell} < 116$~GeV, extrapolated to $m_{\ell\ell} > 40$~GeV with $Z\to\ell\ell$ & lepton $\pt > 27$~GeV, lepton $|\eta| < 2.5$, $66 < m_{\ell\ell} < 116$~GeV for $Z\to\ell\ell$  & Unchanged \\
            Acceptance corrections & Not applied & Applied  & Updated \\
            Cross-section prediction & $\sigma(Z)^{m_{\ell\ell}>40} = 2182^{+20}_{-25}(\text{scale})\pm37\text{(PDF)}$~pb & $\sigma(Z)^{\text{fid.}}~=~741\pm~4\text{(int.)}^{+3}_{-4}(\text{scale})\pm14\text{(PDF)}$~pb  & $\sigma(Z)^{\text{fid.}}~=~746\pm~0.7\text{(int.)}^{+3}_{-5}(\text{scale})\pm21\text{(PDF)}$~pb \\
            \midrule
            \multicolumn{4}{c}{\textbf{Objects definition}}\\
            Electrons & $TightLH$ ID, $Tight\_VarRad$~Isol. & Updated trigger scale factors  & Unchanged \\     
            Muons & $Medium$~ID, $Tight\_VarRad$~Isol. & Updated trigger scale factors  & Unchanged \\
            Jets & k-nearest neighbour based JVT & Neural network based JVT  & Unchanged \\
            Flavour tagging & DL1d tagger & Updated $b$-tagging calibrations  & Unchanged \\
            \midrule
        %    \multicolumn{4}{c}{\textbf{Events selection}}\\
            % Selection & 31 & SN & \\
            % \midrule
            \multicolumn{4}{c}{\textbf{Theoretical prediction for \ratio}}\\
            PDF sets used & PDF4LHC21 & PDF4LHC21(including \mtop variations), CT18, CT18A, MSHT20, NNPDF4.0, ATLASpdf21, ABMP16 & Updated predictions \\
            \midrule
            \multicolumn{4}{c}{\textbf{Data}}\\
            Integrated luminosity & 1.2~\ifb of 2022 data & 11.3~\ifb of 2022 data & 29~\ifb of 2022 data \\
            \multicolumn{4}{c}{\textbf{}}\\
            \multicolumn{4}{c}{\textbf{MC samples}}\\
                & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} \textbf{Generator} & \textbf{Theory prediction} \end{tabular} & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} \textbf{Generator} & \textbf{Theory prediction} \end{tabular} & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} \textbf{Generator} & \textbf{Theory prediction} \end{tabular}\\
            % \ttbar & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} PP8 & NNLO+NNLL \end{tabular} & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} Unchanged \end{tabular} & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} Unchanged \end{tabular} \\
        
            % $V$+jets & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} Sherpa & NNLO(QCD)+NLO(EW) \end{tabular} & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} Unchanged \end{tabular} & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} Unchanged \end{tabular} \\
        
            % Diboson & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} Sherpa & from MC \end{tabular} & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} Sherpa & NNLO (QCD) \end{tabular} & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} Unchanged \end{tabular} \\
        
            Single-top & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} PP8 & NLO+NNLL \end{tabular} & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} Unchanged \end{tabular} & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} Unchanged & Updated \end{tabular} \\
        
            $\ttbar+V$ & Not included & Not included & \begin{tabular}{C{0.1\textwidth}C{0.2\textwidth}} Sherpa & From MC \end{tabular} \\
            \midrule
            \multicolumn{4}{c}{\textbf{Systematic uncertainties}}\\
            Electrons & ID, Isol., Reco., momentum/scale resolution & Updated systematic model & Updated scale uncertainties \\
            Muons & ID, Isol., TTVA, momentum/scale resolution & Updated systematic model & Updated momentum/scale and trigger uncertainties \\
            Jets & JES, JER, JVT uncertainties & Updated systematic model & Updated jet reconstruction \\
            Flavour tagging & SFs=1, uncertainties for $b$-tagging, $c$- and light-mistagging set at 10\%, 20\% and 40\% & SFs estimated on Run~3 data, uncertainties unchanged & Unchanged \\
            Top \pt\ reweighting & Not estimated & Estimated & Estimated \\
            $Z$-boson modelling & Scale uncertainties & Corrected with acceptance effects & Updated \\
            Luminosity & 10\% & 2.2\% & Unchanged \\
            Pileup & Variation of average $\mu$ by 3\% in simulation (Run~2) & Variation of average $\mu$ by 4\% in simulation & Unchanged \\   
            $\ttbar+V$ normalisation & Not included & Not included & Set at 50\% \\
            \multicolumn{4}{c}{\textbf{Additional uncertainties}}\\
            Top mass dependence & Not estimated & Not estimated & Estimated \\
        \midrule
            
            \bottomrule
        \end{tabular}
        \caption{Comparison of the main features characterising the measurements presented in this thesis. Only elements that were updated at least once over time are shown.}
        \label{tab: strategy-comparison}
        \end{footnotesize}
    \end{table}
%   }
\end{landscape}
\restoregeometry 

\section{Statistical treatment}\label{sec:stat_analysis}

\subsection{Profile-likelihood}
\label{sec:profile-likelihood}
% The profile-likelihood approach builds a likelihood that includes the statistical model as well as all the sources of the systematic uncertainties directly in the likelihood.
The profile-likelihood approach offers an efficient way to integrate the statistical model as well as all the sources of the systematic uncertainties in the likelihood function itself.
% Maximising
% ~\footnote{This operation can be replaced by the minimisation of the negative logarithm of the likelihood.} 
% the likelihood implies computing the most probable values (and their uncertainties) for the parameters of interest (POIs) as well as the nuisance parameters (NPs).
Following the maximum-likelihood principle, the best estimates for the parameters of interest (POIs) in a certain model can be obtained by maximising such likelihood function 
with respect to these POIs as well as a set of nuisance parameters (NPs), associated with systematic uncertainties.
The likelihood, $L$, can be schematically written as follows:

\begin{eqnarray}
    L\left(\vec{n}|\mu, \vec{\theta}\right) = \prod_{i\in \text{bin}} \text{Pois}\left(n_{i}|\mu S_{i}(\vec{\theta}) + B_{i}(\vec{\theta})\right)\times\prod_{j\in \text{NP}}G\left(\theta_j\right),
\label{eq:Likelihood}
\end{eqnarray}

\noindent where $\vec{n}$ is the data vector, and $n_{i}$ represents the data yields in each bin.
The notation $\vec{\theta}$ denotes the NPs, that affect the number of signal events $S_{i,r}$ as well as the number of background events $B_{i}$.
Finally, $\mu$ is the signal strength, which is the parameter of interest in the likelihood and it represents the ratio of the measured signal cross-section over the predicted one.
The terms ``Pois'' and $G$, represent the Poisson and Gaussian distributions, respectively.
% It is clear from Eq.~\ref{eq:Likelihood} that the NPs are constrained by the Gaussian term\footnote{For the \emph{gamma} terms, representing the MC statistical uncertainty, the constraint term is Poissonian --- in Bayesian statistics this would result in Gamma function posterior distribution, hence the name.} while the normalisation parameters, $\vec{k}$ are unconstrained.
The Gaussian terms act as constraints on the NPs associated with systematic uncertainties, parametrising the prior knowledge on the parameters controlling systematic effects\footnote{For the \emph{gamma} terms, representing the MC statistical uncertainty, the constraint term is Poissonian --- in Bayesian statistics this would result in Gamma function posterior distribution, hence the name.}.
In addition to the parameter of interest and the nuisance parameters, unconstrained nuisance parameters, $\vec{k}$, can be added to the likelihood, typically as multiplicative factors, or ``normalisation factors'' (NFs) to certain signal or background model components, to be extracted from the data at the same time of the POIs.
The full form of the likelihood can be found in the \emph{HistFactory} reference~\cite{Cranmer:1456844}.

Internally, the systematically-varied histograms provided are compared to the nominal histograms considering the total yield difference; this approach takes the name of normalisation
\footnote{More generally a second component is taken into account, and includes a pure shape effect, where the total yield of the variation is forced to match the nominal prediction. Then, for each bin, the shape variation is interpolated and extrapolated using a linear interpolation. 
Since in this thesis the fit is performed on single bins, no shape component is considered.}.
For each bin, the product of an exponential and polynomial of 6-th order is used; the polynomial is needed to have a smooth transition for the function and its first and second derivative between the up and down variations. 
The normalisation effect is controlled by the parameter $\theta$.
This results in a Gaussian constraint for the shape component and an approximate log-normal constraint for the normalisation component, which prevents an unphysical negative normalisation.

The likelihood from Eq.~\ref{eq:Likelihood} is maximised to obtain the best-fit values for all the parameters as well as their uncertainty.
The maximisation of the likelihood, or rather, the minimisation of the negative logarithm of the likelihood, exploits the MINUIT framework~\cite{James:310399}, adopting the MIGRAD minimisation method.
Such minimisation can be summarised as follows:
\begin{enumerate}
    \item Fix the parameters to a certain value, $x$.
    \item Compute the gradient $\nabla$ in a given point, assuming that the Hessian matrix corresponds to unity.
    \item Along the direction of the gradient, find the value $\alpha$ such that $-\log L(x-\alpha V \times \nabla)$ is minimum. 
    In the previous expression, $V$ represents the covariance matrix of the parameters, or the inverse of the Hessian matrix.
    \item Repeat the operation until the estimated distance to the minimum (EDM) is sufficiently small. The EDM is typically defined as $EDM = \nabla^{\text{T}}V\nabla$.
\end{enumerate}
This procedure provides also the correlation matrix between the NPs, since it can be trivially extracted from the covariance matrix $V$. 
The post-fit uncertainties for both POIs and NPs can be derived from the diagonal elements of the covariance matrix, which is the inverse of the symmetric Hessian matrix. 
For this reason, the estimated uncertainties are symmetric.
To accommodate a more precise determination of the uncertainties, allowing them to be also asymmetric, the MINOS algorithm~\cite{James:310399} is employed.

\subsection{$b$-tag counting method and profile-likelihood}
\label{sec:btagcountPL}

The $b$-tag counting method has proven to be an effective method of measuring the cross-section of the \ttbar process in the dilepton decay channel due to its low sensitivity to systematic uncertainties~\cite{TOPQ-2018-17}.
The method requires to select events with opposite-sign electron and muon pairs and counting the number of those that have exactly one $b$-tagged jet ($N_1$) and exactly two $b$-tagged jets ($N_2$).

The two event counts satisfy the tagging equations:

\begin{eqnarray}
    \label{eq:btagcount}
    N_1 &=& L\sigma_{\ttbar}\epsilon_{e\mu}2\epsilon_b\left(1-C_b\epsilon_b\right) + N_1^{\text{bkg}},\\
    N_2 &=& L\sigma_{\ttbar}\epsilon_{e\mu}C_b\epsilon_b^2 + N_2^{\text{bkg}},
\end{eqnarray}

\noindent where $L$ is the integrated luminosity, $\sigma_{\ttbar}$ is the sought after \ttbar production cross-section, and the $b$-jet efficiency $\epsilon_b$ 
is the efficiency to reconstruct and $b$-tag a jet originating from the fragmentation of a $b$ quark coming from a top-quark decay in \ttbar events. 
$\epsilon_{e\mu}$ is the efficiency for a \ttbar event to pass the opposite sign $e\mu$ selection, 
$C_b$ is a tagging correlation coefficient that is close to unity, and $N_{1(2)}^{\text{bkg}}$ is the number of background events with one (two) $b$-tagged jets.

The correlation factor, $C_b$, is defined as $C_b = \epsilon_{bb}/\epsilon_b^2$, where $\epsilon_{bb}$ represent the probability to reconstruct and tag both $b$-jets.
The deviation of $C_b$ from unity is caused by kinematic correlations of the two $b$-jets produced in a \ttbar event and the kinematic dependence of the $b$-tagging probability.
The correlation factor can be rewritten as:

\begin{eqnarray}\label{eq: C_b}
    C_b = \frac{4N^{\ttbar}_{\geq 0}N_2^{\ttbar}}{\left(N_1^{\ttbar} + 2N_{2}^{\ttbar}\right)^2},
\end{eqnarray}

\noindent where $N_{\geq 0}^{\ttbar}$ is the number of selected $e\mu$ events without any requirements on jet or $b$-tagging.
The $C_b$ parameter is estimated using the MC simulation; in this thesis, using 29 \ifb of data, $C_b=1.0068$.

Although the $b$-tagging technique is powerful, it does not allow for direct implementation in the standard profile-likelihood tools.
This is evident from the Eq.~\ref{eq:btagcount} which does not have the usual form of signal strength (normalisation) times the predicted number of events due to the presence of the free parameter $\epsilon_b$.
The equations have to be adapted to make them suitable for a profile-likelihood fit.
The first step is to rewrite the signal prediction part using $L\sigma_{\ttbar}\epsilon_{e\mu} = \mu N_{\geq 0}^{\ttbar}$, where $\mu$ is the signal strength and $N_{\geq 0}^{\ttbar}$ is the number of \ttbar events passing the lepton selection without any requirement on jets.

The Eq.~\ref{eq:btagcount} can then be reinterpreted as:
\begin{eqnarray*}
    N_1 &=& \mu 2\epsilon_b N_{\geq 0}^{\ttbar} - \mu 2\epsilon_b^2C_b N_{\geq 0}^{\ttbar} + N_1^{\text{bkg}} \\
    N_2 &=& \mu C_b \epsilon_b^2 N_{\geq 0}^{\ttbar} + N_2^{\text{bkg}}.
\end{eqnarray*}

These equations now have the standard form of product between free parameter and prediction, which makes them suitable for a profile likelihood fit.

Moreover, it is possible to rewrite the equations in order to make it more suitable for an implementation in the HistFactory formalism, where data counts in a certain set of bins are 
compared with expectations obtained as a sum of predictions for a number of different signal or background processes, obtained from MC simulation, 
possibly multiplied by normalisation factors (NFs) and modified by systematic effects.
% The numbers of events and tagging efficiency, as well as the correlation factor are shown in Table~\ref{tab:nominal_values_for_N}.
% \begin{table}[!hbpt]
%     \centering
%       \begin{tabular}{c|r}
%       \toprule       
%       $N_1^{\ttbar}$           & 87352\\
%       $N_2^{\ttbar}$           & 52717\\
%       $N^{\ttbar}_{\geq 0}$    & 177000\\
%       $N_1^{\text{bkg}}$       & 11848\\
%       $N_2^{\text{bkg}}$       & 2283\\
%       \midrule
%       $\epsilon_b$             & 0.543\\
%       $C_b$                    & 1.0068\\
%       \bottomrule
%       \end{tabular}
%       \caption{Numbers of events of the Monte Carlo samples passing the different \ttbar selections and background yields rounded to their integer value. The tagging efficiency and correlation factor are calculated from the generated Monte Carlo samples.}
%       \label{tab:nominal_values_for_N}
%   \end{table}


Exploiting Eq.~\ref{eq: C_b} to expand the $C_b$ term yields:

\begin{eqnarray*}
    N_1 &=& M_a + M_b + N_1^{\text{bkg}} \\
    N_2 &=& M_c + N_2^{\text{bkg}},
\end{eqnarray*}

\noindent where

\begin{eqnarray*}
    M_a &=& 2 \mu \epsilon_b N_{\geq 0}^{\ttbar},\\
    M_b &=& -8\mu \epsilon_b^2 N_{\geq 0}^{\ttbar} A, \\
    M_c &=& 4\mu \epsilon_b^2 N_{\geq 0}^{\ttbar} A, \\
    A &=& N_{\geq 0}^{\ttbar} N_2^{\ttbar}/\left(N_1^{\ttbar} + 2 N_2^{\ttbar}\right)^2.
\end{eqnarray*}
    
\noindent Further simplifying the equations leads to:

\begin{equation}
    \begin{aligned}
        \label{eq:tagcountsimple}
        N_1 &= \mu \epsilon_b H_a -\mu \epsilon_b^2 H_b + N_1^{\text{bkg}} \\
        N_2 &= \mu \epsilon_b^2 H_c + N_2^{\text{bkg}},
    \end{aligned}
\end{equation}

\noindent where $H_a$, $H_b$, $H_c$ are one-bin histograms prepared before they are passed to the fit. Specifically:

\begin{equation}
    \begin{aligned}
        \label{eq:fit_histos}
        H_a &= 2 N_{\geq 0}^{\ttbar}, \\
        H_b &= 8\left(N_{\geq 0}^{\ttbar}\right)^2 N_2^{\ttbar}/\left(N_1^{\ttbar} + 2N_2^{\ttbar}\right)^2, \\
        H_c &= 4\left(N_{\geq 0}^{\ttbar}\right)^2 N_2^{\ttbar}/\left(N_1^{\ttbar} + 2N_2^{\ttbar}\right)^2.
    \end{aligned}
\end{equation}

\noindent Using the histograms presented in Eq.~\ref{eq:tagcountsimple} provides a convenient way to interface the counting method to the standard profile-likelihood tools.% as the free parameters ($\mu$ and $\epsilon_b$) are attached to the distributions.
Additionally, systematic uncertainties affect the MC predictions for $N_{\geq 0}^{\ttbar}$, $N_1^{\ttbar}$ and $N_2^{\ttbar}$, therefore are simply propagated to the one-bin histograms $H_{a,b,c}$.
To properly deal with the statistical uncertainty on these predictions, due to the limited number of simulated events used to obtain them (referred to as MC statistical uncertainty in the following), 
three systematic variations for each of the predictions $H_{a,b,c}$ are defined, by shifting $N_{\geq 0}^{\ttbar}, N_1^{\ttbar}$ and $N_1^{\ttbar}$ up and down by one standard deviation, obtained as the square root of the sum over the selected event squared weights.
% The shifted histograms are then passed to the fit as uncertainties that are decorrelated bin-by-bin but correlated between $H_a, H_b$ and $H_c$.
Meanwhile, the standard approach is used for the background MC statistical uncertainties.

\subsection{Practical implementation of the individual cross-sections and ratio fits}
\label{sec:fit-setups}
A total of three fits are used for the extraction of the results. 
The fit setups are organised as follows:
\begin{enumerate}
    \item \textbf{Individual \ttbar and $Z$-boson cross-sections}: in the $e\mu$ channel, the procedure outlined in Section~\ref{sec:btagcountPL} is employed for the $e\mu$ events.
    Events from the $ee$ and $\mu\mu$ channels are also used in this fit. The extension of the \ttbar cross-section measurement to the $ee$ and $\mu\mu$ channels is performed in order to account 
    for the correlations between the \ttbar and the $Z$-boson POIs.
    % by uniforming the regions considered, there is a guarantee that the fitted ratio of cross-sections corresponds exactly to the ratio of the fitted cross-sections. 
    Thus, the output of the first fit is:
    \begin{itemize}
        \item the signal strength for the \ttbar production; 
        \item the signal strength for the $Z$-boson production;
        \item the $b$-jet efficiency estimate.
    \end{itemize}    
    \item \textbf{Ratio of \ttbar and $Z$-boson cross-sections}: as before, the fit is performed in all the three channels ($e\mu$, $ee$ and $\mu\mu$). 
    The likelihood is modified to add a common NF for both \ttbar and $Z$ processes (the same factor is applied to both).
    This allows to scale the contribution from \ttbar and $Z$ events using the same parameter.
    Furthermore, another NF is attached only to the \ttbar prediction.
    This results in \ttbar events being scaled by two NFs, where one of is shared with the $Z$ contribution.
    This means that the NF that is attached only to \ttbar events acts as a relative NF on top of the $Z$ prediction, 
    which is the sought after ratio of the $\ttbar$ over $Z$ cross-section.
    The implementation allows to extract the ratio and its uncertainty directly from the fit, without the need of and error-propagation operation, obtaining as a result:
    \begin{itemize} 
        \item the signal strength for the ratio between cross-sections;
        \item the signal strength for the $Z$-boson production;
        \item the $b$-jet efficiency estimate.
    \end{itemize}    
    Both this fit procedure and the one described in the previous item are equivalent for the estimation of the signal strength for the $Z$-boson production and of the $b$-jet efficiency.
    \item \textbf{Pure $Z$-boson cross-section}: a third fit is implemented to extract the $Z$-boson cross-section from the $ee$ and $\mu\mu$ channels only. 
    This precautionary measure is taken to decorrelate back the \ttbar and $Z$-boson POIs, thus maintaining consistency between the $Z$-boson cross-section presented in this thesis, and other measurements related to different processes\footnote{For instance, a parallel measurement of the $Z$-boson production can be obtained while studying the ratio between the $W$-boson and the $Z$-boson production cross sections.}.
    This fit produces the estimate of a single POI: the signal strength for the $Z$-boson production.
\end{enumerate}
The extrapolation of \xtt\ is derived from the first fit, \ratio\ is obtained from the second fit, and \xz\ is extracted from the third fit. 
The $b$-jet efficiency can be taken either from the first or the second setup.


\subsection{Validation of the fit procedure}
\label{sec:fit-validation}
Validating the outcome of a profile likelihood fit is paramount to ensure the robustness and accuracy of the derived results. 
A set of diagnostic tools, including pull plots, constraints, correlation matrices, and ranking plots, are employed in this thesis and are presented in the following.

\subsubsection{Nuisance parameters pulls and constraints}
\label{sec:pulls-constraints}

In profiling the likelihood to obtain estimates for the POIs and the NPs, the concepts of pulls and constraints become significantly pertinent. 
The ``pull'' of a nuisance parameter \( \theta \) is defined as the normalised difference between the estimated value \( \hat{\theta} \) from the fit and its prior expected value \( \theta_0 \)~\cite{Gross:2018okg}:

\begin{equation}
\text{pull}(\theta) = \frac{\hat{\theta}-\theta_0}{\sigma_{\theta}},
\label{eq:Pull}
\end{equation}

\noindent where \( \sigma_{\theta} \) is the standard deviation of the pre-fit probability distribution of \( \theta \). 
Pulls are instrumental in quantifying how the data influences the NPs away from their prior expectations. 
The expected interval of the pull is $[-1, +1]$; a pull of zero denotes a good agreement between the data and the prior, while non-zero pulls warrant a deeper investigation into the systematic uncertainties or the model itself.

On the other hand, the so-called post-fit constraints quantify how much the fit is able to tell about the NPs, possibly improving upon their prior knowledge embedded in the prediction model. 
Indeed, the constraint terms in the likelihood function are meant to incorporate previous or ``auxiliary'' measurements of the NPs, so that they can be used to define what we call nuisance-parameter `pre-fit' 
uncertainties. 
When the model is compared to the data, i.e. when the fit is performed, the NP best-fit values come with their new uncertainties, in general always equal or smaller than the pre-fit ones. 
The smaller a post-fit uncertainty on a NP, the stronger the constraining power of the data, so that the ratio of the post-fit over the pre-fit uncertainty, often referred as `post-fit NP constraint', 
quantifies how much the data could improve the knowledge on this particular NP, or, in other words, reduce the impact of the associated source of systematic uncertainty.

\subsubsection{Post-fit parameter correlation matrix}
\label{sec:corr-matrix}

As described in Section~\ref{sec:profile-likelihood}, the post-fit parameter correlation matrix elucidates the linear relationships between the parameters of interest and the NPs. 
The elements of the correlation matrix, \( \rho_{ij} \) range from -1 to 1, with -1 indicating a perfect negative linear relationship, 1 indicating a perfect positive linear relationship, 
and 0 indicating no linear correlation.

The interpretation of the correlation matrix offers a reliable way in understanding how uncertainties in the NPs propagate to the uncertainties in the POIs, as well as how pulls and post-fit constraints of different nuisance parameters are correlated. 
High absolute values of correlation indicate that the uncertainties in the parameters are significantly intertwined, which might necessitate a more cautious interpretation of the fit 
results or further investigation into the underlying model and systematic uncertainties.

\subsubsection{Nuisance parameter ranking}
\label{sec:ranking-plots}
The so-called ``nuisance parameter ranking'' (often presented in the form of a plot, hence the common name ``ranking plot'') serves as a vital tool for discerning the impact of NPs on the parameters of interest. 
These plots rank the NPs based on a specified metric, often the impact of each NP on the POIs when varied within their uncertainties. 

The impact of each NP, $\Delta \mu$, is computed by comparing the nominal best-fit value of $\mu/\mu_{\text{pred.}}$ with the result of the fit when fixing the considered nuisance 
parameter to its best-fit value, ($\theta$), shifted by its pre-fit and post-fit uncertainties $\pm \Delta \theta(\pm \Delta \hat{\theta})$.
The ranking plots thus provide a clear, visual representation of which NPs have the most substantial influence on the POIs.

Moreover, ranking plots can expose unexpected behaviours in the fit, such as NPs with unexpectedly large impacts or those whose impacts have changed significantly between different iterations of the analysis. 
Such insights can be instrumental in diagnosing issues with the fit or the model, and in understanding the interplay between different sources of systematic uncertainty. 


\subsection{Comparison with previous measurements}
\subsubsection{Cross-sections estimation treatment in ATLAS-CONF-2022-070}
For this measurement, the definition of the $Z$-boson production cross-section \xzold\ differs with respect to \xz\ presented at the beginning of the chapter.
\xzold\ is measured using events with reconstructed $m_{\ell\ell}$ values that satisfy $66 < m_{\ell\ell} < 116~\gev$, and is extrapolated to $m_{\ell\ell} > 40~\gev$ with $Z\to\ell^+\ell^-$, where $\ell = e$ or $\mu$.

Moreover, only two fits are used; in the first, \xzold, \ratio\ and $\epsilon_b$ are implemented as unconstrained parameters and extracted using the $e\mu$, $ee$ and $\mu\mu$ channels.
The value for \xtt\ is derived from a second fit procedure, using only the $e\mu$ channel.

\subsubsection{Cross-sections estimation treatment in ATLAS-CONF-2023-006}
The fit configuration of this analysis matches the one presented in Section~\ref{sec:fit-setups}, the only difference being that the third fit is not employed. 
Therefore, \xtt\ is taken from the first fit, \ratio\ is extracted from the second fit, and both $\epsilon_b$ and \xz\ can be taken either from the first or the second setup.

\FloatBarrier