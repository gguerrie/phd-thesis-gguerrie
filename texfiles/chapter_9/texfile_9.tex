\chapter{Systematic uncertainties}
\label{sec:chapter_9}

As previously presented in Chapter~\ref{sec:profile-likelihood}, systematic uncertainties are included as NPs in the likelihood.
Additional histograms are produced, corresponding to $\pm 1 \sigma$ variations of each NP, calculated with respect to the nominal distribution.
Each NP affects the nominal distribution by altering either its shape, its normalisation, or both.
As mentioned in Chapter~\ref{sec:chapter_5}, in this thesis the fitted distributions consist of single bins, which means that the shape effects of NPs are implicitly not considered.
All the systematic effects are symmetrised with respect to the nominal histogram by considering half the difference of the $\pm 1 \sigma$ variations.
In certain scenarios, the systematic variation leads to a unidirectional $+(-)1 \sigma$ effect; 
this one-sided impact is mirrored against the nominal histogram to obtain the opposite variation as well.

Systematic uncertainties are categorised into detector-related or instrumental uncertainties, described in Section~\ref{sec:detectorUnc}, and modelling-related uncertainties, presented in Section~\ref{sec:ModellingUnc}.
For the modelling uncertainties affecting the $Z$-boson yields, the impact of the extrapolation from the generated phase space to the fiducial phase-space is subtracted from the absolute impact on the detector level yields in the $ee$ and $\mu\mu$ channels.
This subtraction is performed for each modelling NP individually.
More details about the impact of each NP are provided in Appendix~\ref{app:fiducial_Z}.
The background normalisation uncertainties are summarised in Table~\ref{tab:BkgUncert}, whereas Table~\ref{tab:Syst} lists all the systematic uncertainties considered in the analysis.

\section{Detector uncertainties}
\label{sec:detectorUnc}
The detector uncertainties, or experimental uncertainties, pertain to the modelling of the physics objects, as presented in Chapter~\ref{sec:chapter_4}, 
including integrated luminosity and \pileup.

\subsection{Electron and muon uncertainties}\label{sec:el_mu_uncertainties}
Uncertainties affecting charged lepton detection are subdivided into two main categories: the first encompasses lepton momentum resolution and scales, while the second includes uncertainties originating from trigger, reconstruction, isolation and identification efficiencies.
For the electron energy corrections, uncertainties obtained from the Run~2 calibrations using $Z\to ee$ events, following the prescription of Ref.~\cite{EGAM-2018-01}, are implemented.
These uncertainties are increased to account adequately for the differences in reconstruction between Run~2 and Run~3 of the LHC, as deduced from simulations.
The uncertainty in the muon momentum correction is estimated by separately varying the ID and MS components as estimated from $Z\to\mu\mu$ events, according to what is done in Ref.~\cite{PERF-2015-10}.
For both the energy and momentum correction uncertainties, the so-called \emph{breakdown} model is used, which furnishes multiple NPs for each component -- specifically, 73 for electrons and 8 for muons -- while preserving the input correlations.


Regarding electron efficiencies, different strategies are used for identification, reconstruction, isolation and for trigger SFs, as described in Section~\ref{sec: electrons-reco}.
The identification SF uncertainties are implemented following the \emph{simplified} model, consisting of 34 NPs; additional uncertainties are computed comparing simulation between Run~2 and Run~3. 
It was checked that the SFs in Run~3 are within the estimated uncertainties in Run~2.
Identification SF for electrons with $E_\mathrm{T} < 15~\gev$ are taken equal to 1 with conservative uncertainties.

Reconstruction and isolation SFs are deemed to be equal to 1, with conservative uncertainties assessed as the absolute value of the deviation from 1 to the corresponding SF value estimated during Run~2. %~\cite{egammatwiki1, egammatwiki2}.
The \emph{total} model, meaning that each component is treated as a single NP in the fit, is used for electron reconstruction and isolation SF uncertainties.
% Slides~\cite{egammatalk} show validation of the electron correction.
Uncertainties on the electron trigger SFs cover the differences between efficiencies seen in data and in simulation.
These are provided as a function of electron $\pt$ and $\eta$.

Muon SFs for identification, isolation and TTVA have been measured using the tag-and-probe method for muons with \pt$> 10~\gev$ in Run~3 data, with the related uncertainties represented respectively by 20, 9 and 7 NPs.
These measurements are performed using $Z\to\mu\mu$ events and the uncertainties on these SFs have been calculated using the methods described in~\cite{MUON-2018-03}. 
% More details can be found the Muon CP TWiki~\cite{MCPtwiki}.
Similarly to electrons, a model preserving the correlations is used for the SF uncertainties.
Since a dedicated MC-to-MC correction for the isolation is applied for the samples simulated with the \SHERPA generator, the uncertainty covering the difference between the isolation efficiencies is excluded from the set of uncertainties.

\subsection{Jet uncertainties}\label{sec:jets_uncertainties}

The JES and its uncertainty are computed as described in Section~\ref{sec: jets-reco}, where events featuring a vector boson and additional jets are used for the calibration of jets in the central region.
Moreover, dijet events are exploited to calibrate forward jets against those in the central region of the detector, whereas multi-jet events are used to calibrate high-\pt jets.
The measurements are combined and their uncertainty components decorrelated to obtain a set of eigenvectors depending on the jet \pt\ and $\eta$.
These are split into different categories, including 1 NP related to the $b$-jet energy scale, 16 NPs for the in-situ calibration, 3 NPs dedicated to the $\eta$ intercalibration, 
2 NPs for the flavour composition and response, 4 NPs arising from the uncertainty on the \pileup\ components, 1 NP originating from the uncertainty on the \emph{punch-through}\footnote{The punch-through, or particle leakage, is the act of a shower
particle to exit the calorimeter and enter the ATLAS muon spectrometer.} effect, and 1 NP describing high-\pt\ jets.
Additionally, dedicated uncertainties covering the difference between Run~2 and Run~3 based on the MC simulation, and preliminary calibrations are applied, resulting in 3 additional NPs, for a total of 31.

For the NNJvt SFs, no calibration is available at the time of writing; thus, a conservative 10\% uncertainty per bin is applied.
% This uncertainty is provided by the JetMET experts and covers the maximum difference seen between data and prediction.
This uncertainty, however, is applied only for the backgrounds in the $e\mu$ channel as the $\ttbar$ uncertainty should be fully absorbed by the $\epsilon_b$ parameter.
The uncertainty on the backgrounds is estimated by considering 10\% uncertainty per $b$-tagged jet, as only $b$-tagged jets are relevant in this analysis.
% Additional studies on the consistency of the proposed treatment of JVT uncertainty are described in Appendix~\ref{app:JVT_tests}.

JER uncertainties are included in the analysis as 16 NPs, coming from Run~2 based in-situ measurements, as well as early calibrations and Run~2 against Run~3 MC comparisons.

\subsection{Flavour-tagging uncertainties}
For the efficiency of tagging a true $b$-jet, the uncertainties from the calibration are derived using the Run~3 \ttbar\ dilepton data.
This uncertainty is a function of jet \pt.
More details about the uncertainty on the SFs are provided in Appendix~\ref{app:btagcalib}.
A conservative uncertainty is considered for the efficiency of mis-tagging.
Uncertainties corresponding to 20\% and 40\% are applied respectively on mis-tag rates of $c$- and light-flavour jets.
These uncertainties are inclusive in jet~\pt and $\eta$.
A total of 13 NPs are considered in the analysis, split between 9, 2, and 2 NPs respectively for the $b$-, $c$-, and light-jet-tagging.

\subsection{Luminosity and \pileup\ reweighting}
The uncertainty in the integrated luminosity for data recorded in 2022 is 2.2\%~\cite{ATL-DAPR-PUB-2023-001}, following the methodology discussed in Ref.~\cite{DAPR-2021-01}, using the LUCID-2 detector~\cite{LUCID2} 
for the primary luminosity measurements, complemented by measurements using the inner detector and calorimeters.
The uncertainty primarily arises from two factors: the first involves effects from horizontal-vertical correlations—known as non-factorisation—in the bunch-density distributions; these effects are observed during beam-separation scans used to determine the absolute luminosity scale.
The second factor contributing to the uncertainty involves the transfer from conditions in which absolute luminosity calibration data are collected, to those relevant for routine physics data-taking.
Various beam-, calibration-method-, consistency-, reproducibility- and stability-related effects provide smaller contributions to the overall uncertainty.
The uncertainty in the pile-up modelling is estimated by varying the average number of interactions per bunch-crossing by 4\% in the simulation.

\section{Signal and background modelling uncertainties}
\label{sec:ModellingUnc}

\subsection{Modelling of the \ttbar signal}\label{sec: ttbar_modelling}

The uncertainties on the \ttbar\ sample are divided into various categories.
The impact of the choice of the parton shower and hadronisation model is evaluated
by comparing the nominal \ttbar\ sample with another event sample produced with the
\POWHEGBOX[v2]~\cite{Frixione:2007nw,Nason:2004rx,Frixione:2007vw,Alioli:2010xd}
generator using the \NNPDF[3.0nlo]~\cite{Ball:2014uwa} parton distribution function.
Events in this sample were interfaced to \HERWIG[7.2.3]~\cite{Bahr:2008pv,Bellm:2015jjp},
using the \HERWIG[7.2] default set of tuned parameters~\cite{Bellm:2015jjp,Bellm:2017jjp}
and the \MMHT[lo] PDF set~\cite{Harland-Lang:2014zoa}.

Uncertainties related to the missing higher orders in the hard scattering calculation are estimated considering independent variations of the renormalisation and 
factorisation scales in the ME by factors of 0.5 and 2, but normalising the signal to the nominal cross-section.

A variation of the parameter \emph{Var3c} of the A14 tune~\cite{ATL-PHYS-PUB-2017-007} is considered independently. This corresponds to a variation of
the $\alpha_{\text{S}}$ coupling in the initial-state radiation (ISR) of the parton shower.
An uncertainty due to the final-state radiation modelling is considered by independent variations of the renormalisation scale for emissions from the parton shower by a factor of two up and down.
The uncertainty due to the choice of the \hdamp\ parameter is estimated by varying it by a factor of two up.

To account for the uncertainty due to the top quark \pt distribution mismodelling~\cite{TOPQ-2018-17}, the nominal \ttbar sample is compared with the same sample reweighted according to the NNLO QCD top \pt prediction, computed with the \texttt{MATRIX} program.
The difference between the reweighted and the nominal prediction is symmetrised and used as an uncertainty.
A more detailed study on the top \pt reweighting is presented in Appendix~\ref{app:TOP_PT_REW}.

\subsection{Modelling of the single-top events}
Similarly to the \ttbar uncertainties, scale variations in the ME, Var3c and for the final state radiation of the parton shower are considered for the $tW$ process.
Furthermore, because of the overlap with the \ttbar\ process, an uncertainty is considered by comparing the \emph{diagram removal} (DR) scheme~\cite{Re_2011}, with an alternative removal technique, the \emph{diagram subtraction} (DS)~\cite{Frixione:2008yi}.
The symmetrised difference between the distributions obtained from the DS and the DR methods is implemented as a modelling uncertainty.


\subsection{Modelling of the $V$+jets events}
In the dilepton channel, the $W$+jets events contribute only via fake and non-prompt leptons.
Several sources of systematic uncertainties in the modelling of the $Z$+jets events are considered.
The independent variation of the factorisation and renormalisation scales in the ME, as well as the simultaneous variations of the parton shower by factors of 0.5 and 2, are considered. Variations that differ by a factor of four (such as $\mu_r$ = 2$\mu_{r0}$, $\mu_f$ = 0.5$\mu_{f0}$ and $\mu_r$ = 0.5$\mu_{r0}$, $\mu_f$ = 2$\mu_{f0}$) are excluded. 
The variation having the maximum impact on the yields is symmetrised and used in the analysis.

For the modelling uncertainties affecting the $Z$ samples, the acceptance effect coming from the difference in the simulated phase-space and the analysis phase-space is subtracted.
The largest uncertainties of the scale variations in ME and PS for the $Z$ samples are the simultaneous variation of the factorisation and renormalisation scales by a factor of 0.5 in the ME and PS for $ee$,
and the simultaneous variation of the factorisation and renormalisation scales by a factor of two in the ME and PS for $\mu\mu$ events.
Summary of the impact of the different variations is provided in Appendix~\ref{app:fiducial_Z}.
The difference in the modelling of $Z$-boson events coming from the usage of different MC generators is described in Appendix~\ref{app:PP8_Z}; there was no sizeable effect due to the comparison between the \SHERPA[] and \POWHEGBOX[v2]+\PYTHIA[8] MC generators.
An additional check for the modelling of the $Z$-boson events is considered by reweighting the dilepton \pt\ distribution to match data perfectly.
This check is documented in Appendix~\ref{app:dileptonPt}.

%Appendix~\ref{app:CORRELATION_TESTS} describes tests where the PDF and scale uncertainties and $\alpha_{\text{s}}^{\text{FSR}}$ is correlated between \ttbar and $Z$ processes (and in case of the scales also with $tW$).
%It shows that the assumption about the correlation does not have a significant impact on the expected uncertainty on the ratio.

\subsection{Fake and non-prompt lepton modelling}
For the dilepton channel, a conservative normalisation uncertainty of 50\% is assigned for the fake lepton background in the $e\mu$ channel with one $b$-tagged jet and 100\% uncertainty is assigned to events with two $b$-tagged jets.
Additionally, a decorrelated, 100\% uncertainty is assigned to the fake lepton estimate in the $ee$ and $\mu\mu$ channels.

\subsection{Other SM backgrounds}
A conservative 50\% uncertainty is considered for the normalisation of the diboson processes.
This value is inspired by the 33\% uncertainty quoted in the Run~2 measurement~\cite{TOPQ-2018-17} for the two $b$-tagged region.
The uncertainty is adjusted upwards to account for the imperfect calculation of the $k$-factors for the normalisation.
Nevertheless, the uncertainty does not impact the precision of the measurement.
Additionally, a 50\% uncertainty is assigned for the combined contribution of $t\bar{t}Z$ and $t\bar{t}W$ processes.

\subsection{PDF uncertainty}
The PDF uncertainty for \ttbar\ and $Z$ modelling is estimated by considering the internal variations of the PDF4LHC21 PDF set. 
The PDF sets considered in the PDF4LHC21 recommendation are CT18~\cite{Hou:2019efy}, MSHT20~\cite{Bailey:2020ooq}, and NNPDF3.1.1~\cite{Ball_2017}. 
A set of 42 NPs, symmetrised and uncorrelated with each other, is used in the analysis.
This uncertainty is correlated between processes.

\section{MC statistical uncertainty}
The statistical uncertainty originating from the finite number of simulated events is considered in the analysis as a systematic uncertainty.
The uncertainty results in two NPs for each bin used in the final fit.
One NP combines the MC statistical uncertainty from all non-\ttbar\ processes in the $e\mu$ channel, and one NP represents the MC statistical uncertainty of the \ttbar\ process.
This separation is needed due to the way how the \ttbar\ histograms are generated as described in Section~\ref{sec:btagcountPL}.
In the same flavour channels, one NP per bin is used to represent the MC statistical uncertainty of all processes.
%The impact of MC statistics is found to be negligible, see Appendix~\ref{app:MC_STAT}.

\section{Pruning}
Often, not all sources of systematic uncertainty are significant in an analysis. 
Each NP contributes to increase the complexity of maximising the multidimensional likelihood. 
In order to limit the formation of local maxima, which can make the procedure typically unstable and possibly dependent on the starting point of the maximisation, the so-called pruning technique is applied.
This operation consists in removing any source of systematics that has an impact below a specific threshold.
In this thesis, a threshold on the normalisation effect of NPs is set at 0.01\%; NPs that fail to comply with the threshold requirements, are dropped. 

\begin{table}[!hbt]
    \centering
	\begin{tabular}{lc}
	\hline
    \textbf{Components} & \textbf{Uncert.} \\
	\hline
	Single-Top & 3.5\%\\
	Diboson & 50\%\\
	$t\bar{t}V$ & 50\%\\
	Fake leptons $ee$     & 100\% \\
	Fake leptons $\mu\mu$     & 100\% \\
	Fake leptons $e\mu\, 1 b$      & 50\% \\
	Fake leptons $e\mu\, 2 b$     & 100\% \\
	\hline
	\end{tabular}
	\caption{Uncertainties on the normalisation of background processes.}
	\label{tab:BkgUncert}
\end{table}

\newgeometry{a4paper, total={170mm,257mm}, left=20mm, top=20mm}
\begin{table}[!hbt]
	\centering
	\begin{footnotesize}
		\begin{tabular}{ccc}
			\toprule
      Systematic Uncertainty & Components \\
			\midrule
      \multicolumn{2}{c}{\textbf{Luminosity}}\\
			Luminosity & 1  \\
      \midrule
      \multicolumn{2}{c}{\textbf{Electrons}}\\
			Electron ID & 38   \\
			Electron isolation & 1   \\
			Electron reconstruction & 1   \\
			Electron energy scale & 63   \\
			Electron energy resolution & 10   \\
			Electron trigger & 1   \\
      \midrule
      \multicolumn{2}{c}{\textbf{Muons}}\\
			Muon ID & 20   \\
			Muon isolation & 9   \\
			Muon TTVA & 7   \\
			Muon trigger & 1   \\
			Muon momentum scale & 3   \\
			Muon momentum resolution & 2   \\
			Muon sagitta bias & 2   \\
			\midrule
      \multicolumn{2}{c}{\textbf{Jets}}\\
			Jet energy scale & 31  \\
			Jet energy resolution & 16  \\
			Jet vertex fraction & 1  \\
			\midrule
      \multicolumn{2}{c}{\textbf{Pile-up}}\\
			\Pileup profile & 1  \\
			\midrule
    %   \multicolumn{2}{c}{\textbf{\met}}\\
	% 		\met scale and resolution & 3  \\
	% 		\midrule
      \multicolumn{2}{c}{\textbf{Flavour tagging}}\\
			$b$-tagging efficiency & 9  \\
			$c$-tagging efficiency & 2  \\
			Light-jet-tagging efficiency & 2  \\
			\midrule
      \multicolumn{2}{c}{\textbf{Background Model}} \\
      Single top $\mu_{\text{R}}$ in ME & 1  \\
      Single top $\mu_{\text{F}}$ in ME & 1  \\
      Single top $\alpha_{\text{s}}^{\text{FSR}}$ & 1  \\
      Single top Var3c (ISR scale) & 1  \\
			Single top normalisation & 1  \\
		  Single top and \ttbar overlap & 1  \\
			Fake leptons normalisation & 4   \\
			Diboson normalisation & 1  \\
			$t\bar{t}V$ normalisation & 1  \\
			\midrule
      \multicolumn{2}{c}{\textbf{\ttbar Model}} \\
      \ttbar $\mu_\text{R}$ in ME & 1  \\
      \ttbar $\mu_\text{F}$ in ME & 1   \\
      \ttbar $\alpha_{\text{s}}^{\text{FSR}}$ & 1  \\
      \ttbar Var3c (ISR scale) & 1  \\
      \ttbar $h_{\text{damp}}$ & 1  \\
			\ttbar shower \& hadronisation & 1   \\
			Top quark \pt reweighting & 1  \\
			\midrule
      \multicolumn{2}{c}{\textbf{$Z$+jets Model}} \\
      $Z$+jets scales & 1  \\
			\midrule
      \multicolumn{2}{c}{\textbf{PDFs}} \\
      PDF4LHC21 & 42  \\
			\bottomrule
		\end{tabular}
        \caption{A summary of the systematic uncertainties considered in the analysis with their corresponding number of components that enter the uncertainty calculation.}
		\label{tab:Syst}
	\end{footnotesize}
\end{table}
\restoregeometry 

\section{Comparison with previous measurements}
\subsection*{Systematic uncertainties treatment in ATLAS-CONF-2022-070}

\paragraph*{Electrons and muons:} contrary to the main measurement, electron uncertainties are considered in a preliminary scheme, in which each of the categories listed in Table~\ref{tab:Syst} corresponds to a single NP.
Muon uncertainties are derived by exploiting a subset of the data collected during Run~3; these are implemented in the analysis according to a preliminary scheme.
The early scheme includes 2 NPs for muon identification, 2 NPs for isolation, 2 NPs for TTVA, 2 NPs for muon trigger, and 5 NPs for muon momentum scale and resolution, and sagitta.
Lepton trigger SFs are computed according to the procedure presented in Appendix~\ref{app:TriggerSF}.

\paragraph*{Jets:} to produce the initial set of uncertainties for JES and JER, the jet response, calculated as the reconstructed jet \pt over the truth jet \pt, is calculated as a function of jet \pt and jet $|\eta|$.
A set of \ttbar events is generated using 2018-like MC samples; differences between the track reconstruction and related \pileup\ contamination across the software releases of Run~2 and Run~3 are taken into account during the study.
The obtained distributions are then fitted with a Gaussian function to determine the mean values (JES) and the width (JER).
Additionally, the JVT algorithm is employed, using the uncertainties on SFs as estimated in Run~2.

\paragraph*{Flavour tagging:} as described in Section~\ref{sec: ftag-timeline}, preliminary prescriptions are applied, including SFs set to 1, with conservative uncertainties on the $b$-tagging and $c$- and light-mistagging rates, respectively, 10\%, 20\% and 40\%. 

\paragraph*{Top quark \pt\ reweighting:} this uncertainty is not considered in this analysis, since no NNLO (QCD) predictions for the top quark \pt\ distribution were available at the time of writing.

\paragraph*{$Z$-boson modelling:} given the phase space definition of $Z$ events, no additional acceptance effects are considered. 
% The uncertainty used originates from the simultaneous variation of the renormalisation and factorisation scale by a factor of two in the ME and the parton shower.
% The variation having the maximum impact on the yields, is symmetrised and used in the analysis.


\paragraph*{Luminosity and \pileup:} before the result of the LUCID2 detector, based on the Van-der-Meer scans, the uncertainty on the integrated luminosity collected is estimated to be about 10\%.
The \pileup\ uncertainty is estimated by shifting the average $\mu$ value in simulation by 3\% up and down.
This value is motivated by using the same shift in Run~2.

\paragraph*{SM backgrounds uncertainties:} the normalisation uncertainty on $\ttbar+V$ is not included, since the $\ttbar+V$ sample is not used in the analysis.


\subsection*{Systematic uncertainties treatment in ATLAS-CONF-2023-006}

\paragraph*{Electrons and muons:} uncertainties for electron and muons are implemented as described in Section~\ref{sec:el_mu_uncertainties}. 
Recommendations for electrons remain unchanged, whereas muon uncertainties are updated according to the GRL used.

\paragraph*{Jets:} the implementation of jet-related uncertainties follows the recipe provided in Section~\ref{sec:jets_uncertainties}.

\paragraph*{Flavour tagging:} efficiency SFs are estimated using the technique mentioned in Section~\ref{sec: bjets-reco}. 
Uncertainties on such SFs remain consistent with both ATLAS-CONF-2022-070 and the main measurement.

\paragraph*{Top quark \pt\ reweighting:} predictions for the top quark \pt\ distribution at the NNLO (QCD) are computed using \texttt{MATRIX}, and implemented in the analysis as described in Section~\ref{sec: ttbar_modelling}.

\paragraph*{$Z$-boson modelling:} the same strategy as in the main measurement is implemented in this analysis; the estimation of the acceptance effects relies on the integrated luminosity used in the measurement.

\paragraph*{Luminosity and \pileup:} this analysis employs the same uncertainty on the integrated luminosity and \pileup\ as is used in the main measurement.

\paragraph*{SM backgrounds uncertainties:} the normalisation uncertainty on $\ttbar+V$ is not included, since the $\ttbar+V$ sample is not used in the analysis.

\FloatBarrier