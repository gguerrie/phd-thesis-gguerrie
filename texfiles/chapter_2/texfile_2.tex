\chapter{The top quark, the $Z$-boson, and their properties}
\label{sec:chapter_2}
\section{The top quark}
Historically, the observation of charge-parity (CP)-violation in neutral $K$-mesons~\cite{PhysRevLett.13.138} posed a significant challenge to the SM, which, at the time, accommodated only two generations of quarks and leptons. 
The proposed solution involved the inclusion of a third generation of fermions, which was yet to be observed. With the inclusion of the third quark generation, 
the quark mixing matrix expanded from a $2 \times 2$ matrix with a single parameter, to a $3 \times 3$ matrix, defined by four parameters. The addition of these degrees of freedom allowed for an intrinsic complex phase, 
potentially leading to $CP$-violating terms~\cite{kobayashimaskawa:1973}. This theoretical shift became even more appealing following the discovery of a third-generation lepton, the $\tau$ lepton~\cite{PhysRevLett.35.1489}.

Upon detecting a third-generation quark, often referred to as the bottom\footnote{Sometimes, the name \emph{beauty} is used.} quark~\cite{PhysRevLett.35.1489}, it was logically anticipated that its weak isospin partner, the top quark, would exist. 
This assumption was confirmed in 1995, when the top quark was eventually discovered by the CDF~\cite{PhysRevLett.74.2626} and D\O \:~\cite{PhysRevLett.74.2632} Collaborations at the Fermilab Tevatron $p\bar{p}$ collider.

The top quark is the heaviest known elementary particle of the SM. 
Its large mass, around 172.5~GeV, and consequently the top-Higgs Yukawa coupling $y_t\sim 1$, suggest that the top quark plays an important role in the spontaneous symmetry breaking mechanism.
The top quark mean lifetime is approximately $\tau_t \sim10^{-25}$s~\cite{Workman:2022ynf}; it decays before hadronising, thus transferring all of its properties to the decay products. 


\subsection{Top quark production}
At hadron colliders, and within the SM, the top quark is predominantly\footnote{The SM accounts also for combination of the dominant processes listed, as four-top and three-top production. However, for the purpose of this thesis, the cross section of these processes can be considered negligible.} produced via two classes of processes: 
top quark pairs (\ttbar), via the strong interaction, or single-top production, through the electroweak interaction.

\subsubsection{Hard scattering in proton-proton collisions}
In the study of particle interactions, such as those involving proton-proton or electron-proton collisions, the proton is considered as mainly composed of three \emph{valence} quarks ($uud$), that dictate its quantum numbers and carry the most of its momentum.
Alongside valence quarks, there are \emph{sea} quarks and gluons, that carry the remaining part of the momentum of the proton. Together, the constituents of the proton are called \emph{partons}.
When two protons collide, a hard interaction occurs between their partons;
soft interactions involving the remainder of the hadron constituents produce many low energy particles which are largely uncorrelated with the hard collision.
In the centre-of-mass frame, in which protons are moving relativistically, the momentum of the parton is nearly collinear with the momentum of the proton. Thus, the colliding objects can be ideally visualised as streams of partons, each one carrying a fraction $x_i$\footnote{The quantity $x_i$ is often referred to as Bjorken-$x$.} of the longitudinal momentum of its parent hadron.
As a result, hadronic collisions can be \emph{factorised}~\cite{Collins:1989} into parton collisions weighted by \emph{parton distribution functions} (PDFs), describing the probability for the $i-th$ parton to carry $x_i$.
The cross-section, $\sigma$, of each process can be calculated by harnessing the factorisation properties of hadrons and producing an expression composed of a non-perturbative term, described by PDFs, 
and a perturbative part:

\begin{equation}\label{math: cross-section-expr}
    \sigma(pp\to X) \sum_{i,j=q,\overline{q}, g} \int_{4m^2}^{s} d\hat{s} \hat{F}_{ij}(\hat{s},s,\mu_F)\hat{\sigma}_{ij \to X}(\hat{s},m^2,\mu^2_f, \mu^2_r)
\end{equation}

where $X$ represents a generic final state, $s$ is the square of the $pp$ centre-of-mass energy, $\hat{\sigma}_{ij \to X}$ is the partonic cross-section. 
The factorisation scale, $\mu_f$, is a term that separates the non-perturbative and the perturbative part, represented by the renormalisation scale $\mu_r$. $\hat{F}_{ij}$ is the partonic density, expressed as

\begin{equation}\label{math: partonic-density}
    \hat{F}_{ij} = \dfrac{1}{s}\int_{\hat{s}}^{s} \dfrac{ds'}{s'}f_{i/p} \bigg( \mu_f^2. \frac{s'}{s} \bigg) f_{j/p}\bigg( \mu_f^2. \frac{s'}{s} \bigg)
\end{equation}
in which $f_{i(j)/p}$ represents the initial state of a parton $i(j)$ in a proton $p$, and $\hat{s}$ is the square of the partonic centre-of mass energy.
Typically, the factorisation scale is considered together with the appropriate scale for the renormalisation of the perturbative cross-section, given that both parameters are arbitrary.
This common scale, $\mu$, is usually taken to be similar to the energetic scale of the investigated process; for instance, when measuring the \ttbar cross section, $\mu$ is equal to the top quark mass, $\mu_r=\mu_f=m_t$; an exact calculation would not depend on these scales.
However, these scales affect finite-order calculations and must be taken into account as sources of uncertainty in the theoretical predictions.
In this thesis, these uncertainties are generally estimated by bounding the predictions using two sets of calculations, considering an independent variation of the renormalisation and factorisation scales in the ME by factors of 0.5 and 2. 
Further details are provided in Section~\ref{sec:chapter_9}.

\subsubsection{Top quark pair production}\label{sec: ttbar-theory}
In proton-proton collisions, the majority of top quark pair production originates from strong interactions. Nonetheless, a minor contribution also comes from electroweak processes. 
% This contribution, though present, falls beneath the theoretical uncertainty on \ttbar production deriving from the PDF uncertainty~\cite{PhysRevD.78.017503} and, as a consequence, can be considered negligible.
At leading order (LO), two main processes can be distinguished: quark-antiquark ($q\overline{q}$) annihilation and gluon-gluon ($gg$) fusion, as illustrated in Figure~\ref{fig: feynman-top-pair}.
The relative contribution to the production, whether due to the annihilation process or fusion, depends on the colliding objects and properties.
In proton-antiproton ($p\overline{p}$) collisions at energies close to the kinematic threshold, both quark and antiquark can be valence quarks. 
Therefore, the contribution from the $q\overline{q} \to \ttbar$ process can prevail, as in the case of the Tevatron collider. 
In $pp$ colliders like the LHC, antiquarks can only come from the quark sea, therefore gluon fusion dominates the production.
At higher energies, the $gg \to \ttbar$ process dominates for both $p\overline{p}$ and $pp$ collisions, as the gluon density increases with the energy. 
\begin{figure}[!ht]
\centering
    \includegraphics[width=0.8\textwidth]{figures/chapter_2/feynman.png}
    \caption{Feynman diagrams of the LO processes for $\ttbar$ production: (a) quark-antiquark annihilation ($q\overline{q} \to \ttbar$) and (b) gluon-gluon fusion ($gg \to \ttbar$).}
\label{fig: feynman-top-pair}
\end{figure}

In this thesis, the \TOPpp[2.0]~\cite{Czakon:2011xx} program is employed to calculate the $\ttbar$ cross-section; the predictions~\cite{Beneke:2011mq,Cacciari:2011hy,Baernreuther:2012ws,Czakon:2012zr,Czakon:2012pz,Czakon:2013goa} are computed at
next-to-next-to-leading order (NNLO) with next-to-next-to-leading resummation of logarithmic soft gluon terms (NNLL). 
The theoretical predictions estimation is further described in Chapter~\ref{sec:chapter_6}.
The uncertainties on the predictions originate from variations of renormalisation and factorisation scales, as well as from an uncertainty from the PDF. 
Experimental checks of the theoretical predictions for $\xtt$ show that all observed values agree within their uncertainties with these predictions, as summarised in Figure~\ref{fig: top-summary}.
\begin{figure}[!hb]
    \centering
        \includegraphics[width=0.8\textwidth]{figures/chapter_2/tt_curve_toplhcwg_jun23.pdf}
        \caption{Summary of LHC and Tevatron results~\cite{TOPQ-2018-40-fix,CMS-TOP-20-004,TOPQ-2018-39,TOPQ-2018-17,TOPQ-2020-02,CMS-TOP-18-002,ATLAS:2023sjw,CMS:2023qyl} for the top quark pair production
        cross-section measurements as a function of the centre-of-mass energy compared
        to the theoretical prediction~\cite{Czakon:2013goa}. Both theory and experimental results assume
        $m_t = 172.5$~GeV. The figure is taken from~\cite{LHCtopWGTwiki}.}
    \label{fig: top-summary}
    \end{figure}

\subsubsection{Single-top production}\label{sec: stop-theory}
Top quarks can be produced as single particles through the EW interaction involving the $tWb$ vertex. 
Three production models exist, regulated by the virtuality\footnote{See Section~\ref{sect: QCD}.} $Q^2$ of the $W$-boson:

\begin{itemize}
    \item \textbf{$s$-channel} \\
    A virtual space-like ($Q^2>0$) $W$-boson interacts with a $b$-quark inside the proton; production in the $t$-channel is the dominant source of single top
    quarks at the LHC. Figure~\ref{fig: feynman-stop} (a) illustrates the Feynman diagrams corresponding to this process.
    \item \textbf{$t$-channel} \\
    This channel, also called $t\overline{b}$ production, corresponds to a Drell-Yan process, in which a time-like $W$-boson with $Q^2\le (m_t+m_b)^2$ is produced 
    by the fusion of two quarks belonging to an $SU(2)$-isospin doublet. This process is displayed in Figure~\ref{fig: feynman-stop} (b)
    \item \textbf{$tW$-channel} \\
    The top quark is produced in association with a real ($Q^2 = m_W^2$) $W$-boson. 
    Figure~\ref{fig: feynman-stop} (c) illustrates the Feynman diagram corresponding to this process.
\end{itemize}

\begin{figure}[!hbt]
    \centering
        \includegraphics[width=0.8\textwidth]{figures/chapter_2/stop.png}
        \caption{Feynman diagrams of the LO processes for single-top production: (a) $s$-channel, (b) $t$-channel and (c) associated $tW$ production.}
    \label{fig: feynman-stop}
\end{figure}

In this work, cross-section predictions in the $s$-channel are calculated at next-to-leading order (NLO) in QCD with the \HATHOR[2.1]~\cite{Aliev:2010zk,Kant:2014oha} program.
The predicted cross-sections in the $t$-channel are calculated with the MCFM program~\cite{Campbell:2020fhf} at NNLO in QCD.
In the $tW$ channel, predictions are computed at NLO in QCD with the addition of third-order corrections of soft-gluon emissions by resumming NNLL terms~\cite{Kidonakis:2021vob}.
The top quark mass is set to 172.5\,GeV for all channels, and the PDF4LHC21 PDF set~\cite{Ball:2022hsh} is used.

Both the ATLAS and the CMS collaboration have extensively measured single-top production across the three channels~\cite{ATLAS:2023laura,TOPQ-2012-21,TOPQ-2015-05,ATLAS:2023lukas,
TOPQ-2011-17,TOPQ-2012-20,TOPQ-2015-16,
TOPQ-2015-01,TOPQ-2018-04,
CMS-TOP-11-021,CMS-TOP-12-038,CMS-TOP-17-011,
CMS-TOP-11-022,CMS-TOP-12-040,CMS-TOP-21-010,
CMS-TOP-13-009} covering all the processes at $\sqs=7,8$~TeV, and some at 5.02 and 13 TeV. The results are summarised in Figure~\ref{fig: single-top-summary}.

\begin{figure}[!hbt]
    \centering
        \includegraphics[width=0.8\textwidth]{figures/chapter_2/single-top-plots-nov23.pdf}
        \caption{Summary of ATLAS, CMS and combined measurements of the single top production cross-sections
        in various channels as a function of the center of mass energy. The measurements are compared to theoretical
        calculations based on NLO QCD, NLO QCD complemented with NNLL resummation and NNLO QCD ($t$-channel
        only). The figure is taken from~\cite{LHCtopWGTwiki}.}
    \label{fig: single-top-summary}
\end{figure}

\FloatBarrier

\subsection{Top quark decay}
As discussed before, the top quark decays via the weak interaction, with a mean life shorter than the typical timescale of the hadronisation processes, producing a $W$-boson and a down-type quark, $t\to W^+q(\overline{t}\to W^-\overline{q})$.
The flavour of the produced quark is determined by the CKM matrix. Assuming its unitarity and considering three generations of quarks, the predominant matrix element responsible 
for the top quark decay is $|V_{tq}|\simeq |V_{tb}|=0.999142^{+0.000018}_{-0.000023}$~\cite{CKMFitter:2021}.
This coupling allows to consider the top quarks as decaying essentially only into a $W$-boson and a $b$-quark.
The $W$-boson decays in about $1/3$ of the cases into a charged lepton ($e$, $\mu$ or $\tau$ ) and a neutrino, with all the three lepton flavours being produced at equal
rate. In the remaining $2/3$ of the cases, the $W$-boson decays into a quark-antiquark pair,
and the abundance of a given pair is instead determined by the magnitude of the relevant
CKM matrix element. Specifically, the CKM mechanism suppresses the production of $b$-quarks as $|V_{cb}|\simeq 1.7\times 10^{-3}$. 
Therefore, the hadronic $W$-boson decay can be considered as a clean source of light quarks.
Experimentally, a $\ttbar$ pair decay can be characterised by the subsequent decays of the $W$-bosons into the following channels (see Figure~\ref{fig: ttbar-branching-ratios}).\\

\textbf{All-hadronic or fully hadronic}\footnote{Sometimes, this channel can be also called \emph{all-jets}.} ($\ttbar\to W^+bW^-\overline{b}\to \overline{q}q'b\:q''\overline{q}'''\overline{b}$):
representing $\sim$4/9 of all the $\ttbar$ decays, in this channel both $W$-bosons decay hadronically into quarks. Thus, six jets characterise the event: 
two $b$-jets from the top quark decay and four light jets from the $W$-boson decay. 
This process suffers from a large multijet background, which is quite difficult to model, leading to a lower signal-to-background ratio.
Moreover, matching reconstructed jets to the original partons produced in the $\ttbar$ decay
constitutes a significant challenge, due to the high combinatorial nature of the background.
This complication, combined to the fact that the experimental resolutions of jets are broader compared to charged leptons, impacts directly the computation of variables like reconstructed invariant masses.

\textbf{Dilepton or fully leptonic} ($\ttbar\to W^+bW^-\overline{b}\to \ell^+\nu b\:\ell^-\overline{\nu}\overline{b}$): 
enclosing $\sim$1/9 of all the $\ttbar$ decays, this channel is characterised by the leptonic decay of both the $W$-bosons into a charged lepton, and a related neutrino.
This mode yields an extremely pure sample of top quark events; moreover, because it involves only two jets, the measurement has reduced uncertainties originating from reconstruction of jet kinematics. 
On the other hand, this process suffers both from poor statistics, and by the presence of two neutrinos that remain undetected.

\textbf{Single-lepton or Semileptonic}\footnote{Sometimes, this channel can be also called \emph{lepton+jets} ($\ell$+jets).} ($\ttbar\to W^+bW^-\overline{b}\to \ell^+\nu b\:q\overline{q}'\overline{b}$ or $\ttbar\to W^+bW^-\overline{b}\to \overline{q}q'b\: \ell^-\overline{\nu} \overline{b}$): 
encompassing $\sim$4/9 of all the $\ttbar$ decays, in this process one $W$-boson decays hadronically into two quarks and the other decays leptonically into a charged lepton and a corresponding neutrino.
The presence of a single high \pt\footnote{A particle's \pt, is defined as the projection of the particle momentum ($p$) on the plane orthogonal to the $z$ axis. See Section~\ref{sect: coordinate system}.} lepton and of four or more jets (two of them coming from $b$-quarks) allows to suppress the multijet and the $W$+jet backgrounds respectively.
This set of features grants the single-lepton channel a larger signal-to-background ratio compared to the all-hadronic channel, and higher branching ratio compared to the dilepton channel. 
Moreover, the presence of a single neutrino simplifies its reconstruction process, being it the only source of missing energy.


$W$-boson decays involving $\tau$ leptons are generally not directly measurable due to the short lifetime  of the leptons $\tau_{\tau}=2.9\times 10^{-13}$s~\cite{Workman:2022ynf}. 
In these processes, the $\tau$ lepton decays $62.96\%$~\cite{Workman:2022ynf} of the times hadronically, into a $\tau$ neutrino and one or more hadrons. 
In the remaining cases, either an electron or a muon, and two neutrinos are produced. 
Usually, analyses do not consider leptonically decaying taus in either the dilepton or single-lepton channel; nevertheless, these decays are implicitly included in the events considered.
Analyses involving the identification of a hadronically decaying $\tau$ are treated apart.

\begin{figure}[!hbt]
    \centering
        \includegraphics[width=0.6\textwidth]{figures/chapter_2/branching-fractions.png}
        \caption{Top quark pair branching fractions.}
    \label{fig: ttbar-branching-ratios}
\end{figure}

\subsection{Top quark mass}
There are a number of free parameters within the SM\footnote{The complete list of the free parameters of the SM is summarised in Section~\ref{sect: Fermions masses}.}, and the top quark mass happens to be one of them. 
It cannot be calculated from first principles, therefore precise measurements of $m_t$ provide a crucial benchmark for the consistency of the EW parameters in the SM~\cite{Baak:2014ora,Haller:2018nnx,Haller:2022eyb}.
Moreover, the measurement of $m_t$ has significant implications for understanding the stability of the electroweak vacuum, and it plays a crucial role in our knowledge of the early stages of the universe~\cite{ISIDORI2001387}.
The mass of the top quark has been measured in the experiments conducted at both the Tevatron and the LHC, involving various energies and both $\ttbar$ and single-top production channels~\cite{
arXiv:1403.4427,TOPQ-2013-02,TOPQ-2016-03,TOPQ-2017-03,TOPQ-2015-03,TOPQ-2013-03,TOPQ-2017-17,ATLAS:2022andrea,
CMS-TOP-11-015,CMS-TOP-11-016,CMS-TOP-11-017,CMS-TOP-14-022,CMS-TOP-15-001,CMS-TOP-17-007,
CMS-TOP-19-009,CMS-PAS-TOP-20-008,
% CMS:2022emx,
CMS:2023ebf}.
Among the various results, the world combination of the top quark measurements reports $m_t^{\textrm{world}}=172.69\pm 0.30$~GeV~\cite{Workman:2022ynf}, whereas the ATLAS and CMS combination from 2023 yields $m_t^{\textrm{ATLAS+CMS}}=172.52\pm 0.33$~GeV~\cite{ATLAS-CONF-2023-066}.

The investigation of the top quark mass is a vital element of experimental physics, one of the most relevant aspects of which is the precise determination of the parameters being experimentally measured.
Direct estimations employ the reconstruction of invariant mass from the decay products of the top quark, and then comparing the distribution observed in data with predictions obtained using different $m_t$ values as input parameter in the simulation of the full process, including non-perturbative effects.
Such direct measurements are usually considered possessing an intrinsic theoretical uncertainty of the order of $\Lambda_{QCD}$ related to the inclusion of non-perturbative effects in the predictions, even if the argument is still under debate in the theory community\footnote{Recent studies suggest that the implementation of EW corrections in computing $m_t$ could help in understanding the scale-dependent top quark masses obtained from the LHC and Tevatron data~\cite{Kataev:2022dua}.}~\cite{Marquard:2015qpa,Beneke:2016cbu}.
To avoid these interpretation issues, so-called ``indirect'' top-quark mass measurements are performed by estimating $m_t^{\textrm{pole}}$\footnote{The top quark pole mass $m_t^{\textrm{pole}}$, corresponds to the definition of the mass of a free particle~\cite{nason2018mass}.} via the top-quark production cross-section dependence on it~\cite{TOPQ-2014-06,TOPQ-2013-04,TOPQ-2017-09,CMS-TOP-12-022,CMS-TOP-18-004,CMS-TOP-21-008}.
Another possible strategy is to exploit purely leptonic distributions, avoiding the hadronisation issue, and achieving a theoretically cleaner measurement of $m_t^{\textrm{pole}}$~\cite{TOPQ-2015-02}.
Currently, the most precise estimate of the pole-mass with indirect measurements is $m_t^{\textrm{pole}}=170.5\pm0.8$~GeV~\cite{CMS-TOP-18-004}.

In this thesis, unless stated otherwise, a top quark mass of 172.5~GeV is adopted for the cross-section calculations and the Monte Carlo samples. 
This approach aligns with the recommendations of the LHC Top Physics Working Group~\cite{LHCTopWG} and maintains consistency with the previous analyses using Run 2 data~\cite{TOPQ-2015-09,STDM-2016-02}.

\section{The $Z$-boson}
The discovery of the $Z$-boson, along with the discovery of the $W$-boson, was achieved in 1983 by the joint efforts of the UA1 and UA2 collaborations at the S$p\bar{p}$S collider at CERN~\cite{UA2:1983mlz,ARNISON1983103,ARNISON1984241}.
These events represent the culmination of a series of accomplishments that began in 1973, with the observation of Neutral Currents; immediately afterwards, the charm quark was discovered in 1974, the tau lepton in 1975, and the bottom quark in 1977.
The combination of these experimental milestones and the theoretical breakthrough represented by the renormalisation of the Gauge Theory has outstandingly solidified the SM as an experimentally successful theory.

The $Z$-boson serves as one of the most effective tools for investigating the features of the SM with unprecedented accuracy, as demonstrated by the experimental campaigns at the Large Electron-Positron Collider (LEP)~\cite{RICHTER197647}, the Stanford Linear Collider (SLC)~\cite{osti_826885}, and hadron colliders.


\subsection{$Z$-boson properties}
The $Z$-boson lineshape parameters, such as the mass ($m_Z$), the total width ($\Gamma_Z$) and the partial widths ($\Gamma_{f\bar{f}}$), have been determined from an analysis of the production cross sections of fermionic final states starting from $e^+e^-$ collisions, constituting arguably the most significant achievement of LEP.
The shape of the cross-section around the $Z$ peak, $\sigma(s)$, can be estimated through an analytic expression:
\begin{equation}
    \sigma(s) = \sigma^0_{f\bar{f}}\frac{s\Gamma_Z^2}{(s-m_Z^2)^2+\frac{s^2}{m_Z^2}\Gamma_Z^2},
\end{equation}
where $\sigma^0_{f\bar{f}}$ represents the cross-section of the $e^+e^-\rightarrow f\bar{f}$ process at $\sqs=m_Z$. 
Currently, the most up-to-date values~\cite{Workman:2022ynf} for the $Z$-boson mass and total width are: 
\begin{align*}
    m_Z &= 91.1876\pm0.0021 \:\gev, \\
    \Gamma_Z &= 2.4955\pm0.002 \:\gev.
\end{align*}
On the other hand, the pole cross section can be described as a function of the $Z$-boson partial widths:
\begin{equation}
    \sigma^0_{f\bar{f}} = \frac{12\pi}{m_Z^2}\frac{\Gamma_{ee}\Gamma_{f\bar{f}}}{\Gamma_Z^2}.
\end{equation}
In terms of total width, the decay of the $Z$-boson can be expressed as:
\begin{equation}
    \Gamma_Z = N_{\nu}\Gamma_{\nu} + 3\Gamma_{ee} + \Gamma_{\text{hadrons}},
\end{equation}
where $N_{\nu}$ is the number of neutrino families. 
Experiments at LEP determined $N_{\nu}$ to be $2.9840\pm0.0082$, in agreement with the three observed generations of fundamental fermions~\cite{2006257}.
Figure~\ref{fig: lineshape} displays the predicted cross-sections for different numbers of generations around the $Z$ peak.

\begin{figure}[!hb]
\centering
    \includegraphics[width=0.6\textwidth]{figures/chapter_2/lineshape.png}
    \caption{Measurements of the hadron production cross-section around the $Z$-boson resonance. 
    The curves indicate the predicted cross-section for two, three and four neutrino species with SM couplings and negligible mass. 
    Figure taken from~\cite{2006257}.}
    \label{fig: lineshape}
\end{figure}
More recent studies~\cite{Voutsinas_2020,Janot_2020} have applied corrections to the LEP results, including the effect of correlated luminosity systematics, and used an improved cross-section calculation to obtain $N_{\nu} = 2.9963 \pm 0.0074$.

\subsection{$Z$-boson production}
The dominant contribution to the $Z$-boson at the LHC is given by the neutral-current \emph{Drell-Yan} process~\cite{PhysRevLett.25.316}. 
The annihilation of a quark-antiquark pair leads to the creation of a $Z$-boson (or a virtual photon), consequently decaying in a pair of high transverse momentum\footnote{For the definition of transverse momentum, see Section~\ref{sect: coordinate system}.} fermions.
At the LHC, measuring the $Z$-boson decaying into leptons is preferred because of clearer signals, reduced backgrounds, and enhanced energy and momentum resolution.
Figure~\ref{fig: drell-yan} reports a representative LO Feynman diagram for the Drell-Yan process, with leptons in the final states.

\begin{figure}[!hb]
\centering
    \includegraphics[width=0.3\textwidth]{figures/chapter_2/drell-yan.png}
    \caption{Leading order Feynman diagram for the lepton pair production in the Drell-Yan process.}
    \label{fig: drell-yan}
\end{figure}

As shown in Figure~\ref{fig: drell-yanNLO}, at NLO the production of a $Z$-boson occurs alongside a jet. 
This process includes both an electroweak vertex and a strong interaction vertex.

\begin{figure}[!hb]
\centering
    \subfloat[]{\includegraphics[width=0.24\textwidth]{figures/chapter_2/drell-yanNLO1.png}}
    \subfloat[]{\includegraphics[width=0.24\textwidth]{figures/chapter_2/drell-yanNLO2.png}}
    \subfloat[]{\includegraphics[width=0.24\textwidth]{figures/chapter_2/drell-yanNLO3.png}}
    \subfloat[]{\includegraphics[width=0.24\textwidth]{figures/chapter_2/drell-yanNLO4.png}}
    \caption{NLO Feynman diagrams for the lepton pair production in the Drell-Yan process.}
    \label{fig: drell-yanNLO}
\end{figure}

The NLO Drell-Yan process enables the examination of perturbative QCD across a broad range of kinematics and varying jet multiplicities.
A high level of precision can be achieved in these studies, helping to refine the PDFs, which represent a primary source of uncertainty in modeling final states with multiple partons.

Since its discovery, several measurements of the $Z$-boson cross-section have been performed with $p\bar{p}$ collisions by the CDF and D0 collaborations, at the centre-of-mass energies of $\sqs~=~1.8$~TeV and $\sqs~=~1.96$~TeV~\cite{Degenhardt:2006vx,Abulencia_2007}, 
as well as with $pp$ collisions at the LHC by the ATLAS and CMS experiments~\cite{STDM-2011-06,STDM-2012-20,STDM-2014-12,STDM-2015-03,STDM-2018-06,CMS-EWK-10-005,CMS-SMP-12-011,CMS-SMP-13-003,CMS-SMP-14-003,CMS:2023foa,CMS-PAS-SMP-22-017}.

\subsection{$Z$-boson early measurements at the LHC}

The generation of $Z$-bosons at hadron colliders is characterised by a large production rate and provides a distinct signature, thanks to the high transverse momentum leptons in the final state.
These characteristics, together with its well-known properties, such as precise mass and decays, make the $Z$-boson a ``standard candle'' in the SM, a vital benchmark tool for detector calibration and alignment in hadron collider physics. 

In early stages of data-taking, such as the ones described in this thesis, when experimental conditions change, including shifts in the centre-of-mass energy or modifications to the detector configuration, 
measuring the $Z$-boson is often one of the first steps~\cite{STDM-2016-01}. 

As further described in Section~\ref{sec:chapter_4}, several detector calibrations rely on the precise knowledge of the $Z$ peak energy, such as the lepton reconstruction.
Moreover, the monitoring of the luminosity can be enhanced by using the counts of reconstructed $Z \rightarrow \ell \ell$ events, or \emph{Z counting technique}~\cite{ATL-DAPR-PUB-2021-001}.
Exploiting the knowledge of $Z$-boson phenomenology ensures that detectors are correctly calibrated and functioning as intended, paving the way for more complex and less well-known analyses.

Moreover, it is usually beneficial to exploit the high precision attainable in the $Z$-boson measurements to enhance analyses that would be otherwise challenging during the initial phases of data collection.
For instance, as presented in Section~\ref{sec:chapter_5}, by calculating the ratio between the top quark pair cross-section and the $Z$-boson cross-section, 
it is possible to mutually cancel several sources of systematic uncertainty and become sensitive to the gluon-to-quark PDF ratio. 
Several measurements have been conducted by the ATLAS collaboration at \sqs=7,8, and 13~TeV in order to probe gluon PDFs in different Bjorken-$x$ regions~\cite{STDM-2016-02}.