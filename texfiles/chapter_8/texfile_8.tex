\chapter{Prediction for the \texorpdfstring{$t\bar{t}$}{ttbar} over \texorpdfstring{$Z$}{Z} production cross-section}
\label{sec:ratioCalculation}

This chapter describes both the theoretical prediction and the associated uncertainty for the ratio of \ttbar over $Z$-boson production.
As outlined in Chapter~\ref{sec:chapter_6}, the prediction for the \ttbar inclusive cross-section is estimated through the \TOPpp[2.0] program for different masses of the top quark and different PDF sets.
The calculation precision is NNLO in QCD with NNLL terms.
The $Z$-boson prediction is calculated using the MATRIX program, as documented in Appendix~\ref{app:ZbosonTheory}.
Its precision is at NNLO in QCD and NLO in EW coupling.
To calculate the $Z$-boson production cross-section, the fiducial phase-space is defined as follows: lepton $\pt > 27~\gev$, lepton $|\eta| < 2.5$ and $66 < m_{\ell\ell} < 116~\gev$.

In calculating the ratio, uncertainties arising from scale variation are assumed to be uncorrelated between the processes; meanwhile, uncertainties from PDF variations are assumed fully correlated.
The scale uncertainty is assessed through restricted scale variations, wherein $\mu_\text{R}$ and $\mu_{\text{F}}$ are independently varied by a factor of two, subject to the constraint that their values never diverge by more than this factor.
Table~\ref{tab:RatioPrediction} displays the predicted values for the ratio of \ttbar to $Z$-boson fiducial production cross-sections, considering different PDF sets and, in the case of the PDF4LHC21, varying top-quark masses as well.

\begin{table}[!hbpt]
\centering
\renewcommand{\arraystretch}{2}
\resizebox{\textwidth}{!}{
\begin{tabular}{ccccccc}
\toprule
PDF set & Top-quark mass [GeV] & $R_{\ttbar/Z}$ & Stat. & Scale unc. & PDF+$\alpha_{\text{S}}$ unc. & Total unc. \\
\midrule
PDF4LHC21 & 171.5 & 1.2720 & $\pm 0.0013$ & ${}^{+0.0321}_{-0.0463}$ & $\pm 0.0566$ & ${}^{+0.0651}_{-0.0731}$ \\
PDF4LHC21 & 172.5 & 1.2379 & $\pm 0.0012$ & ${}^{+0.0312}_{-0.0450}$ & $\pm 0.0551$ & ${}^{+0.0634}_{-0.0712}$ \\
PDF4LHC21 & 173.5 & 1.2048 & $\pm 0.0012$ & ${}^{+0.0304}_{-0.0438}$ & $\pm 0.0537$ & ${}^{+0.0617}_{-0.0693}$ \\
CT18 & 172.5 & 1.2661 & $\pm 0.0013$ & ${}^{+0.0317}_{-0.0462}$ & ${}^{+0.1031}_{-0.0913}$ & ${}^{+0.1078}_{-0.1024}$ \\
CT18A & 172.5 & 1.2237 & $\pm 0.0012$ & ${}^{+0.0306}_{-0.0447}$ & ${}^{+0.0720}_{-0.0645}$ & ${}^{+0.0783}_{-0.0785}$ \\
MSHT20 & 172.5 & 1.2340 & $\pm 0.0012$ & ${}^{+0.0315}_{-0.0451}$ & ${}^{+0.0539}_{-0.0351}$ & ${}^{+0.0624}_{-0.0571}$ \\
NNPDF4.0 & 172.5 & 1.1748 & $\pm 0.0012$ & ${}^{+0.0294}_{-0.0426}$ & ${}^{+0.0209}_{-0.0256}$ & ${}^{+0.0361}_{-0.0498}$ \\
ATLASpdf21 & 172.5 & 1.2440 & $\pm 0.0012$ & ${}^{+0.0314}_{-0.0453}$ & ${}^{+0.0713}_{-0.0619}$ & ${}^{+0.0779}_{-0.0767}$ \\
ABMP16 & 172.5 & 1.1269 & $\pm 0.0011$ & ${}^{+0.0284}_{-0.0413}$ & $\pm 0.0378$ & ${}^{+0.0473}_{-0.0560}$ \\
\bottomrule
\end{tabular}
}
\caption{Prediction and uncertainties on the ratio of \ttbar and $Z$-boson production cross-section at centre-of-mass energy of $13.6~\tev$. For the $Z$-boson prediction, a fiducial phase-space defined by: lepton $\pt > 27~\gev$, lepton $|\eta| < 2.5$ and $66 < m_{\ell\ell} < 116~\gev$ is considered. The prediction for the \ttbar inclusive cross-section is estimated using the \TOPpp[2.0] program. The calculation precision is NNLO in QCD with NNLL terms.
The $Z$-boson prediction is calculated using the MATRIX program.
Its precision is NNLO in QCD and NLO in EW coupling.
Predictions for different PDF sets, and in the case of the PDF4LHC21 set also for different top-quark masses, are provided.}
\label{tab:RatioPrediction}
\end{table}

\newpage

\section{Comparison with previous measurements}
\subsection*{Ratio prediction estimation in ATLAS-CONF-2022-070}

During the calculation of the $Z$-boson production cross-section, a requirement of $m_{\ell\ell} > 40~\gev$ or $m_{\ell\ell} > 60~\gev$ or a fully fiducial prediction with lepton $\pt > 27~\gev$, lepton $|\eta| < 2.5$ and $ 66 < m_{\ell\ell} < 116~\gev$ is imposed.

The acceptances for dileptonic, same-flavour $Z$ events, either with dressed or bare leptons, in the context of the fully fiducial selection are:

\begin{align}
  \text{acc}_{\text{nom, $e^+e^-$ dressed}}     &= 0.105 \pm 0.003\\
  \text{acc}_{\text{nom, $e^+e^-$ bare}}        &= 0.099 \pm 0.003\\
  \text{acc}_{\text{nom, $\mu^+\mu^-$ dressed}} &= 0.109 \pm 0.003\\
  \text{acc}_{\text{nom, $\mu^+\mu^-$ bare}}    &= 0.107 \pm 0.003.
\end{align}
Further details are provided in Appendix~\ref{app:fiducial_Z-2022-070}.

In the calculation of the ratio, the uncertainty from the scale variation are assumed to be uncorrelated between the processes, whereas uncertainties from PDF variations, obtained from PDF4LHC21 eigenvector decomposition, 
are fully correlated.
The scale uncertainty is evaluated as described in the previous section.
Table~\ref{tab:RatioPrediction-2022-070} shows the predicted values for the ratio of \ttbar over $Z$-boson production cross-section, utilising the PDF4LHC21 prediction.

\begin{table}[!hbpt]
\centering
\resizebox{\textwidth}{!}{
\begin{tabular}{cccccc}
\toprule
Top-quark mass [GeV] & Fiducial selection for $Z$ & $R_{\ttbar/Z}$ & Scale unc. & PDF unc. & Total unc. \\
\midrule
171.5 & $m_{\ell\ell} > 40~\gev$ & 0.4349 & $\pm 0.0117$ & $\pm 0.0106$ & $\pm 0.0158$ \\
172.5 & $m_{\ell\ell} > 40~\gev$ & 0.4232 & $\pm 0.0114$ & $\pm 0.0103$ & $\pm 0.0154$ \\
173.5 & $m_{\ell\ell} > 40~\gev$ & 0.4120 & $\pm 0.0111$ & $\pm 0.0101$ & $\pm 0.0150$ \\
172.5 & $m_{\ell\ell} > 60~\gev$ & 0.4564 & $\pm 0.0123$ & $\pm 0.0077$ & $\pm 0.0145$ \\
%$66 < m_{\ell\ell} < 116~\gev$, $\pt^{\ell} > 27~\gev$, $|\eta|^{\ell} < 2.5$ & XXX  & XXX \\
\bottomrule
\end{tabular}
}
\caption{Prediction and uncertainties on the ratio of \ttbar and $Z$-boson production cross-section at centre-of-mass energy of $13.6~\tev$. Predictions for different fiducial phase-space for the $Z$-boson production are presented and for the different masses of the top quark. The prediction for the \ttbar inclusive cross-section is estimated using the \TOPpp[2.0] program using the top-quark mass of $\mtop = \SI{172.5}~\gev$ and using the PDF4LHC21 PDF set. The calculation precision is NNLO in QCD with NNLL terms.
The $Z$-boson prediction is calculated using the MATRIX program with PDF4LHC21 PDF set.
Its precision is NNLO in QCD and NLO in EW coupling.}
\label{tab:RatioPrediction-2022-070}
\end{table}

%==================================================================================================================

\subsection*{Ratio prediction estimation in ATLAS-CONF-2023-006}

In this measurement, an initial estimate of the ratio prediction of provided using different PDFs sets, as listed above.  
Similarly to the primary measurement, Table~\ref{tab:RatioPrediction-2023-006} shows the predicted values for the ratio of \ttbar over $Z$-boson fiducial production cross-section.
In this calculation, a different value for the $Z$-boson fiducial prediction is employed with respect to the main measurement, as described in Section~\ref{sec:DataMC_definitions_Z}.
Morevoer, at the time of the writing of the table, the individual components of the total uncertainty were not available for all the PDF sets considered.

\begin{table}[!hbpt]
    \centering
    \renewcommand{\arraystretch}{2}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{ccccccc}
    \toprule
    PDF set & Top-quark mass [GeV] & $R_{\ttbar/Z}$ & Stat. & Scale unc. & PDF unc. & Total unc. \\
    \midrule
    PDF4LHC21 & 171.5 & 1.2796 & $\pm 0.0077$ & ${}^{+0.0323}_{-0.0466}$ & $\pm 0.0782$ & ${}^{+0.0849}_{-0.0913}$ \\
    PDF4LHC21 & 172.5 & 1.2453 & $\pm 0.0075$ & ${}^{+0.0314}_{-0.0453}$ & $\pm 0.0761$ & ${}^{+0.0827}_{-0.0889}$ \\
    PDF4LHC21 & 173.5 & 1.2121 & $\pm 0.0073$ & ${}^{+0.0306}_{-0.0441}$ & $\pm 0.0741$ & ${}^{+0.0805}_{-0.0866}$ \\
    CT18 & 172.5 & 1.2768 & --- & --- & --- & ${}^{+0.0649}_{-0.0804}$ \\
    CT18A & 172.5 & 1.2529 & --- & --- & --- & ${}^{+0.0565}_{-0.0792}$ \\
    MSHT20 & 172.5 & 1.2375 & --- & --- & --- & ${}^{+0.0601}_{-0.0578}$ \\
    NNPDF4.0 & 172.5 & 1.218 & $\pm 0.0061$ & ${}^{+0.0305}_{-0.0442}$ & $\pm 0.0143$ & ${}^{+0.0342}_{-0.0468}$ \\
    ATLASpdf21 & 172.5 & 1.252 & --- & --- & --- & ${}^{+0.0624}_{-0.0745}$ \\ 
    ABMP16 & 172.5 & 1.1342 & $\pm 0.0045$ & ${}^{+0.0286}_{-0.0416}$ & $\pm 0.0380$ & ${}^{+0.0478}_{-0.0565}$ \\
    \bottomrule
    \end{tabular}
    }
    \caption{Prediction and uncertainties on the ratio of \ttbar and $Z$-boson production cross-section at centre-of-mass energy of $13.6~\tev$. For the $Z$-boson prediction, a fiducial phase-space defined by: lepton $\pt > 27~\gev$, lepton $|\eta| < 2.5$ and $66 < m_{\ell\ell} < 116~\gev$ is considered. The prediction for the \ttbar inclusive cross-section is estimated using the \TOPpp[2.0] program. The calculation precision is NNLO in QCD with NNLL terms.
    The $Z$-boson prediction is calculated using the MATRIX program.
    Its precision is NNLO in QCD and NLO in EW coupling.
    Predictions for different PDF sets, and in the case of the PDF4LHC21 set also for different top-quark masses, are provided.}
    \label{tab:RatioPrediction-2023-006}
\end{table}

\FloatBarrier
