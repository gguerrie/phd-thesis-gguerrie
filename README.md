[![pipeline status](https://gitlab.cern.ch/gguerrie/phd-thesis-gguerrie/badges/main/pipeline.svg)](https://gitlab.cern.ch/gguerrie/phd-thesis-gguerrie/-/commits/main) 
[![Latest Release](https://gitlab.cern.ch/gguerrie/phd-thesis-gguerrie/-/badges/release.svg)](https://gitlab.cern.ch/gguerrie/phd-thesis-gguerrie/-/releases) 

Project for a PhD thesis
====================

Howdy! This repository is the container of the analyses that Giovanni brought out during is PhD. 
It ain't much, but it's honest work.

## Issues

If there is any issue with the CI pipelines, any job failing, any bug spotted or any question relating the project configuration or GitLab workflow, please pretend nothing happened and carry on.

## Continuous integration

On the left side-bar there is a menu called CI/CD. Here will appear the different pipelines triggered after each commit. The PO-GitLab CI tools will check after each commit has been pushed to the repository that the document is properly formatted for publication and using the latest version of the ATLAS Latex Template.

### Useful artifacts
The CI pipeline produces a set of useful artifacts to be used in cross checking:
1. Thesis file in `.pdf` format. Definitely useful. *IMPORTANT: two pdf files are produced, with and without line numbers. Yes, I am lazy.* 
2. Spellcheck `.txt` file: thi file is produced thanks to the [aspell](http://aspell.net/) package. The file containes a list of words that do not match the british dictionary of the tool. Unfortunately acronyms, or physics jargon are going to be included as well, but better safe than sorry. Check the `./scripts/spellcheck.sh` and `./scripts/spellcheck_refined.sh` macros for more info.
3. Compressed version of the thesis: quite useful in the review stage, where the text matters and it's better to have a file that is 3Mb rather than 30Mb :D

## Get the note
In order to get this repository, do 
```
git clone https://gitlab.cern.ch/gguerrie/phd-thesis-gguerrie.git
```
## Compile the note on lxplus
Make sure that you have exported the following variable (in lxplus-like environments):
```
export PATH=/cvmfs/sft.cern.ch/lcg/external/texlive/latest/bin/x86_64-linux:$PATH
```
Then, execute
```
make
```
This will produce the PDF file.

## Compile the note using Docker
Once inside the main directory, execute
```
docker run --rm -it -v ${pwd}:/thesis texlive/texlive:TL2022-historic
```
To completely compile the pdf, cd into  `/thesis` and run:
```
make
```
This will use the `Makefile` file.

For a sloppy compilation (not very recommended), just run:
```
pdflatex phd-thesis-gguerrie
biber phd-thesis-gguerrie
pdflatex phd-thesis-gguerrie
```
