#!/bin/bash

file="all_misspelled_words.txt"
tmpfile=$(mktemp)

# Flag to determine if we are in a header section
in_header=1

while IFS= read -r line; do
    if [[ $line == "-----------------------" ]]; then
        in_header=1
        echo "$line" >> "$tmpfile"
        continue
    elif [[ $line == Checking* ]]; then
        echo "$line" >> "$tmpfile"
        continue
    elif [[ $in_header -eq 1 ]]; then
        in_header=0
        words_to_remove=($(awk -F: '/:/ {print $2}' "$file" | sort | uniq -c | awk '$1 > 3 {print $2}'))
    fi

    remove_line=0
    for word in "${words_to_remove[@]}"; do
        if [[ $line == *":$word" ]]; then
            remove_line=1
            break
        fi
    done

    if [[ $remove_line -eq 0 ]]; then
        echo "$line" >> "$tmpfile"
    fi
done < "$file"

mv "$tmpfile" "$file"
