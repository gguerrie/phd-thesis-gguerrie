#!/bin/bash

# Creating/Clearing the output file
> all_misspelled_words.txt

# Looping through all .tex files in texfiles/ directory and its subdirectories
find texfiles/ -type f -name "*.tex" | while read -r texfile
do
    echo "Checking $texfile" >> all_misspelled_words.txt
    
    # Getting the list of misspelled words from aspell
    cat "$texfile" | aspell --lang=en_GB --mode=tex list > misspelled_words.txt

    
    # Getting line numbers for the misspelled words and writing to the output file
    grep -n -w -o -F -f misspelled_words.txt "$texfile" >> all_misspelled_words.txt

    
    # Adding a separator for easier reading
    echo "-----------------------" >> all_misspelled_words.txt
done

# Cleaning up the temporary misspelled words file
rm misspelled_words.txt
