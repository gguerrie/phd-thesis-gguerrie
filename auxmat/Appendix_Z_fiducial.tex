\chapter{Acceptances for fiducial-level $Z$}
\label{app:fiducial_Z}

This appendix describes the acceptances calculated for the fiducial-level selection on the $Z$ dilepton, same-flavor MC samples (i.e. $ee$ and $\mu\mu$).
A selection requiring all leptons to fulfill $p_{\text{T}} \geq \SI{27}{\gev}$ and $|\eta| < 2.5$, as well as a dilepton mass requirement of
$\SI{66}{\gev} < m_{\ell\ell} <\SI{116}{\gev}$ is applied to events on particle-level and on Born level separately.
This selection matches the reco-level selection used in the analysis.
For all passing events, per DSID, the individual weights are summed and finally scaled by the sum of all weights regardless of the selection, giving the acceptance

\begin{equation}
  \text{acc}_{\text{var}} = \frac{\sum_{\text{events passing selection}} \left( \text{weight}\left(\text{var}\right) \right)}{\sum_{\text{all events}} \left( \text{weight}\left(\text{var}\right) \right)}.
\end{equation}

The statistical uncertainties of the acceptances are calculated as
\begin{equation}
  \sigma_{\text{stat, }\text{acc}_{\text{var}}} = \frac{\sqrt{\sum_{\text{events passing selection}} \left[ \text{weight}\left(\text{var}\right)\right]^2}}{\sum_{\text{all events}} \left[ \text{weight}\left(\text{var}\right)\right]}.
\end{equation}

The weighted mean of the acceptances per DSID are calculated, using the filter efficiency of the DSID as weight.

These acceptances are given in \autoref{tab:acc_fid_Z}.
They are derived, using about $\SI{20}{\percent}$ of the events, ensuring that the statistical uncertainty for each DSID
is smaller or in the order of magnitude of $10^{-5}$, while the acceptances have an order of magnitude of $10^{-1}$.

The statistical uncertainties of the final acceptances are maximally of the order of magnitude of $10^{-6}$ and thus negligible.
The systematic uncertainty on the nominal acceptance is chosen as the largest deviation between nominal and the acceptances of the variations of ME and PS.
The nominal acceptances are

  \begin{align}
    \text{acc}_{\text{nom, $ee$}}     &= 0.336 \pm 0.010\\
    \text{acc}_{\text{nom, $\mu\mu$}} &= 0.337 \pm 0.008.
  \end{align}

\begin{table}[htb!]
  \centering
  \sisetup{
    tight-spacing           = true,
    input-open-uncertainty  = ,
    input-close-uncertainty = ,
    round-mode              = places,
    round-precision         = 3,
    table-align-text-pre    = false
    }%
    \begin{tabular}{
    l
    S[table-format=1.3]
    S[table-format=1.3]      
  }%
  \toprule
              \empty&\multicolumn{2}{c}{Acceptances}\\
              &{$ee$ events} & {$\mu\mu$ events}  \\

 \midrule
  Weight                                                 &  0.336 & 0.337   \\
  MUR05\_MUF05\_PDF303200\_PSMUR05\_PSMUF05              &  0.335 & 0.335   \\
  ME\_ONLY\_MUR05\_MUF05\_PDF303200\_PSMUR05\_PSMUF05    &  0.337 & 0.337   \\
  MUR05\_MUF1\_PDF303200\_PSMUR05\_PSMUF1                &  0.335 & 0.336   \\
  ME\_ONLY\_MUR05\_MUF1\_PDF303200\_PSMUR05\_PSMUF1      &  0.335 & 0.336   \\
  MUR1\_MUF05\_PDF303200\_PSMUR1\_PSMUF05                &  0.335 & 0.336   \\
  ME\_ONLY\_MUR1\_MUF05\_PDF303200\_PSMUR1\_PSMUF05      &  0.337 & 0.338   \\
  MUR1\_MUF2\_PDF303200\_PSMUR1\_PSMUF2                  &  0.344 & 0.343   \\
  ME\_ONLY\_MUR1\_MUF2\_PDF303200\_PSMUR1\_PSMUF2        &  0.335 & 0.336   \\
  MUR2\_MUF1\_PDF303200\_PSMUR2\_PSMUF1                  &  0.337 & 0.338   \\
  ME\_ONLY\_MUR2\_MUF1\_PDF303200\_PSMUR2\_PSMUF1        &  0.337 & 0.338   \\
  MUR2\_MUF2\_PDF303200\_PSMUR2\_PSMUF2                  &  0.346 & 0.345   \\
  ME\_ONLY\_MUR2\_MUF2\_PDF303200\_PSMUR2\_PSMUF2        &  0.336 & 0.337   \\
  \end{tabular}
  \caption{The particle-level acceptances for different variations are shown, separated for $ee$ and $\mu\mu$ events.
  The variations include the nominal (Weight) and scale variations, for both matrix element and parton shower, as well as matrix element-only (ME\_ONLY).}
  \label{tab:acc_fid_Z}
\end{table}

Additionally, the acceptances for only the dilepton mass requirement of $\SI{66}{\gev} < m_{\ell\ell} <\SI{116}{\gev}$ are calculated in the same way.
These acceptances, as well as the ratio between the full fiducial-selection and only the dilepton mass selection acceptances are given in \autoref{tab:acc_mll_fid_Z}.

\begin{table}[htb!]
  
  \centering
  \sisetup{
    tight-spacing           = true,
    input-open-uncertainty  = ,
    input-close-uncertainty = ,
    round-mode              = places,
    round-precision         = 3,
    table-align-text-pre    = false
    }%
    \resizebox{\textwidth}{!}{  
    \begin{tabular}{
    l
    S[table-format=1.3]
    S[table-format=1.3]      
    S[table-format=1.3]
    S[table-format=1.3] 
  }%
  \toprule
  \empty  &\multicolumn{2}{c}{Acceptances} &\multicolumn{2}{c}{Ratio of Acceptances} \\
              &{$ee$ events} & {$\mu\mu$ events} &{$ee$ events} & {$\mu\mu$ events}  \\
 \midrule
  Weight                                                 &  0.900 & 0.901   &  0.374	 &   0.374  \\
  MUR05\_MUF05\_PDF303200\_PSMUR05\_PSMUF05              &  0.903 & 0.905   &  0.370	 &   0.371  \\
  ME\_ONLY\_MUR05\_MUF05\_PDF303200\_PSMUR05\_PSMUF05    &  0.903 & 0.904   &  0.373	 &   0.373  \\
  MUR05\_MUF1\_PDF303200\_PSMUR05\_PSMUF1                &  0.900 & 0.901   &  0.373	 &   0.372  \\
  ME\_ONLY\_MUR05\_MUF1\_PDF303200\_PSMUR05\_PSMUF1      &  0.900 & 0.901   &  0.372	 &   0.373  \\
  MUR1\_MUF05\_PDF303200\_PSMUR1\_PSMUF05                &  0.904 & 0.905   &  0.370	 &   0.371  \\
  ME\_ONLY\_MUR1\_MUF05\_PDF303200\_PSMUR1\_PSMUF05      &  0.903 & 0.904   &  0.373	 &   0.374  \\
  MUR1\_MUF2\_PDF303200\_PSMUR1\_PSMUF2                  &  0.902 & 0.901   &  0.383	 &   0.381  \\
  ME\_ONLY\_MUR1\_MUF2\_PDF303200\_PSMUR1\_PSMUF2        &  0.899 & 0.900   &  0.373	 &   0.374  \\
  MUR2\_MUF1\_PDF303200\_PSMUR2\_PSMUF1                  &  0.901 & 0.901   &  0.375	 &   0.375  \\
  ME\_ONLY\_MUR2\_MUF1\_PDF303200\_PSMUR2\_PSMUF1        &  0.900 & 0.901   &  0.374	 &   0.375  \\
  MUR2\_MUF2\_PDF303200\_PSMUR2\_PSMUF2                  &  0.902 & 0.902   &  0.384	 &   0.382  \\
  ME\_ONLY\_MUR2\_MUF2\_PDF303200\_PSMUR2\_PSMUF2        &  0.899 & 0.899   &  0.374	 &   0.375  \\
  \end{tabular}
}
  \caption{The particle-level acceptances for the dilepton mass selection-only for different variations are shown, separated for $ee$ and $\mu\mu$ events.
  The variations include the nominal (Weight) and scale variations, for both matrix element and parton shower, as well as matrix element-only (ME\_ONLY).
  Additionally, the ratio of the full fiducial selection acceptances over the dilepton mass selection acceptances is shown.}
  \label{tab:acc_mll_fid_Z}
\end{table}


From the full-fiducial acceptances, the fiducial cross-section and reco-level scale variation uncertainties can be corrected. 
This is done by taking the deviation from one of ratio between the acceptance of each variance and the nominal acceptance, and then shifting the reco-level
uncertainty corresponding to the variation by this value.
These values are given in \autoref{tab:unc_fid_Z}.

\begin{table}[htb!]
  
  \centering
  \sisetup{
    tight-spacing           = true,
    input-open-uncertainty  = ,
    input-close-uncertainty = ,
    round-mode              = places,
    round-precision         = 2,
    table-align-text-pre    = false
    }%
    \resizebox{\textwidth}{!}{  
    \begin{tabular}{
    l
    S[table-format=1.2]
    S[table-format=1.2]  
    S[table-format=1.3]
    S[table-format=1.3]     
    S[table-format=1.3]
    S[table-format=1.3] 
  }%
  \toprule
  {Variation} &\multicolumn{2}{c}{Uncor. unc. in \%} &\multicolumn{2}{c}{acc. ratio} & \multicolumn{2}{c}{Cor. unc. in \%}\\
   &{$ee$ } & {$\mu\mu$ } &{$ee$ } & {$\mu\mu$ } &{$ee$ } & {$\mu\mu$ }  \\

 \midrule
  MUR05\_MUF05\_PDF303200\_PSMUR05\_PSMUF05              &  0.10   & 0.48  & 0.995 & 0.995 & 0.637  &  0.050    \\
  ME\_ONLY\_MUR05\_MUF05\_PDF303200\_PSMUR05\_PSMUF05    &  0.29   & 0.00  & 1.001 & 1.000 & 0.234  &  0.003    \\
  MUR05\_MUF1\_PDF303200\_PSMUR05\_PSMUF1                & -0.17   & 0.31  & 0.997 & 0.995 & 0.101  &  0.140    \\
  ME\_ONLY\_MUR05\_MUF1\_PDF303200\_PSMUR05\_PSMUF1      &  0.13   & 0.36  & 0.997 & 0.996 & 0.454  &  0.009    \\
  MUR1\_MUF05\_PDF303200\_PSMUR1\_PSMUF05                & -0.17   & 0.39  & 0.995 & 0.995 & 0.307  &  0.077    \\
  ME\_ONLY\_MUR1\_MUF05\_PDF303200\_PSMUR1\_PSMUF05      &  0.34   & 0.30  & 1.003 & 1.002 & 0.045  &  0.057    \\ 
  MUR1\_MUF2\_PDF303200\_PSMUR1\_PSMUF2                  &  2.49   & 2.16  & 1.024 & 1.019 & 0.099  &  0.283    \\
  ME\_ONLY\_MUR1\_MUF2\_PDF303200\_PSMUR1\_PSMUF2        & -0.27   & 0.28  & 0.997 & 0.997 & 0.006  &  0.018    \\ 
  MUR2\_MUF1\_PDF303200\_PSMUR2\_PSMUF1                  &  0.10   & 0.37  & 1.003 & 1.002 & 0.209  &  0.183    \\
  ME\_ONLY\_MUR2\_MUF1\_PDF303200\_PSMUR2\_PSMUF1        &  0.04   & 0.27  & 1.002 & 1.002 & 0.166  &  0.084    \\
  MUR2\_MUF2\_PDF303200\_PSMUR2\_PSMUF2                  &  2.76   & 2.70  & 1.029 & 1.023 & 0.103  &  0.392    \\
  ME\_ONLY\_MUR2\_MUF2\_PDF303200\_PSMUR2\_PSMUF2        & -0.18   & 0.06  & 1.000 & 1.000 & 0.186  &  0.067    \\
  \end{tabular}
}
\caption{The corrected and uncorrected ME and PS variation uncertainties on reco-level are shown, using the fiducial acceptances for the correction, separated for $ee$ and $\mu\mu$ events.
Furthermore, the ratio of the variation's acceptance and the nominal acceptance is shown.
The variations include the nominal (Weight) and scale variations, for both matrix element and parton shower, as well as matrix element-only (ME\_ONLY).}
\label{tab:unc_fid_Z}
\end{table}

As these uncertainties just effect the yields in the singular bins in the $Z$ signal region,
only the largest uncertainty of all variations is assigned as the corresponding uncertainty.
The reco-level ME and PS uncertainties for the $Z$ samples are
\begin{align}
  \sigma_{\text{ME+PS, $Z \to ee$}}     &= \SI{0.637}{\percent}\\
  \sigma_{\text{ME+PS, $Z \to \mu\mu$}} &= \SI{0.392}{\percent},
\end{align}
corresponding to the simultaneous variation of the factorisation and renormalisation scales by a factor of 0.5 in the ME and PS for $ee$ and 
the simultaneous variation of the factorisation and renormalisation scales by a factor of two in the ME and PS for $\mu\mu$.

The PDF systematic uncertainties are corrected according to the fiducial selection in the same way.
