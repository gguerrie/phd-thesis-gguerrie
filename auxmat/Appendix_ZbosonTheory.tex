\chapter{$Z$-boson cross-section prediction}
\label{app:ZbosonTheory}
In this appendix, the calculation of the higher order cross sections for the $W^{\pm}$ and $Z$-bosons are reported.
To perform these computations, \texttt{MATRIX} v2~\cite{Grazzini:2017mhc} was used,
which can compute cross sections up to NNLO QCD and NLO EW,
and combine them by means of an additive prescription.

Uncertainties on the cross sections arising from missing higher orders (standard 7-points variations for the renormalisation scale $\mu_{R}$ and factorisation scale $\mu_{F}$), the variation of the strong coupling constant $\alpha_s$, and 
variations of the input PDFs are also assessed and provided.

\section{Input parameters and EW scheme}
In the following, the SM input parameters used in the computation are summarised.
For more details, these are also reported in Section~5.1 of the \texttt{MATRIX} documentation~\cite{Grazzini:2017mhc}.

%
\begin{table}[hb]
    \centering
    \begin{tabular}{llrcc}
      \toprule
      Parameter   & Value \\
      \midrule
      $m_W$       & $80.385 \, \text{GeV}$ & \\
      $\Gamma_W$  & $2.091 \, \text{GeV}$ & \\
      $m_Z$       & $91.1876 \, \text{GeV}$ & \\ 
      $\Gamma_Z$  & $2.4952 \, \text{GeV}$ & \\
      $m_H$       & $125 \, \text{GeV}$ & \\ 
      $\Gamma_H$  & $0.00407 \, \text{GeV}$ & \\
      $m_t$       & $173.2 \,  \text{GeV}$ & \\ 
      $\Gamma_t$  & $1.44262 \, \text{GeV}$ & \\ 
      $G_F$       & $1.16638 \, \times 10^{-5}\, \text{GeV}^{-2}$ & \\ 
      $\sin^{2}(\theta_{W})$ & $0.222897222524$ & \\ 
      $1/\alpha_{\mathrm{QED}}(m_{Z})$ & $132.233229791$ & \\ 
      \bottomrule
      \end{tabular}
      \caption{EW input parameters used for the calculation of inclusive EW vector boson cross sections.}
    \vspace{0.3cm}
  \label{tab:parameters_input} 
\end{table}

%
The $G_{\mu}$ scheme is employed. When considering leptonic final states produced via off-shell EW bosons, 
the complex-mass scheme is employed, i.e. complex $W$- and $Z$-boson masses are used and the EW mixing angle is defined 
as $\cos\theta_W^2 = \left(m^2_W - i \Gamma_W m_W\right)/\left(m^2_Z - i \Gamma_Z m_Z\right)$
and $\alpha = \sqrt{2}\,G_{\mu}\,m^2_W \sin^2\theta_W/\pi$.
The PDG values of $G_F$, $m_W$, $\Gamma_W $, $m_Z $ and $\Gamma_Z$ are used and reported for completeness 
in Table~\ref{tab:parameters_input}.

%
All processes considered here apply the 5 flavour scheme (FS) with a vanishing bottom mass $m_b = 0$. 
The top quark is treated as massive and unstable throughout
with $m_t = 173.2 \, \text{GeV}$ as well as $\Gamma_t = 1.44262\, \text{GeV}$

%
The CKM matrix is set to unity except for the production of a single $W^\pm$ boson. 
In that case the PDG SM values are used 
\[
\begin{bmatrix}
V_{ud} & V_{us} & V_{ub }\\
V_{cd} & V_{cs} & V_{cb }\\
V_{td} & V_{ts} & V_{tb }
\end{bmatrix}
=
\begin{bmatrix}
  0.97417 & 0.2248 &  0.00409\\
  0.22 & 0.995 & 0.0405 \\
  0.0082 & 0.04 & 1.009
\end{bmatrix}
\]

\section{PDFs input and uncertainty}
\label{sec:pdf_error}
Cross section estimates for a series of different PDF sets are provided and listed in the following, as summarised in Table~\ref{tab:pdfs}. 
Each set includes an overview of the main feature and 
the prescription used to compute the corresponding PDF error on a given observable.

\paragraph{PDF4LHC21:} presented in Ref.~\cite{Ball:2022hsh}. 
Only NNLO grids are provided.
The Hessian set denoted as PDF4LHC21\_40 is employed, which 
renders the central value (members 0) and the 68\% probability intervals (members 1:40).
Denoting as $\mathcal{F}^{\left(k\right)}$ a physical observable computed using the $k$-th eigenvector direction,
the PDF uncertainty is given by 
\begin{align}
  \label{eq:symmhessian}
  \delta^{\text{PDF}}\mathcal{F} = 
  \sqrt{\sum_{k=1}^{N_{\text{eig}}}\left(\mathcal{F}^{\left(k\right)}- \mathcal{F}^{\left(0\right)}\right)^2}\,,
\end{align} 
which gives the $68\%$ CL uncertainty according to the symmetric Hessian prescription.

\paragraph{CT18:} presented in Ref.~\cite{Hou:2019efy}. NNLO grids are provided
for the central fit and 58 eigenvectors sets, corresponding to 29 eigenvectors in the `+' and -' directions
giving the $90\%$ CL interval. 
The $90\%$ CL PDF uncertainty is computed according to the asymmetric Hessian prescription
\begin{align}
  \label{eq:asymmhessian}
  \delta^{\text{PDF}}\mathcal{F} = 
  \frac{\sqrt{\sum_{k=1}^{N_{\text{eig}}}\left(\mathcal{F}^{\left(+\right)}- \mathcal{F}^{\left(-\right)}\right)^2}}{2}\,.
\end{align}
In the final results the $68\%$ CL is quoted, obtained applying a rescale factor to Eq.~\eqref{eq:asymmhessian}.\\
Furthermore, an alternative set (CT18A) which also includes the ATLAS precision 7\,TeV $W/Z$ data~\cite{STDM-2012-20} is considered as well.
 
\paragraph{MSHT20:} presented in Refs.~\cite{Cridge:2021qfd,Bailey:2020ooq,Cridge:2021pxm}.
Also in this case NNLO grids are provided for the central fit and 64 eigenvectors sets 
(corresponding to 32 eigenvectors in the '+' and '-' directions) giving the $68\%$ CL interval.
The PDF uncertainty is computed according to Eq.~\eqref{eq:asymmhessian}. 

\paragraph{NNPDF4.0:} presented in Ref.~\cite{Ball:2021leu}, represents the latest release of the
NNPDF family sets.
NNLO grids are provided, for the central fit and 100 MC replicas. 
To compute the best value of the physical observable $\mathcal{F}$ and the corresponding 
PDF uncertainty it is necessary to first compute the ensemble $\mathcal{S}$ of
the $\mathcal{F}$ values given the 100 replicas of the set:
\begin{align}
  \label{eq:replicas}
  \mathcal{S} = \{ \mathcal{F}^{(1)}, ..., \mathcal{F}^{(100)}\}\,.
\end{align}
Next, the elements of Eq.~\eqref{eq:replicas} are sorted in ascending order, while removing the lowest and the highest
16\% of the ordered replicas, keeping in this way the central 68\% replicas. Denoting the resulting 68\% CL 
interval as $\left[\mathcal{F}_{16}, \mathcal{F}_{84}\right]$ the up and down PDF error are estimated as
\begin{align}
  \delta^{\text{PDF,up}}\mathcal{F} = \mathcal{F}_{84} - \mathcal{F}_{0}\,, \,\,\,\,\,\,\,
  \delta^{\text{PDF,down}}\mathcal{F} = \mathcal{F}_{0} - \mathcal{F}_{16}\,.
\end{align}
The median of the ensemble $\mathcal{S}$ in Eq.~\eqref{eq:replicas} is quoted as the best value of the observable.

\paragraph{ABMP16:} presented in Ref.~\cite{Alekhin:2017kpj}. NNLO grids are provided for the 
central fit (member 0) and 29 eigenvectors (member 1 to 30). The $68\%$ CL interval can be obtained using 
the symmetric Hessian prescription of Eq.~\eqref{eq:symmhessian}. For NNLO results, the ABMP16als118\_5\_nnlo set is employed. 

\paragraph{ATLASpdf21} presented in Ref.~\cite{STDM-2020-32}. It represents the determination of a new set of PDFs  using diverse measurements in $pp$ collisions at $\sqrt{s}$ =7, 8 and 13~\TeV, together with deep inelastic scattering (DIS) data from $ep$ collisions at the HERA collider.
NNLO grids are provided for the central fit (member 0) and 42 eigenvectors (member 1 to 43). The experimental uncertainties for this specific set have been evaluated with an enhanced $\chi^{2}$ tolerance, $\Delta\chi^{2}=T^{2}$, with $T = 3$. The $68\%$ CL interval can be obtained using
the symmetric Hessian prescription of Eq.~\eqref{eq:symmhessian}. 

\begin{table}[!th]
    \centering
%    \footnotesize
    \begin{tabular}{lclcc}
      \toprule %\\
      & Number of members  & Perturbative orders & Error type & Reference \\ % \\    
      \toprule %\\
      PDF4LHC21            & 43   & NNLO & 68\% CL symmHessian  & \cite{Ball:2022hsh}     \\ %\\
      CT18, CT18A          & 59   & NNLO & 68\% CL Hessian & \cite{Hou:2019efy}    \\% \\
      MSHT20               & 65   & NNLO & 68\% CL Hessian & \cite{Cridge:2021qfd,Bailey:2020ooq,Cridge:2021pxm}   \\% \\
      NNPDF4.0             & 101  & NNLO & Montecarlo  & \cite{Ball:2021leu}  \\% \\
      ABMP16               & 30   & NNLO & 68\% CL symmHessian & \cite{Alekhin:2017kpj}     \\ %\\
      ATLASpdf21           & 43   & NNLO & 68\% CL symmHessian & \cite{STDM-2020-32}     \\ %\\
      \hline% \\
    \end{tabular}
    \caption{Summary of the PDF sets used in the computation of theoretical predictions for the fiducial cross sections.}
    \label{tab:pdfs} 
%    \vspace{0.3cm}
  \end{table}


%-------------------------------------------------------------------------------
\section{Results}
In Table~\ref{tab:NNLO} NNLO QCD + NLO EW results are reported.
Results are presented for each PDF set listed in the previous section, and are supplemented with 
PDF and scale variations error. The fiducial cuts which have been used to produce results are reported 
in Table~\ref{tab:fiducial_cuts_single_boson}.\\
% Inclusive cross sections have been computed as well, and they are reported in Table~\ref{tab:NNLO_Inc}. 
The only requirement of 66~$< m_{\ell\ell} <$~116~GeV has been adopted.

In order to perform NNLO computations a cut-off $r_{\text{cut}}$ on the quantity $q_T/M$ is used,
as described in details in Sections~2.1 and 2.2 of Ref.~\cite{Grazzini:2017mhc}.
Depending on the process, the result might show a more or less marked dependence on the choice of such cut-off,
and an additional extrapolation procedure is required to get the final NNLO result.
The cut-off is therefore sent to 0, and the resulting extrapolated result, denoted as $\sigma^{\text{extr}}$,
is taken as the best value of the computation.
In the case of the process $pp\rightarrow e^+e^-$, for which the $r_{\text{cut}}$ dependence is reported to be
especially strong, the \texttt{MATRIX} runs have been performed using a dedicated option available in the code,
which uses smaller values of the cut-off.
The details regarding the extrapolation procedure and the $r_{\text{cut}}$ dependence of the different results
are described in details in Section7 of Ref.~\cite{Grazzini:2017mhc}.  

\begin{table}[!th]
  \centering
%  \footnotesize
  \begin{tabular}{cc}
    \toprule %\\
       & $pp \rightarrow \ell^+ \ell^-$ \\
   \toprule% \\
     Lepton $p_{\mathrm{T}}$ cuts &  $p_{\mathrm{T}} >$ 27~\GeV \\%  \\
     \hline %\\
     Lepton $\eta$ cuts &   $|\eta| <$ 2.5 \\
     \hline %\\
     Mass cuts &  66  $< m_{\ell\ell} <$ 116~\GeV \\
      \hline
   \end{tabular}
   \caption{Fiducial cuts for single boson production processes.}
   \label{tab:fiducial_cuts_single_boson} 
%  \vspace{0.3cm}

\end{table}

%\clearpage
%\newpage


\begin{table}[!th]
  \centering
%  \footnotesize
  \begin{tabular}{cc} 
    \toprule 
    PDF set & $pp \rightarrow \ell^{+} \ell^{-}$ [pb] \\
    \hline %\\ 
    CT18 & 733.22$^{+0.5\%, +0.5\%, +5.9\%}_{-0.5\%, -0.5\%, -6.2\%}$ \\
    CT18A & 750.47$^{+0.4\%, +0.5\%, +3.7\%}_{-0.4\%, -0.5\%, -4.3\%}$ \\
    MSHT20 & 747.48$^{+0.6\%, +0.5\%, +2.2\%}_{-0.6\%, -0.7\%, -2.7\%}$ \\
    NNPDF4.0 & 767.68$^{+0.5\%, +0.3\%, +0.9\%}_{-0.5\%, -0.5\%, -0.9\%}$ \\
    PDF4LHC21 & 746.04$^{+0.6\%, +0.4\%, +4.6\%}_{-0.6\%, -0.6\%, -4.6\%}$ \\
    ATLASpdf21 & 787.39$^{+0.4\%, +0.4\%, +3.2\%}_{-0.4\%, -0.6\%, -4.1\%}$ \\
    ABMP16 & 746.12$^{+0.5\%, +0.6\%, +1.5\%}_{-0.5\%, -0.6\%, -1.5\%}$ \\
    \hline 
  \end{tabular}
  \caption{Single boson production fiducial cross-section results for a series of different PDF sets. 
  The first error which is quoted correspond to statistical uncertainty, the second to scale error, while the third to PDF error, estimated as 
  described in Section~\ref{sec:pdf_error}.}
  \label{tab:NNLO} 
\end{table}

% \begin{table}[!th]
%   \centering
% %  \footnotesize                                                                                                                                                
%   \begin{tabular}{cc} 
%     \toprule 
%     PDF set & $pp \rightarrow \ell^{+} \ell^{-}$ [nb] \\
%     \hline %\\
%     CT18 & 1.984$^{+0.04\%, +0.5\%, +5.9\%}_{-0.04\%, -0.5\%, -6.2\%}$ \\
%     CT18A & 2.010$^{+0.04\%, +0.5\%, +3.7\%}_{-0.04\%, -0.5\%, -4.3\%}$ \\
%     MSHT20 & 1.999$^{+0.04\%, +0.5\%, +2.2\%}_{-0.04\%, -0.7\%, -2.7\%}$ \\
%     NNPDF4.0 & 2.034$^{+0.04\%, +0.3\%, +0.9\%}_{-0.04\%, -0.5\%, +0.9\%}$ \\
%     PDF4LHC21 & 2.000$^{+0.04\%, +0.4\%, +4.6\%}_{-0.04\%, -0.6\%, -4.6\%}$ \\
%     ATLASpdf21 & 2.098$^{+0.04\%, +0.4\%, +3.2\%}_{-0.04\%, -0.6\%, -4.1\%}$ \\
%     ABMP16 & 1.998$^{+0.04\%, +0.6\%, +1.5\%}_{-0.04\%, -0.6\%, -1.5\%}$ \\
%     \hline 
%   \end{tabular}
%   \caption{Single boson production inclusive results for a series of different PDF sets. The first error which is quoted correspond to statistical uncertainty, the second to scale error, while the third to PDF error, estimated as described in Section~\ref{sec:pdf_error}.}
%   \label{tab:NNLO_Inc}
% \end{table}

\section{Estimate of $\alpha_s$ uncertainty}
The following paragraph reports an estimate for the $\alpha_s$ uncertainty, using as input the PDF4LHC21 set.
As detailed in Ref.~\cite{Ball:2022hsh}, the PDF4LHC21 combination assumes
\begin{align}
  \alpha_s(m_Z^2) = 0.1180 \pm 0.0010\,.
\end{align}
Following the prescription given in Ref.~\cite{Ball:2022hsh}, for a given cross section $\sigma$
the $\alpha_s$ uncertainty is estimated as 
\begin{align}
  \label{eq:alpha_error}
  \delta^{\alpha_s}\sigma = \frac{ \sigma\left(\alpha_s=0.119\right) - \sigma\left(\alpha_s=0.117\right) }{2}
\end{align} 
corresponding to an uncertainty $\delta\alpha_s=0.0010$ at the 68\% CL.
In order to compute Eq.~\eqref{eq:alpha_error} Hessian variants which include the central
fits for variations of the strong coupling constant $\alpha_s(m_Z^2)$ are provided.
Results for each process considered here are reported in Table~\ref{tab:alpha_err}.
\begin{table}[!th]
  \centering
%  \footnotesize
  \begin{tabular}{cccc}
    \toprule
     & \multicolumn{3}{c}{PDF4LHC21}  \\
     \cline{2-4}
          & $\sigma\left(\alpha_s=0.117\right)$ [pb] & $\sigma\left(\alpha_s=0.119\right)$ [pb] &  $\delta^{\alpha_s}\sigma$  \\      
     \hline% \\
     $pp \rightarrow \ell^+ \ell^-$ & 740.93 & 751.40 & 0.7\%  \\
    \hline
   \end{tabular}
   \caption{Fiducial cross sections predictions for $\alpha_s$ variations obtained using PDF4LHC21.}
   \label{tab:alpha_err} 
%  \vspace{0.3cm}
\end{table}
