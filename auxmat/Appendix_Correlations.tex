% \setlength{\footskip}{5pt}

\chapter{Nuisance parameters: correlation matrices, pulls, and constraints}
\label{app:correlations_29}
This appendix reports the correlation matrices, the pull plots and the ranking of the nuisance parameters for the systematic uncertainties included in the fit.
Figures~\ref{fig:ttbar_Ranking},~\ref{fig:Z_Ranking}, and~\ref{fig:zratio_Ranking} shows the impact of most important systematic uncertainties on the cross-section ratio.
Correlations for the \ttbar, $Z$ and ratio measurements are displayed respectively in Figures~\ref{fig:ttbar_corrmatrix}, \ref{fig:Z_corrmatrix}, and \ref{fig:ratio_corrmatrix}.
The correlations are extracted using the method described in Section~\ref{sec:profile-likelihood}.

On the other hand, pulls and constraints of the NPs are shown in Figures~\ref{fig:nuis_ttbar}, \ref{fig:nuis_Z}, and \ref{fig:nuis_ratio}.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/chapter_10/ttbar/Ranking_mu_ttbar.pdf}
  \caption{Ranking of the 10 most important NPs in the simultaneous fit of \ttbar and $Z$-boson cross sections to data. 
  The impact of each NP on the measured \ttbar cross section, $\Delta \sigma_{\ttbar}$, is defined as the shift induced in $\sigma_{\ttbar}$ as the NP is shifted by its pre-fit and post-fit 
  uncertainties $\pm \Delta \theta$($\pm \Delta \hat{\theta}$) from its nominal best-fit value, $\hat{\theta}$, and then fixed while all other NPs profiled. 
  The empty boxes show the pre-fit impact while the filled boxes show the post-fit impact of each nuisance parameter on the result. 
  The black dots represent the post-fit value (pull) of each NP where the pre-fit value is subtracted, while the black line represents the post-fit uncertainty normalised to the pre-fit uncertainty.}  
  \label{fig:ttbar_Ranking}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/chapter_10/ttbar/Ranking_mu_Z.pdf}
  \caption{Ranking plot showing the effect of the 10 most important systematic uncertainties in the fit to $ee$ and $\mu\mu$ data. 
  The impact of each NP on the measured fiducial $Z$-boson cross section, $\Delta \sigma_Z$, is defined as the shift induced in $\sigma_Z$ as the NP is shifted by its pre-fit and post-fit 
  uncertainties $\pm \Delta \theta$($\pm \Delta \hat{\theta}$) from its nominal best-fit value, $\hat{\theta}$, and then fixed while all other NPs profiled. 
  The empty boxes show the pre-fit impact while the filled boxes show the post-fit impact of each nuisance parameter on the result. 
  The black dots represent the post-fit value (pull) of each NP where the pre-fit value is subtracted, while the black line represents the post-fit uncertainty normalised to the pre-fit uncertainty.}
  \label{fig:Z_Ranking}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/chapter_10/zratio/Ranking_mu_ratio.pdf}
  \caption{Ranking plot showing the effect of the 10 most important systematic uncertainties on the measured ratio of the \ttbar cross-section over $Z$ cross-section, $R$, in the fit to data. 
  The impact of each NP, $\Delta R/R_{\text{pred.}}$, is computed by comparing the nominal best-fit value of $R/R_{\text{pred.}}$ with the result of the fit when fixing 
  the considered nuisance parameter to its best-fit value, ($\theta$), shifted by its pre-fit and post-fit uncertainties $\pm \Delta \theta(\pm \Delta \hat{\theta})$. 
  The empty boxes show the pre-fit impact while the filled boxes show the post-fit impact of each nuisance parameter on the result. 
  The black dots represent the post-fit value (pull) of each NP where the pre-fit value is subtracted, while the black line represents the post-fit uncertainty normalised to the pre-fit uncertainty.}  
  \label{fig:zratio_Ranking}
\end{figure}

\begin{figure}[!hbt]
  \centering
  \includegraphics[width=0.65\textwidth,trim={0 0 0 0},clip]{figures/chapter_10/ttbar/CorrMatrix.pdf}
  \caption{Correlations of nuisance parameters in the fit to the observed data for the \ttbar fit. 
  Only entries with correlation larger than 20\% are shown.}
  \label{fig:ttbar_corrmatrix}
\end{figure}

\begin{figure}[!hbt]
  \centering
  \begin{minipage}[t]{0.5\textwidth}
  \vspace{0pt}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/ttbar/NuisPar_JES.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Jets};
  \end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/ttbar/NuisPar_Lumi.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Luminosity};
  \end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.1cm},clip,width=1\textwidth]{figures/chapter_10/ttbar/NuisPar_Electron.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Electrons};
  \end{tikzpicture}
  \end{minipage}\hfill
  \begin{minipage}[t]{0.5\textwidth}
  \vspace{0pt}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/ttbar/NuisPar_b-tagging.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {$b-$tagging};
  \end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/ttbar/NuisPar_Theory.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Theory};
  \end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/ttbar/NuisPar_PDF.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {PDF};
  \end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/ttbar/NuisPar_Instrumental.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Instrumental};
  \end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/ttbar/NuisPar_trigger.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Trigger};
  \end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/ttbar/NuisPar_Muon.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Muons};
  \end{tikzpicture}
  \end{minipage}
  \caption{Fitted nuisance parameters after a fit to the observed data for the \ttbar fit. NPs are arranged in different groups.}
  \label{fig:nuis_ttbar}
  \end{figure}
  



\begin{figure}[!hbt]
  \centering
  \includegraphics[width=0.65\textwidth,trim={0 0 0 0},clip]{figures/chapter_10/Z/CorrMatrix.pdf}
  \caption{Correlations of nuisance parameters in the fit to the observed data for the $Z$ fit. 
  Only entries with correlation larger than 20\% are shown.}
  \label{fig:Z_corrmatrix}
\end{figure}

\begin{figure}[!hbt]
  \centering
  \begin{minipage}[t]{0.5\textwidth}
  \vspace{0pt}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/Z/NuisPar_JES.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Jets};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/Z/NuisPar_Lumi.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Luminosity};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/Z/NuisPar_Electron.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Electrons};
\end{tikzpicture}
  \end{minipage}\hfill
  \begin{minipage}[t]{0.5\textwidth}
  \vspace{0pt}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/Z/NuisPar_b-tagging.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {$b-$tagging};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/Z/NuisPar_Theory.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Theory};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/Z/NuisPar_PDF.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {PDF};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/Z/NuisPar_Instrumental.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Instrumental};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/Z/NuisPar_trigger.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Trigger};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/Z/NuisPar_Muon.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Muons};
\end{tikzpicture}
  \end{minipage}
  \caption{Fitted nuisance parameters after a fit to the observed data for the Z fit. NPs are arranged in different groups.}
  \label{fig:nuis_Z}
  \end{figure}




\begin{figure}[!hbt]
  \centering
  \includegraphics[width=0.65\textwidth,trim={0 0 0 0},clip]{figures/chapter_10/zratio/CorrMatrix.pdf}
  \caption{Correlations of nuisance parameters in the fit to the observed data for the ratio fit. 
  Only entries with correlation larger than 20\% are shown.}
  \label{fig:ratio_corrmatrix}
\end{figure}

\begin{figure}[!hbt]
  \centering
  \begin{minipage}[t]{0.5\textwidth}
  \vspace{0pt}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/zratio/NuisPar_JES.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Jets};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/zratio/NuisPar_Lumi.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Luminosity};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.1cm},clip,width=1\textwidth]{figures/chapter_10/zratio/NuisPar_Electron.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Electrons};
\end{tikzpicture}
  \end{minipage}\hfill
  \begin{minipage}[t]{0.5\textwidth}
  \vspace{0pt}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/zratio/NuisPar_b-tagging.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {$b-$tagging};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/zratio/NuisPar_Theory.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Theory};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/zratio/NuisPar_PDF.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {PDF};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/zratio/NuisPar_Instrumental.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Instrumental};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/zratio/NuisPar_trigger.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Trigger};
\end{tikzpicture}
  \vspace*{-0.12cm}
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[trim={0 0 0 1.2cm},clip,width=1\textwidth]{figures/chapter_10/zratio/NuisPar_Muon.pdf}};
    \node[anchor=north west, font=\sffamily\scriptsize, yshift=0.4cm, xshift=0.1cm] at (image.north west) {Muons};
\end{tikzpicture}
  \end{minipage}
  \caption{Fitted nuisance parameters after a fit to the observed data for the ratio fit. NPs are arranged in different groups.}
  \label{fig:nuis_ratio}
  \end{figure}

\clearpage
% \setlength{\footskip}{24pt}