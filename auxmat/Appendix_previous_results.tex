\chapter{Results from previous measurements}
\label{app:PreviousResults}

\section{ATLAS-CONF-2022-070 - 1.2 \ifb}
\label{sec:ATLAS-CONF-2022-070}

% The measurement is compatible within the uncertainty with SM prediction.
Figure~\ref{fig:070_PrePost} shows the distribution used in the fit to extract the cross-sections and the ratio, before and after performing the fit.
The expected values for the \ttbar and $Z$-boson production cross-section, for the fiducial phase space of the $Z$-boson defined by $m_{\ell\ell} > 40~\gev$ and the ratio of the cross-sections are 

\begin{align*}
  R_{t\bar{t}/Z}^{\text{SM}} &= 0.423 \pm 0.015 \text{(scale+PDF)}, \\
    \sigma_{\ttbar}^{\text{SM}} &=924 ^{+32}_{-40} \text{(scale+PDF)}~\text{pb}, \\
    \sigma_{Z\to\ell\ell}^{m_{\ell\ell > 40}, \text{SM}} &= 2182^{+42}_{-45} \text{(scale+PDF)}~\text{pb}.
\end{align*}

Furthermore, the $\epsilon_b$ value in the \ttbar\ simulation is $0.5529\pm 0.0002$(MC stat.).
The fitted values are:

\begin{align*}
  \ratio &=0.400 \pm 0.006\mathrm{(stat.)}\pm 0.017\mathrm{(syst.)}\pm 0.005\mathrm{(lumi.)}, \\
  \xtt &=830 \pm 12\mathrm{(stat.)}\pm 27\mathrm{(syst.)}\pm 86\mathrm{(lumi.) pb}, \\
  \xzold &= 2075 \pm 2\mathrm{(stat.)}\pm 98\mathrm{(syst.)}\pm 199\mathrm{(lumi.) pb}, \\ 
  \epsilon_b &= 0.553 \pm 0.007 \mathrm{(stat.)} \pm 0.005 \mathrm{(syst.)} \pm 0.001 \mathrm{(lumi.)}.
\end{align*}
The \ttbar\ cross-section is measured by repeating the fit in the $e\mu$ channel, see Section~\ref{sec:fit-setups}. 

Even with the relatively low integrated luminosity, the measurement of the \ttbar cross-sections is dominated by systematic uncertainties.
The dominant source of systematic uncertainties originates from luminosity.
The uncertainty on the efficiency of the $b$-tagging simulation is caused by a large impact on the $tW$ single-top contribution that is not absorbed by $\epsilon_b$ as in the case of the \ttbar production.
Other important sources of the systematic uncertainties include: lepton identification, and parton-shower and hadronisation model.

Also, the $Z$ cross section measurement is dominated by the luminosity uncertainty, followed by the matrix element and parton shower uncertainty on $Z$+jets samples, and by the electron and muon reconstruction uncertainties.

As for the ratio, Figure~\ref{fig:Ranking_070} shows the impact of most important systematic uncertainties on the cross-section, and Table~\ref{tab:070_zratio_GroupedImpact} shows the impact of the systematic uncertainties grouped by their origin.
The dominant source of systematic uncertainties originates from the simultaneous variation of matrix element and parton shower uncertainty in the $Z$+jets samples.
The uncertainty on the luminosity has a large impact due to its effect on the backgrounds that do not have an unconstrained normalisation parameter in the fit to compensate for it.
Other important sources of the systematic uncertainties include: parton-shower and hadronisation model, 
and the uncertainty in the simulation of the response of the muon trigger efficiency.

Several checks have been performed to validate the obtained results.
Firstly, the ratio of $Z\to \mu\mu$ over $Z \to ee$ has been measured to be $R_{Z\to \mu\mu/Z\to ee} = 0.99 \pm 0.05$.
This ratio is consistent with one within the uncertainties, validating the estimation of the lepton uncertainties. 
Secondly, the fit has been performed for each of the four LHC fills separately, to validate the data quality requirements.
The ratio values obtained for different runs are consistent with each other within the measured uncertainties.
Thirdly, to assess the possible impact of the top-quark~$\pt$ modelling on the acceptance, the fit has been repeated with an increased requirement on the lepton~\pt of 30 and 35~\gev.
The impact on \ratio\ due to the tighter lepton \pt\ requirement is found to be up to 2\%.
Finally, to assess the impact coming from same-flavour regions, the fit has been repeated excluding the $ee$ and $\mu\mu$ regions.

Figure~\ref{fig:070_zratio_xSec_ratio_vs_Theory} reports the fitted values of the \ttbar over $Z$-boson production cross-section ratio,
the results of the fits in the individual LHC fills, and the fits with only one flavour channel for the $Z$-boson cross-section.
For the last two cross-checks, only the statistical uncertainty is shown.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.49\textwidth]{figures/chapter_10/ATLAS-CONF-2022-070/Summary.pdf}
  \includegraphics[width=0.49\textwidth]{figures/chapter_10/ATLAS-CONF-2022-070/Summary_postFit.pdf}
  \caption{Comparison of data and prediction for the event yields in the three lepton channels before the fit (left) and after the fit (right). 
  The $e\mu$ channel is split into events with one or two $b$-tagged jets. 
  The bottom panel shows the ratio of data over the prediction. 
  The hashed bands represent the total uncertainty. 
  Correlations of the NPs as obtained from the fit are used to build the uncertainty band in the post-fit distribution.}
  \label{fig:070_PrePost}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/chapter_10/ATLAS-CONF-2022-070/Ranking_mu_ratio.pdf}
  \caption{Ranking plot showing the effect of the 20 most important systematic uncertainties on the measured \ttbar cross-section, $\mu$, in the fit to data. The impact of each NP, $\Delta \mu$, is computed by comparing the nominal best-fit value of $\mu/\mu_{\text{pred.}}$ with the result of the fit when fixing the considered nuisance parameter to its best-fit value, ($\theta$), shifted by its pre-fit and post-fit uncertainties $\pm \Delta \theta(\pm \Delta \hat{\theta})$. The empty boxes show the pre-fit impact while the filled boxes show the post-fit impact of each nuisance parameter on the result. The black dots represent the post-fit value (pull) of each NP where the pre-fit value is subtracted, while the black line represents the post-fit uncertainty normalised to the pre-fit uncertainty.}  
  \label{fig:Ranking_070}
\end{figure}

\begin{table}[!h]
  \centering
\begin{tabular}{llccc}
\toprule
  & \textbf{Category} & \multicolumn{3}{c}{\textbf{Uncert.} [\%]}       \\
  \toprule
  &  &  $\sigma_{t\bar{t}}$  & $\sigma_{Z \to\ell\ell}^{m_{\ell\ell}>40}$ & $R_{t\bar{t}/Z}$ \\
\midrule  
$t\bar{t}$    & $t\bar{t}$ parton shower/hadronisation    &0.6  &0.2 &0.7 \\
            & $t\bar{t}$ scale variations                 &0.5  &\(<0.2\) &0.5 \\
  $Z$       & $Z$ scale variations                        &0.2  &2.9 &2.9 \\
Bkg.        & Single top modelling                        &0.6  &\(<0.2\) &0.6 \\
            & Diboson modelling                           &\(<0.2\)  &\(<0.2\) &0.5 \\
            & Mis-Id leptons                              &0.6  &\(<0.2\) &0.6 \\
Lept.       & Electron reconstruction                     &1.6  &2.3 &1.1 \\
            & Muon reconstruction                         &1.3  &2.4 &0.3 \\
            & Lepton trigger                              &0.2  &1.3 &1.1 \\
Jets/tagging & Jet reconstruction                         &0.2  &- &0.2 \\
            & Flavour tagging                             &1.9  &- &1.9 \\
\midrule
            & Pileup                                      & 0.5 & 0.6 & \(<0.2\) \\
            & PDFs                                        &0.5  &1.4 &1.3 \\
            & Luminosity                                  &10.3  &9.6 &1.3 \\
\bottomrule
            & Systematic Uncertainty                      &10.8  &10.7 &4.4 \\
            & Statistical Uncertainty                     &1.5  &0.1 &1.5 \\
\midrule
            & Total Uncertainty                           &11  &10.7 &4.7 \\
\bottomrule
\bottomrule
\end{tabular}
\caption{Observed impact of the different sources of uncertainty on the measured \ttbar and $Z$-boson cross-section and on the ratio \ratio,
  grouped by categories. The impact of each category is obtained by repeating the fit after having fixed the set of
nuisance parameters corresponding to that category, subtracting the square of the resulting uncertainty from the
square of the uncertainty found in the full fit, and calculating the square root. The statistical uncertainty is obtained
by repeating the fit after having fixed all nuisance parameters to their fitted values. Only the acceptance effects are considered for the PDF and the modelling uncertainties.}
\label{tab:070_zratio_GroupedImpact}
\end{table}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=1.\textwidth]{figures/chapter_10/ATLAS-CONF-2022-070/Xsec_ratio_vs_Theory.pdf}
  \caption{Fitted values and uncertainties of the \ttbar/$Z$ cross-section ratio \ratio.
  The three shaded bands correspond to the statistical uncertainty, statistical and experimental systematic
  uncertainties added in quadrature, and total uncertainty, including luminosity uncertainty.
  The fitted values and statistical uncertainties for the fits in the individual LHC fills or from fits with only one flavour channel for the $Z$-boson cross-section (triangular marker pointing up) are marked with a dashed error bar. 
  The Standard Model prediction based on the NNLO+NNLL calculation for \ttbar and NNLO(QCD)+NLO EW prediction for $Z$+jets production, using the PDF4LHC21 parton distribution function, is indicated with a square marker. 
  The corresponding total (scale+PDF) uncertainty is shown as a dotted error bar.}
  \label{fig:070_zratio_xSec_ratio_vs_Theory}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\afterpage{\clearpage}
\newpage
\section{ATLAS-CONF-2023-006 - 11.3 \ifb}
\label{sec:ATLAS-CONF-2023-006}

Figure~\ref{fig:006_PrePost} shows the distribution used in the fit to extract the cross-sections and the ratio, before and after performing the fit.
The expected values for the \ttbar and $Z$-boson production cross-section, for the fiducial phase space of the $Z$-boson and the ratio of the cross-sections are 

\begin{align*}
  R_{t\bar{t}/Z}^{\text{SM}} &= 1.2453 \pm 0.076 \mathrm{(scale+PDF, PDF4LHC21)} , \\
  \sigma_{\ttbar}^{\text{SM}} &= 924^{+32}_{-40} \mathrm{(scale+PDF) pb}, \\
  \sigma_{Z}^{\text{SM}} &= 741 \pm 15\mathrm{(scale+PDF) pb}.
\end{align*}
The $\epsilon_b$ value in the \ttbar\ simulation is $0.5451\pm 0.0004 \mathrm{(MC stat.)}$

The fitted values are:

\begin{align*}
  R_{t\bar{t}/Z} &=1.144 \pm 0.006\mathrm{(stat.)}\pm 0.022\mathrm{(syst.)}\pm 0.003\mathrm{(lumi.)}, \\
  \sigma_{\ttbar} &=859 \pm 4\mathrm{(stat.)}\pm 22 \mathrm{(syst.)}\pm 19 \mathrm{(lumi.) pb}, \\
  \xz &= 751.2 \pm 0.3\mathrm{(stat.)}\pm 15 \mathrm{(syst.)}\pm 17\mathrm{(lumi.) pb}, \\  
  \epsilon_b &= 0.548\pm 0.002\mathrm{(stat.)}\pm 0.004 \mathrm{(syst.)}\pm 0.001\mathrm{(lumi.) pb}.
\end{align*}

The measured $\epsilon_b$ is compatible within the per mille uncertainty with the simulated one.
To estimate the impact of each systematic uncertainty on the uncertainty of the signal strength, a ranking plot is shown in Figure~\ref{fig:006_ttbar_Ranking}, Figure~\ref{fig:006_Z_Ranking} and Figure~\ref{fig:006_zratio_Ranking}.
Table~\ref{tab:006_zratio_GroupedImpact} shows the impact of the systematic uncertainties grouped by their origin.

In both the individual cross section measurements, the largest source of systematic uncertainty originates from the luminosity estimation, immediatly followed by leptons reconstruction.
In the \ttbar case, the uncertainty in the parton shower and hadronisation modelling has a significant impact on the precision as well.
The dominant uncertainty in the ratio stems from the \ttbar modelling, followed by trigger and lepton reconstruction efficiencies.

Figure \ref{fig:006_results1} shows the measured $\ttbar$ cross-section as a function of the
centre-of-mass energy compared to the theory prediction using the PDF4LHC21 PDF set.
This new measurement is slightly lower than the predictions but compatible at 1.3 standard deviations, assuming uncorrelated uncertainties between the prediction and measurement.
In Figure~\ref{fig:006_results2} the ratio of the $\ttbar$ and $Z$-boson production cross-sections is compared to
the prediction for several sets of PDF. The ratio is slightly lower than most predictions where the level of compatibility varies between the PDF sets.

A number of checks have been performed to validate the obtained results.
Firstly, the ratio of $Z\to \mu\mu$ over $Z \to ee$ has been measured to be $1.003\pm0.034$, in agreement with unity.
Then, in order to study the impact of isolation uncertainties, these were decorrelated for \ttbar and $Z$ samples; such treatment of the uncertainties does not impact the result.
To estimate the impact of fiducial corrections in $Z$-boson events, decorrelated uncertainties were assigned to out-of-fiducial events, and did not undergo the fiducial correction. 
The result is found to be not sensitive to any of these effects
Finally, the simulated distribution of the \pt\ of the dilepton pair has been reweighted to match the data perfectly in the $ee$ and $\mu\mu$ events, assuming only $Z$-boson events contributing.
After the reweighting, the impact on the total predicted yields has been checked.
This impact was found to be below 0.15\%, thus negligible.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.49\textwidth]{figures/chapter_10/ATLAS-CONF-2023-006/Summary.pdf}
  \includegraphics[width=0.49\textwidth]{figures/chapter_10/ATLAS-CONF-2023-006/Summary_postFit.pdf}
  \caption{Comparison of data and prediction for the event yields in the three lepton channels before the fit (left) and after the fit (right). The $e\mu$ channel is split into events with one or two $b$-tagged jets. The bottom panel shows the ratio of data over the prediction. The hashed bands represent the total uncertainty. Correlations of the NPs as obtained from the fit are used to build the uncertainty bad in the post-fit distribution.}  
  \label{fig:006_PrePost}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/chapter_10/ATLAS-CONF-2023-006/Ranking_mu_ttbar.pdf}
  \caption{Ranking plot showing the effect of the 10 most important systematic uncertainties on the measured \ttbar cross-section, $\mu$, in the fit to data. 
  The impact of each NP, $\Delta \mu$, is computed by comparing the nominal best-fit value of $\mu/\mu_{\text{pred.}}$ with the result of the fit when fixing the considered nuisance parameter to its best-fit value, ($\theta$), shifted by its pre-fit and post-fit uncertainties $\pm \Delta \theta(\pm \Delta \hat{\theta})$. 
  The empty boxes show the pre-fit impact while the filled boxes show the post-fit impact of each nuisance parameter on the result. 
  The black dots represent the post-fit value (pull) of each NP where the pre-fit value is subtracted, while the black line represents the post-fit uncertainty normalised to the pre-fit uncertainty.}  
  \label{fig:006_ttbar_Ranking}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/chapter_10/ATLAS-CONF-2023-006/Ranking_mu_Z.pdf}
  \caption{Ranking plot showing the effect of the 10 most important systematic uncertainties on the measured $Z$ cross-section, $\mu$, in the fit to data. 
  The impact of each NP, $\Delta \mu$, is computed by comparing the nominal best-fit value of $\mu/\mu_{\text{pred.}}$ with the result of the fit when fixing the considered nuisance parameter to its best-fit value, ($\theta$), shifted by its pre-fit and post-fit uncertainties $\pm \Delta \theta(\pm \Delta \hat{\theta})$. 
  The empty boxes show the pre-fit impact while the filled boxes show the post-fit impact of each nuisance parameter on the result. 
  The black dots represent the post-fit value (pull) of each NP where the pre-fit value is subtracted, while the black line represents the post-fit uncertainty normalised to the pre-fit uncertainty.}  
  \label{fig:006_Z_Ranking}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/chapter_10/ATLAS-CONF-2023-006/Ranking_mu_ratio.pdf}
  \caption{Ranking plot showing the effect of the 10 most important systematic uncertainties on the measured ratio of the \ttbar cross-section over $Z$ cross-section, $R$, in the fit to data. The impact of each NP, $\Delta R/R_{\text{pred.}}$, is computed by comparing the nominal best-fit value of $R/R_{\text{pred.}}$ with the result of the fit when fixing the considered nuisance parameter to its best-fit value, ($\theta$), shifted by its pre-fit and post-fit uncertainties $\pm \Delta \theta(\pm \Delta \hat{\theta})$. The empty boxes show the pre-fit impact while the filled boxes show the post-fit impact of each nuisance parameter on the result. The black dots represent the post-fit value (pull) of each NP where the pre-fit value is subtracted, while the black line represents the post-fit uncertainty normalised to the pre-fit uncertainty.}  
  \label{fig:006_zratio_Ranking}
\end{figure}

\begin{table}[!hbt]
  \centering
\begin{tabular}{llccc}
\toprule
  & \textbf{Category} & \multicolumn{3}{c}{\textbf{Uncert.} [\%]}       \\
  \toprule
  &  &  $\sigma_{t\bar{t}}$  & $\sigma_{Z \to\ell\ell}^{\text{fid.}}$ & $R_{t\bar{t}/Z}$ \\
\midrule  
$t\bar{t}$    & $t\bar{t}$ parton shower/hadronisation    & 1.1  &\(<0.2\) & 1.0 \\
            & $t\bar{t}$ scale variations                 & 0.2  & \(<0.2\) & 0.2 \\
            & Top quark \pt reweighting                   & 0.6  &\(<0.2\) & 0.5 \\
  $Z$       & $Z$ scale variations                        & 0.2  & 0.5 & 0.3 \\
Bkg.        & Single top modelling                        & 0.4  &\(<0.2\) & 0.4 \\
            & Diboson modelling                           &\(<0.2\)  &\(<0.2\) & \(<0.2\) \\
            & Mis-Id leptons                              & 0.5  &\(<0.2\) & 0.5 \\
Lept.       & Electron reconstruction                     & 1.0  & 1.1 & 0.5 \\
            & Muon reconstruction                         & 1.5  & 1.2 & 0.8 \\
            & Lepton trigger                              & 0.4  & 0.7 & 0.8 \\
Jets/tagging & Jet reconstruction                         & 0.4  &- & 0.3 \\
            & Flavour tagging                             & 0.2  &- & 0.2 \\
\midrule
            & PDFs                                        & 0.4  & 0.2 & 0.4 \\
            & Pileup                                      & 1.1  & 1.1 & \(<0.2\) \\
            & Luminosity                                  & 2.3  & 2.2 & 0.3 \\
\bottomrule
            & Systematic Uncertainty                      & 3.5  & 3.0 & 2.0 \\
            & Statistical Uncertainty                     & 0.5  & 0.03 & 0.5 \\
\midrule
            & Total Uncertainty                           & 3.5  & 3.0 & 2.0 \\
\bottomrule
\bottomrule
\end{tabular}
\caption{Observed impact of the different sources of uncertainty on the measured \ttbar and $Z$-boson cross-section and on the ratio \ratio,
  grouped by categories. The impact of each category is obtained by repeating the fit after having fixed the set of
nuisance parameters corresponding to that category, subtracting the square of the resulting uncertainty from the
square of the uncertainty found in the full fit, and calculating the square root. The statistical uncertainty is obtained
by repeating the fit after having fixed all nuisance parameters to their fitted values. Only the acceptance effects are considered for the PDF and the modelling uncertainties.}
\label{tab:006_zratio_GroupedImpact}
\end{table}

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/chapter_10/ATLAS-CONF-2023-006/xsec_ecm}
  \caption{Comparison of the measures $\ttbar$ cross-section compared to theory predictions using the PDF4LHC21 PDF set.
  The measurements shown include $e\mu$ final state ($e\mu$ + $b$-tagged jets); $ee$, $\mu\mu$ and $e\mu$ final states ($ll$ + $b$-tagged jets); 
  single lepton final states ($l$ + jets); as well as combinations of final states (combined).
  The bottom panel shows the ratio of the measured values and three predictions that either contain only the uncertainties originating from the QCD scale variations (black), 
  only the variations in the PDF uncertainties (red) or the total uncertainty in the prediction (blue).}
  \label{fig:006_results1}
\end{figure}

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/chapter_10/ATLAS-CONF-2023-006/Xsec_ratio_vs_Theory}
  \caption{Ratio of the $\ttbar$ to the $Z$-boson cross-section
    compared to the prediction for several sets of parton distribution
    functions. For the PDF4LH21 PDF set, predictions for different assumptions about the top-quark mass are also displayed.}
  \label{fig:006_results2}
\end{figure}