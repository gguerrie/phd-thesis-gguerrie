"""
The texparser module includes a lit of utilities to scan and parse LaTeX documents extracting environments and understanding its content.
"""
import re
import os
import glob
import string
import warnings
import codecs

from TexSoup import TexSoup

from pogitlab import utils
from pogitlab.formatter import Formatter
from pogitlab.entities.buffer import Buffer

TOKENS = {
    "Escape": '\\',
    'Comment': '%',
    'EndOfLine': ('\n', '\r'),
    "BracketBegin": '[',
    "BracketEnd": ']',
    "ParenBegin": '{',
    "ParenEnd": '}'
}


def get_braces_content(text, command, beg=0):
    """
    Extract the content of a given LaTeX command from a string.

    **Example:**

    >>> from pogitlab.textools import texparser
    >>> text = "\\\\subfloat[]{\\\\includegraphics[width=.49\\\\textwidth]{fig_11a.pdf}\\\\label{fig:trilepsing_Zpt_VVCR}}"
    >>> texparser.get_braces_content(text, "\\\\includegraphics")
    {'end': 61, 'begin': 49, 'prev': '[width=.49\\textwidth]', 'buf': 'fig_11a.pdf'}
    >>> texparser.get_braces_content(text, "\\\\label")
    {'end': 92, 'begin': 68, 'prev': '', 'buf': 'fig:trilepsing_Zpt_VVCR'}

    The returned dictionary contains the following keys:

    * end: the position in the string where the closing curly brace is found
    * begin: the position in the string where the opening curly brace is found
    * prev: any content between the mactro and the opening curly brace
    * buf: the actual content of the command

    If several matches are found only the first one is returned. To get all matching results used the `get_all_braces` method.

    :param text: Text to scan
    :param command: LaTeX command to search for
    :param beg: Beginning of the search in the string, usefull for nested commands.
    :return: A dictionary with the information about the search
    """
    cmd_start = text.find(command, beg)

    if cmd_start == -1:
        return {'buf': "", "end": beg, "begin": beg}

    if command == "\\subfloat" or command == "\\subfigure":
        assert_subfigures_args_are_valid(text, command, beg)

    args = extract_macro_args(text[cmd_start:], command)

    if args:
        prev = args.get("match")
        init = cmd_start + args.get("span")[1]
    else:
        prev = ""
        init = cmd_start + len(command)

    params = extract_parameters_from_tex(init, text)
    params['prev'] = prev

    return params


def remove_command_from_tex(text, cmd_to_remove):
    """Remove a LaTeX command from a piece of text

    :param text: LaTeX content.
    :type text: string
    :param cmd_to_remove: LaTeX command to be removed from text.
    :type cmd_to_remove: string
    :return: A new text without the commend given.
    :rtype: string
    """
    assert isinstance(text, str)

    return remove_commands_from_tex(text, [cmd_to_remove])


def remove_commands_from_tex(text, cmds_to_remove):
    """Remove a list of commands from text a piece of text

    :param text: LaTeX content.
    :type text: string
    :param cmds_to_remove: LaTeX command to be removed from text.
    :type cmd_to_remove: list
    :return: A new text without the commend given.
    :rtype: string
    """
    assert isinstance(text, str)
    assert isinstance(cmds_to_remove, list)

    buf = Buffer(list(text))

    final_content = []
    while buf.has_next():
        token = next(buf)
        if is_command(token):
            cmd_name = ''.join(buf.peek_until(is_beginning_of_a_group))
            if cmd_name in cmds_to_remove:
                advance_until_end_of_command(buf)
                continue
        final_content.append(token)

    return ''.join(final_content)


def advance_until_end_of_command(buf):
    """
    """
    while buf.has_next():
        token = next(buf)
        if is_beginning_of_a_group(token):
            advance_until_end_of_group(buf)
            if not is_beginning_of_a_group(buf.peek()):
                return
    raise IndexError('Could not reach end of command')


def advance_until_end_of_group(buf):
    while buf.has_next():
        token = next(buf)
        if is_beginning_of_a_group(token):
            advance_until_end_of_group(buf)
        if is_end_of_a_group(token):
            return


def is_command(token):
    return TOKENS["Escape"] == token


def is_beginning_of_a_group(token):
    return token == TOKENS["BracketBegin"] or token == TOKENS["ParenBegin"]


def is_end_of_a_group(token):
    return token == TOKENS["BracketEnd"] or token == TOKENS["ParenEnd"]


def extract_parameters_from_tex(init, text):
    open_count = 0
    close_count = 0
    buf = ""
    begin = init
    last = begin

    # Iterates over the document searching for LaTeX contents
    for i in range(init, len(text)):
        last = i
        c = text[i]
        if c == "{":
            if open_count == 0:
                begin = i
            open_count += 1
            if open_count == 1:
                continue
        elif c == "}":
            close_count += 1
        elif not c.isspace() and open_count == 0:
            begin = i
            buf += c
            break
        if open_count == close_count and open_count > 0:
            break
        if open_count > close_count:
            buf += c

    if open_count == 0:
        last = begin

    return {'buf': buf, "end": last, "begin": begin}

def get_all_braces(text, macro):
    """
    Find the content of all the instances of a given macro in a piece of text.

    **Example:**

    >>> from pogitlab.textools import texparser
    >>> text ="This is an example when using \\\\ref{fig1} and \\\\ref{fig2}"
    >>> texparser.get_braces_content(text, "\\\\ref")
    {'end': 39, 'begin': 34, 'prev': '', 'buf': 'fig1'}
    >>> texparser.get_all_braces(text, "\\\\ref")
    [{'end': 39, 'begin': 34, 'prev': '', 'buf': 'fig1'}, {'end': 54, 'begin': 49, 'prev': '', 'buf': 'fig2'}]


    :param macro: The environment name to be found
    :param text: The string to search in
    :return: An array with all the contents inside the environments ``pattern`` found in ``string``
    """
    result = get_braces_content(text, macro)
    found = []
    while result['buf'] or result['end'] > result['begin']:
        found.append(result)
        result = get_braces_content(text, macro, result['end'])
    return found

def extract_macro_args(text, macro):
    """
    Get the macro arguments in a string format.
    >>> from pogitlab.textools import texparser
    >>> text ="\includegraphics[][width]{somefigure}"
    >>> texparser.extract_macro_args(text, "includegraphics")
    {'span': (16, 25), 'match': '[][width]'}

    :param text: The tex string to search in
    :param macro: The environment name to be found
    :return: A dict containing the in the form {'span': (16, 25), 'match': '[width]'}
    when arguments for the macro are found and None otherwise
    """
    pattern = create_regex_pattern_from_macro(macro)
    regex = re.compile(pattern, re.DOTALL)
    match = regex.search(text)
    if match:
        return {"match": match.group(), "span": match.span()}
    return None

def create_regex_pattern_from_macro(macro):
    seeds = [
        r'(?<=',
        re.escape(macro.replace('\\', '')),
        r')(\[.*?\])((?<=\])(\[.*?\]))?'
    ]
    return ''.join(seeds)

def assert_subfigures_args_are_valid(text, macro, beg=0):
    init = text.find(macro, beg)
    args = extract_macro_args(text[init:], macro)

    if args is not None:
        # this is a weak solution since it won't cover cases where it's located inside braces
        not_math_mode = not "$" in args.get("match")

        if "{" in args.get("match") and not_math_mode or "}" in args.get("match") and not_math_mode:
            raise AttributeError(
                """
                You must not use individual captions for subfigures or
                any additional braces inside your command arguments definition.
                Remove them and use just plain text arguments without '{}'
                """
            )

    return True


def clean_lines(input):
    """
    Read a file and return an array with all the lines avoiding LaTeX comments. This implies:

    * Lines starting with "%" are removed
    * Lines are only parsed until a comment symbol "%" is found. After that charater the rest of the line is ignored. Scaped comment symbols are not considered as comments.
    * Blank lines are maintained due to its importance in LaTeX

    :param input: File to read
    :type input: String
    :return: Array of strings with valid LaTeX lines, not including newline characters
    """
    lines = {"lines": [], "ns": []}
    nline = 0
    if not os.path.isfile(input): return lines
    for i, (ignored, replaced) in enumerate(zip(
        codecs.open(input,"r","utf-8","ignore"),
        codecs.open(input,"r","utf-8","replace"))):
        if ignored!=replaced:
            raise RuntimeError("You have a character in line " + str(i+1) + " of file " + os.path.basename(input) + " that is not UTF-8 encoded. Please replace with a valid UTF-8 encoded character.")
    with open(input,mode="r",encoding="utf-8") as input_file:
        for line in input_file.readlines():
            nline += 1
            line = line.strip()
            if len(line) == 0:
                line = " "
            if line[0] == "%":
                continue
            if "%" in line:
                buf = line[0]
                read = True
                for c in range(1, len(line)):
                    if line[c] == "%" and line[c - 1] != "\\":
                        read = False
                    if read:
                        buf += line[c]
                lines["lines"].append(buf)
                lines["ns"].append(nline)
            else:
                lines["lines"].append(line)
                lines["ns"].append(nline)
    return lines

def get_bibs(input, formatter = Formatter()):
    """
    Get the bibliography files used in a given LaTeX file

    :param input: File where the bib files are included
    :return: Array with all bib files
    """
    latex_commands = clean_lines(input)
    # Check if there's any 'backend=bibtex' forcing not use biber
    for i in range(len(latex_commands["lines"])):
            if "backend=bibtex" in latex_commands["lines"][i]:
                formatter.save_error("Bibtex not allowed please use biber instead")
                formatter.save_error("Bibtex command found on line: " + str(latex_commands["ns"][i]) + " at: '" + latex_commands["lines"][i] + ". This can produce a document with wrong references, so avoid using it.")
    bib = re.compile("addbibresource{(.*?)}", re.MULTILINE)
    return bib.findall("".join(latex_commands["lines"]))


def get_sty_files(document):
    """
    Get all styles files from the document. In order to avoid errors when including packages part of LaTeX and not within the document the existence of the file is checked.

    Style files of the atlaslatex template are included by default.

    :param document: Main LaTeX document where the packages are included
    :return: Array of strings representing the style files used
    """
    files = []
    path = "./"+os.path.dirname(document)
    # Check for any usepackage command with an existing file
    pack = re.compile("usepackage{(.*?)}", re.MULTILINE)
    lines = clean_lines(document)["lines"]
    packages = pack.findall("".join(lines))
    packages += glob.glob(path+"/latex/*sty")
    for p in packages:
        if "atlascover1" in p:
            continue
        f_path = p
        # Now try to fix paths and extensions if it is not present in the package
        if path not in f_path:
            f_path = path+"/"+f_path
        if ".sty" not in f_path:
            f_path += ".sty"
        if os.path.isfile(f_path): files.append(f_path)
    return files

def get_dclass_options(document):
    """
    Get the options passed to the ``documentclass`` LaTeX macro

    :param document: Main document where the document class macro is used
    :return: Array of strings with the options used
    """
    with open(document, 'r') as input_stream:
        tex_soup = TexSoup(input_stream.read())
    dc = tex_soup.documentclass
    if dc is None:
        raise RuntimeError('Document ' + repr(document) + r' is missing a \documentclass')
    if len(dc.args) == 1:
        optional_arguments = []
        required_argument = dc.args[0]
    else:
        optional_arguments = [option.strip() for option in dc.args[0].contents[0].split(',') if option.strip() != '']
        required_argument = dc.args[1]
    return [optional_arguments, required_argument.contents[-1].strip()]

def get_refs_and_labels(files):
    """
    Get information about the references and labels used in a list of files suplied.

    This information includes information about the reference and labels like which ones are missing or udefined as well as format information used to printout with the proper format to the terminal. It is returned as a dictionary with the following keys:

    * max_rep_lab: maximum length of any repeated label (used for proper tabbing)
    * max_unu_lab: maximum length of any unused label (used for proper tabbing)
    * max_und_ref: maximum length of any undefined reference (used for proper tabbing)
    * n_cref: total number of references used in cref or Cref commands (used to check proper parsing)
    * rep_lab: Dictrionary with information about repeated labels.
    * unu_lab: Array of dictionaris with information about unused labels
    * und_ref: Array of dictionaries with information about undefined references

    The unused labels and undefined references are represented in an array of dictionaries. Each element of the array represent an unused label or undefined reference and has the following information:

    * key: the label or reference itself
    * nline: the line number where it has been found
    * file: the file where it has been found

    That information is used for proper referencing in the output log to know where to look for it and fix it.

    The dictionary representing the repeated labels has the label itself as key and as value has an array of dictionries. Each entry of the array represent a repetition of a label and the keys of this dictionary are:

    * lines: an array of line numbers where the label has been found
    * files: an array of files where the label has been found

    Each entry in the lines and files arrays match each other which means that the label has been found in files[0] in the line number lines[0] and so on.

    :param files: Files to look for labels and references
    :return: A dictionary as explained above with all the information about labels and references
    """
    # Loop over all files
    lines = []
    for f in files:
        # Remove lines which are commented and blank and only read the ones which are interesting
        clean = clean_lines(f)
        lls = clean["lines"]
        ns = clean["ns"]
        keywords = "\\ref \\cref \\Cref \\subref \\eqref \\pageref \\labelcref \\label".split()
        for x in range(len(lls)):
            if any(key in lls[x] for key in keywords):
                lines.append({"line": lls[x], "nline": ns[x], "file": f})
    refs = []  # Found references
    labels = []  # Found labels
    all_refs = []  # List with references alone to easily look for them
    all_crefs = [] # List with references found within cref or Cref
    all_labels = []  # List with labels alone
    for line in lines:
        if r'\ref' in line["line"]:
            all = [x["buf"] for x in get_all_braces(line["line"],r'\ref')]
            all_refs += all
            refs.append({"keys": all, "nline": line["nline"], "file": line["file"]})
        if r'\cref' in line["line"]:
            all = [x["buf"] for x in get_all_braces(line["line"],r'\cref')]
            allcopy = all[:]
            for a in all:
                if "," in a:
                    all_refs_split = a.split(",")
                    all_refs_split = [x.strip(" ") for x in all_refs_split]
                    allcopy.remove(a)
                    allcopy.extend(all_refs_split)
            all_refs += allcopy
            all_crefs += allcopy
            refs.append({"keys": allcopy, "nline": line["nline"], "file": line["file"]})
        if r'\Cref' in line["line"]:
            all = [x["buf"] for x in get_all_braces(line["line"],r'\Cref')]
            allcopy = all[:]
            for a in all:
                if "," in a:
                    all_refs_split = a.split(",")
                    all_refs_split = [x.strip(" ") for x in all_refs_split]
                    allcopy.remove(a)
                    allcopy.extend(all_refs_split)
            all_refs += allcopy
            all_crefs += allcopy
            refs.append({"keys": allcopy, "nline": line["nline"], "file": line["file"]})
        if r'\labelcref' in line["line"]:
            all = [x["buf"] for x in get_all_braces(line["line"],r'\labelcref')]
            allcopy = all[:]
            for a in all:
                if "," in a:
                    all_labels_split = a.split(",")
                    all_labels_split = [x.strip(" ") for x in all_labels_split]
                    allcopy.remove(a)
                    allcopy.extend(all_labels_split)
            all_refs += allcopy
            all_crefs += allcopy
            refs.append({"keys": allcopy, "nline": line["nline"], "file": line["file"]})
        if r'\subref' in line["line"]:
            all = [x["buf"] for x in get_all_braces(line["line"],r'\subref')]
            all_refs += all
            refs.append({"keys": all, "nline": line["nline"], "file": line["file"]})
        if r'\eqref' in line["line"]:
            all = [x["buf"] for x in get_all_braces(line["line"],r'\eqref')]
            all_refs += all
            refs.append({"keys": all, "nline": line["nline"], "file": line["file"]})
        if r'\pageref' in line["line"]:
            all = [x["buf"] for x in get_all_braces(line["line"],r'\pageref')]
            all_refs += all
            refs.append({"keys": all, "nline": line["nline"], "file": line["file"]})
        if r'\label' in line["line"] and r'\labelcref' not in line["line"]:
            all = [x["buf"] for x in get_all_braces(line["line"], r'\label')]
            all_labels += all
            labels.append({"keys": all, "nline": line["nline"], "file": line["file"]})
    # Check for undefined references, repeated labels and unused labels
    max_rep_lab = 0  # Keep track of maximum number of characters for output
    max_unu_lab = 0
    max_und_ref = 0
    n_cref = 0
    rep_lab = []
    unu_lab = []
    und_ref = []
    read = []
    for lab in labels:
        for k in lab["keys"]:
            if k in read:
                if len(k) > max_rep_lab:
                    max_rep_lab = len(k)
                rep_lab.append(k)
            elif k not in all_refs:
                if len(k) > max_unu_lab:
                    max_unu_lab = len(k)
                unu_lab.append({"key": k, "nline": lab["nline"], "file": lab["file"]})
        read += lab["keys"]
    for ref in refs:
        for k in ref["keys"]:
            if k not in all_labels:
                if len(k) > max_und_ref:
                    max_und_ref = len(k)
                und_ref.append({"key": k, "nline": ref["nline"], "file": ref["file"]})
    n_cref = len(all_crefs)
    final_rep = {}
    for lab in rep_lab:
        rep = []
        for l in labels:
            if lab in l["keys"]:
                rep.append(l)
        final_rep[lab] = {"lines": [], "files": []}
        for r in rep:
            final_rep[lab]["lines"].append(r["nline"])
            final_rep[lab]["files"].append(r["file"])
    return {"max_rep_lab": max_rep_lab, "max_unu_lab": max_unu_lab, "max_und_ref": max_und_ref,
            "n_cref": n_cref, "rep_lab": final_rep, "unu_lab": unu_lab, "und_ref": und_ref}


def get_title(metadata_file):
    """
    The title of the document from the metadata file

    :param metadata_file: The file with the metadata information
    :return: The title
    """
    alltext = ""
    for line in clean_lines(metadata_file)["lines"]:
        alltext += line
    match = re.search(r"(\\AtlasTitle{.*})", alltext)
    if match:
        title = match.groups()[0]
        return get_braces_content(title, "AtlasTitle")["buf"]


def get_abstract(metadata_file):
    """
    The abstract of the document from the metadata file

    :param metadata_file: The file with the metadata information
    :return: The abstract
    """
    alltext = ""
    for line in clean_lines(metadata_file)["lines"]:
        alltext += line
    match = re.search(r"(\\AtlasAbstract{.*})", alltext)
    if match:
        abstract = match.groups()[0]
        return get_braces_content(abstract, "AtlasAbstract")['buf']


def get_preprint_id(metadata_file):
    """
    The preprint ID of the document from the metadata file

    :param metadata_file: The file with the metadata information
    :return: The preprint ID
    """
    alltext = ""
    for line in clean_lines(metadata_file)["lines"]:
        alltext += line
    match = re.search(r"(\\PreprintIdNumber{.*})", alltext)
    if match:
        preprint_id = match.groups()[0]
        return get_braces_content(preprint_id, "PreprintIdNumber")['buf']


# Custom snippet to avoid issues with the font settings
def get_inputs(fname, rel_path ="./"):
    """
    Get all texfiles used as inputs in the main document. Files included with the ``include`` and ``input`` macros are considered.

    :param fname: File to scan for
    :param rel_path: Path to prepend to the files
    :return: Array of strings with file paths of the included files
    """
    fname = rel_path + "/" + fname
    # Lines causing issues with TexSoup
    problematic_lines = [
        "\\titleformat{\\chapter}[display]",
        "\\titleformat{\\section}",
        "\\titleformat{\\subsection}",
        "\\titleformat{\\subsubsection}"
    ]
    cleaned_lines = []
    with open(fname, "r") as file:
        for line in file:
            # Skip problematic lines
            if any(pl in line for pl in problematic_lines):
                continue
            cleaned_lines.append(line.rstrip())
    text = "\n".join(cleaned_lines) + "\n"
    tex_soup = TexSoup(text)
    texfiles = []
    # Look for tex files included in the main doc
    for node in tex_soup.find_all(["include", "input"]):
        in_file = str(node.contents[0])
        if ".tex" not in in_file:
            in_file += ".tex"
        texfiles.append(in_file)
        for ff in get_inputs(in_file, rel_path):
            texfiles.append(ff)
    return list(set(texfiles))


def get_figures(files, rel_path ="./"):
    """
    Get all figures included in a list of files.

    :param files: Array of files to scan
    :param rel_path: Path to prepend to the files
    :return: A tuple with 2 elements, the figures included (with the correct paths) and figures which are included but are not present in any known path
    """
    figs = []
    paths = []
    if rel_path[-1] != "/":
        rel_path += "/"
    for f in files:
        clean = clean_lines(f)
        lines = clean["lines"]
        ns = clean["ns"]
        for line in lines:
            if r"\includegraphics" in line:
                if r"\includegraphics*" in line:
                    fig = get_braces_content(line, "includegraphics*")
                else:
                    fig = get_braces_content(line, "includegraphics")
                figs.append({"fig": fig["buf"].strip('\"'), "where": "file: {}, line: {}".format(f, ns[lines.index(line)])})
            if r"\graphicspath" in line:
                # If there are more than one split them
                pp = get_braces_content(line, "graphicspath")
                pp = pp["buf"].replace("{", " ").replace("}", " ").strip('\"')
                paths = [rel_path + x for x in pp.split()]
                # Make sure the "/" is prsent at the end
                for p in paths:  # pragma: no cover
                    if p[-1] != "/":
                        p += "/"
    paths.append("./")

    return figures_path_and_missing(figs,paths)

def graphicspath_from_text(text,ref_path):
    """
    Get the paths where figures are stored defined using the ``graphicspath`` macro in LaTeX

    :param text: String to look for the paths defined
    :param ref_path: Reference path to which the graphics path can be relative to
    :return: A list of paths where the figures can be found as defined in the document
    """
    # Use a set here to avoid duplicated
    paths = set()
    if ref_path[-1] != "/":
        ref_path += "/"
    p_paths = get_all_braces(text, r'\graphicspath')
    for path in p_paths:
        pp = path['buf'].replace("{", " ").replace("}", " ")
        for p in pp.split():
            paths.add(ref_path + p)
        for p in paths:  # pragma: no cover
            if p[-1] != "/":
                p += "/"
    paths.add("./")
    # Return a list for easier use later in any case
    return list(paths)

def normalize_paths(paths):
    return set([os.path.normpath(path) for path in paths])

def figures_path_and_missing(figs, paths):
    """
    From a list of figure names (as included in the ``includegraphics`` command) and the paths where to look for them (from the ``graphicspath`` command) get the list of figure paths as well as which figures cannot be found in those paths.

    If the figures used in the ``includegraphics`` command don't have an extension the extension will also be included in the final figure.

    :param figs: List of figures
    :param paths: The paths where to look for them
    :return: A tuple with two elements, a list of figure paths as found in the system and a list of figures from the list which are not found anywhere
    """
    # Note that the order of file extensions here is important.
    # Eariler entries take precedence over later ones.
    figs_ext = (".pdf", ".png", ".jpg", ".jpeg", ".eps")
    finalfigs = []
    missingfigs = []

    paths = normalize_paths(paths)

    for fig in figs:
        filename = fig["fig"]
        exists = False
        for path in paths:
            if filename.lower().endswith(figs_ext):
                filepath = os.path.join(path, filename)
                if os.path.isfile(filepath):
                    finalfigs.append({"fig": os.path.relpath(filepath), "where": fig["where"]})
                    exists = True
            else:
                for ext in figs_ext:
                    filepath = os.path.join(path, filename + ext)
                    if os.path.isfile(filepath):
                        finalfigs.append({"fig": os.path.relpath(filepath), "where": fig["where"]})
                        exists = True
                        # Only consider the first extension found
                        break
            if exists:
                # Only consider the first path found
                break
        if not exists:
            missingfigs.append({"fig": filename, "where": fig["where"]})

    return finalfigs, missingfigs


def get_begin_end(text, environment, beg = 0):
    """
    Environments in LaTeX are defined with ``begin{environment}`` and ``end{environment}``. This method finds the environment especified in a given text and returns information about it.

    The information is returned in a dictionary with the following keys:

    * buf: the environment found
    * end: the position in the text where the environment ends
    * begin: the position in the text where the environment starts
    * special: a dictionary which tells if any special event has been found. For instance, if it has been found that this piece of text belong to the auxiliary material. For each especial event the dictionary will have a value of True if is found or False otherwise.

    If several environments are present only the first one will be returned, to get all environments with the same name use the `get_all_begin_end` method.

    :param text: Text to scan
    :param environment: Environment to find
    :param beg: Where to start looking in the text
    :return: Dictionary with information about the environment
    """
    # Check if the command can be a documentwide command before continuing
    init = text.find("\\begin{{{}}}".format(environment), beg)
    wide_init = text.find("\\begin{{{}}}".format(environment + "*"), beg) # Check also for a pagewide macro
    if init == wide_init == -1:
        return {'buf': "", "end": beg, "begin": beg}
    else:
        if init == -1 or ( wide_init > 0 and init > wide_init) :
            init = wide_init
            environment += "*"
    buf = ""
    last = beg
    begin = beg
    to_begin = True
    # Special events are points in the document from which the special key
    # in the results is filled
    special_events = {
        "auxmat": utils.__basename__.replace(".tex","-auxmat.tex")
    }
    for i in range(init, len(text)):
        if "\\end{{{}}}".format(environment) in buf:
            break
        if "\\begin{{{}}}".format(environment) in buf and to_begin:
            begin = i
            to_begin = False
        last = i
        buf += text[i]

    # Populate special text found
    special = {}
    for sp in special_events:
        if special_events[sp] in text[beg:init]:
            special[sp] = True
        else:
            special[sp] = False
    return {'buf': buf, "end": last, "begin": begin, "special": special}


def get_all_begin_end(text, environments):
    """
    Find all environments in the text supplied.

    :param text: Text to scan
    :param environments: Environments to find in the tex
    :return: Array of figures environments. Each entry has the same format as the returned object in the `get_begin_end` method.
    """
    if not isinstance(environments, list):
        environments = [environments]

    found = []
    for environment in environments:
        result = get_begin_end(text, environment)

        while result['buf']:
            found.append(result)
            result = get_begin_end(text, environment, result['end'])

    return found

def get_preamble(document):
    """
    Get the preample of the LaTeX document, i.e. everything before the ``\\\\begin{document}`` line.

    :param document: The document to scan
    :return: A string with everything in the preamble.
    """
    lines = clean_lines(document)["lines"]
    preamble = []
    for line in lines:
        if "begin{document}" in line: break
        preamble.append(line)
    return preamble


def get_atlas_version(text):
    """
    Extract the AtlasVersion number in the ATLAS template.

    :param text: The content of the main atlaslatex template
    :return: The version of the package
    """
    version = get_braces_content(text,r'\AtlasVersion')['buf']
    if version:
        return int(version.split(".")[0])
    else:
        return 2


def get_label_ref_dict(text):
    """
    Scan text to get all labels and what they should be replaced by when referenced.

    :param text: Text to scan
    :return: A dictionary with a mapping from labels to their reference replacement text
    """

    texsoup = TexSoup(text)
    node_ref_dict = {}
    label_ref_dict = {}
    env_types = ["section",
                 "subsection",
                 "subsubsection",
                 "figure", "figure*",
                 "table", "table*",
                 "equation"]
    env_counts = {env_type: 0 for env_type in env_types if env_type[-1] != '*'}
    current_section_number = 0
    current_subsection_number = 0
    current_subsubsection_number = 0
    for node in texsoup.find_all(env_types + ["label"]):
        if node.name == "section":
            current_section_number += 1
            current_subsection_number = 0
            current_subsubsection_number = 0
        elif node.name == "subsection":
            current_subsection_number += 1
            current_subsubsection_number = 0
        elif node.name == "subsubsection":
            current_subsubsection_number += 1
        elif node.name == "label":
            label_text = str(node.string)
            if node.parent == texsoup or node.parent.name == 'document':
                if current_subsection_number == 0:
                    label_ref_dict[label_text] = str(current_section_number)
                elif current_subsubsection_number == 0:
                    label_ref_dict[label_text] = str(current_section_number) + "." + str(current_subsection_number)
                else:
                    label_ref_dict[label_text] = str(current_section_number) + "." + str(current_subsection_number) + "." + str(current_subsubsection_number)
            else:
                parent = node.parent
                while parent is not None and str(parent) not in node_ref_dict:
                    parent = parent.parent
                if parent is None:
                    warnings.warn(f'Could not associate label {label_text} with a TeX node', RuntimeWarning)
                else:
                    label_ref_dict[label_text] = node_ref_dict[str(parent)]
        else:
            env_type = str(node.name).rstrip('*')
            env_counts[env_type] += 1
            node_ref_dict[str(node)] = str(env_counts[env_type])
            if env_type == "figure":
                current_subfigure_index = -1
                for subnode in node.find_all(['subfigure', 'subfloat']):
                    current_subfigure_index += 1
                    node_ref_dict[str(subnode)] = node_ref_dict[str(node)] + string.ascii_lowercase[current_subfigure_index]
    return label_ref_dict
